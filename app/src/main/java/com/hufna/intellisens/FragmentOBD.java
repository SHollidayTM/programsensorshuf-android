package com.hufna.intellisens;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/23/15.
 */
public class FragmentOBD extends Fragment{
    ActivityMain act;
    ImageButton btnLF;
    ImageButton btnRF;
    ImageButton btnRR;
    ImageButton btnLR;
    ImageButton btnSP;
    TextView txtIDLF;
    TextView txtIDRF;
    TextView txtIDRR;
    TextView txtIDLR;
    TextView txtIDSP;
    TextView txtPSILF;
    TextView txtPSIRF;
    TextView txtPSIRR;
    TextView txtPSILR;
    TextView txtPSISP;
    TextView txtMHzLF;
    TextView txtMHzRF;
    TextView txtMHzRR;
    TextView txtMHzLR;
    TextView txtMHzSP;
    TextView txtTempLF;
    TextView txtTempRF;
    TextView txtTempRR;
    TextView txtTempLR;
    TextView txtTempSP;
    TextView txtBATLF;
    TextView txtBATRF;
    TextView txtBATRR;
    TextView txtBATLR;
    TextView txtBATSP;
    ProgressBar prgLF;
    ProgressBar prgRF;
    ProgressBar prgRR;
    ProgressBar prgLR;
    ProgressBar prgSP;
    int checkSensor;
    ArrayList<String> strIDs;
    int checkCount;
    boolean rechecking;
    boolean shouldRestore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        act.obdCheck = true;
        View view = inflater.inflate(R.layout.fragment_obd, container, false);
        TextView txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        txtTitle.setText(getString(R.string.m_relearnReqs) + " " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        btnLF = (ImageButton)view.findViewById(R.id.btnLF);
        btnLF.setOnClickListener(clkLF);
        btnRF = (ImageButton)view.findViewById(R.id.btnRF);
        btnRF.setOnClickListener(clkRF);
        btnRR = (ImageButton)view.findViewById(R.id.btnRR);
        btnRR.setOnClickListener(clkRR);
        btnLR = (ImageButton)view.findViewById(R.id.btnLR);
        btnLR.setOnClickListener(clkLR);
        btnSP = (ImageButton)view.findViewById(R.id.btnSP);
        btnSP.setOnClickListener(clkSP);
        txtIDLF = (TextView)view.findViewById(R.id.txtIDLF);
        txtIDRF = (TextView)view.findViewById(R.id.txtIDRF);
        txtIDRR = (TextView)view.findViewById(R.id.txtIDRR);
        txtIDLR = (TextView)view.findViewById(R.id.txtIDLR);
        txtIDSP = (TextView)view.findViewById(R.id.txtIDSP);
        txtPSILF = (TextView)view.findViewById(R.id.txtPSILF);
        txtPSIRF = (TextView)view.findViewById(R.id.txtPSIRF);
        txtPSIRR = (TextView)view.findViewById(R.id.txtPSIRR);
        txtPSILR = (TextView)view.findViewById(R.id.txtPSILR);
        txtPSISP = (TextView)view.findViewById(R.id.txtPSISP);
        txtMHzLF = (TextView)view.findViewById(R.id.txtMHzLF);
        txtMHzRF = (TextView)view.findViewById(R.id.txtMHzRF);
        txtMHzRR = (TextView)view.findViewById(R.id.txtMHzRR);
        txtMHzLR = (TextView)view.findViewById(R.id.txtMHzLR);
        txtMHzSP = (TextView)view.findViewById(R.id.txtMHzSP);
        txtTempLF = (TextView)view.findViewById(R.id.txtTempLF);
        txtTempRF = (TextView)view.findViewById(R.id.txtTempRF);
        txtTempRR = (TextView)view.findViewById(R.id.txtTempRR);
        txtTempLR = (TextView)view.findViewById(R.id.txtTempLR);
        txtTempSP = (TextView)view.findViewById(R.id.txtTempSP);
        txtBATLF = (TextView)view.findViewById(R.id.txtBATLF);
        txtBATRF = (TextView)view.findViewById(R.id.txtBATRF);
        txtBATRR = (TextView)view.findViewById(R.id.txtBATRR);
        txtBATLR = (TextView)view.findViewById(R.id.txtBATLR);
        txtBATSP = (TextView)view.findViewById(R.id.txtBATSP);
        prgLF = (ProgressBar)view.findViewById(R.id.prgLF);
        prgRF = (ProgressBar)view.findViewById(R.id.prgRF);
        prgRR = (ProgressBar)view.findViewById(R.id.prgRR);
        prgLR = (ProgressBar)view.findViewById(R.id.prgLR);
        prgSP = (ProgressBar)view.findViewById(R.id.prgSP);
        if(act.preferences.getInt("psiBar", 0) == 1){
            TextView txtPSIBarLF = (TextView)view.findViewById(R.id.txtPSIBarLF);
            txtPSIBarLF.setText(getString(R.string.m_bar));
            txtPSILF.setText(getString(R.string.m_bar));
            TextView txtPSIBarRF = (TextView)view.findViewById(R.id.txtPSIBarRF);
            txtPSIBarRF.setText(getString(R.string.m_bar));
            txtPSIRF.setText(getString(R.string.m_bar));
            TextView txtPSIBarRR = (TextView)view.findViewById(R.id.txtPSIBarRR);
            txtPSIBarRR.setText(getString(R.string.m_bar));
            txtPSIRR.setText(getString(R.string.m_bar));
            TextView txtPSIBarLR = (TextView)view.findViewById(R.id.txtPSIBarLR);
            txtPSIBarLR.setText(getString(R.string.m_bar));
            txtPSILR.setText(getString(R.string.m_bar));
            TextView txtPSIBarSP = (TextView)view.findViewById(R.id.txtPSIBarSP);
            txtPSIBarSP.setText(getString(R.string.m_bar));
            txtPSISP.setText(getString(R.string.m_bar));
        }
        Button btnRelearn = (Button)view.findViewById(R.id.btnRelearn);
        btnRelearn.setOnClickListener(clkRelearn);
        strIDs = new ArrayList<String>();
        if(shouldRestore){
            restoreValues();
        }else{
            act.editor.putString("storedOBDLF", "");
            act.editor.putString("storedOBDRF", "");
            act.editor.putString("storedOBDRR", "");
            act.editor.putString("storedOBDLR", "");
            act.editor.putString("storedOBDSP", "");
            act.editor.commit();
        }
        return view;
    }
    View.OnClickListener clkLF = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 0;
            if(prgLF.getProgress() == 100){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    View.OnClickListener clkRF = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 1;
            if(prgRF.getProgress() == 100){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    View.OnClickListener clkRR = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 2;
            if(prgRR.getProgress() == 100){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    View.OnClickListener clkLR = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 3;
            if(prgLR.getProgress() == 100){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    View.OnClickListener clkSP = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 4;
            if(prgSP.getProgress() == 100){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    public void askForCheckAgain(){
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
                rechecking = true;
                act.doCheck();
            }
        });
        builder.setNeutralButton(getString(R.string.m_cancel), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.setMessage(getString(R.string.m_checkSensorAgain));
        builder.show();
    }
    public void checkResult(String currID, String strPressure, String strTemp, String strFreq, String strBat){
        boolean duplicate = false;
        if(!rechecking){
            for(String prevID : strIDs){
                if(currID.equals(prevID)){
                    if(checkCount < 2){
                        checkCount++;
                        act.doCheck();
                        return;
                    }
                    duplicate = true;
                    break;
                }
            }
        }
        rechecking = false;
        switch(checkSensor){
            case 0: //LF
                act.editor.putString("storedOBDLF", currID + "|" + strPressure + "|" + strTemp + "|" + strFreq + "|" + strBat);
                txtIDLF.setText(currID);
                txtPSILF.setText(strPressure);
                txtTempLF.setText(strTemp);
                txtMHzLF.setText(strFreq);
                txtBATLF.setText(strBat);
                break;
            case 1: //RF
                act.editor.putString("storedOBDRF", currID + "|" + strPressure + "|" + strTemp + "|" + strFreq + "|" + strBat);
                txtIDRF.setText(currID);
                txtPSIRF.setText(strPressure);
                txtTempRF.setText(strTemp);
                txtMHzRF.setText(strFreq);
                txtBATRF.setText(strBat);
                break;
            case 2: //RR
                act.editor.putString("storedOBDRR", currID + "|" + strPressure + "|" + strTemp + "|" + strFreq + "|" + strBat);
                txtIDRR.setText(currID);
                txtPSIRR.setText(strPressure);
                txtTempRR.setText(strTemp);
                txtMHzRR.setText(strFreq);
                txtBATRR.setText(strBat);
                break;
            case 3: //LR
                act.editor.putString("storedOBDLR", currID + "|" + strPressure + "|" + strTemp + "|" + strFreq + "|" + strBat);
                txtIDLR.setText(currID);
                txtPSILR.setText(strPressure);
                txtTempLR.setText(strTemp);
                txtMHzLR.setText(strFreq);
                txtBATLR.setText(strBat);
                break;
            case 4: //SP
                act.editor.putString("storedOBDSP", currID + "|" + strPressure + "|" + strTemp + "|" + strFreq + "|" + strBat);
                txtIDSP.setText(currID);
                txtPSISP.setText(strPressure);
                txtTempSP.setText(strTemp);
                txtMHzSP.setText(strFreq);
                txtBATSP.setText(strBat);
                break;
        }
        act.editor.commit();
        if(duplicate){
            switch(checkSensor){
                case 0: //LF
                    btnLF.setBackgroundColor(Color.rgb(214, 0, 21));
                    prgLF.getProgressDrawable().setColorFilter(Color.rgb(214, 0, 21), PorterDuff.Mode.SRC_IN);
                    break;
                case 1: //RF
                    btnRF.setBackgroundColor(Color.rgb(214, 0, 21));
                    prgRF.getProgressDrawable().setColorFilter(Color.rgb(214, 0, 21), PorterDuff.Mode.SRC_IN);
                    break;
                case 2: //RR
                    btnRR.setBackgroundColor(Color.rgb(214, 0, 21));
                    prgRR.getProgressDrawable().setColorFilter(Color.rgb(214, 0, 21), PorterDuff.Mode.SRC_IN);
                    break;
                case 3: //LR
                    btnLR.setBackgroundColor(Color.rgb(214, 0, 21));
                    prgLR.getProgressDrawable().setColorFilter(Color.rgb(214, 0, 21), PorterDuff.Mode.SRC_IN);
                    break;
                case 4: //SP
                    btnSP.setBackgroundColor(Color.rgb(214, 0, 21));
                    prgSP.getProgressDrawable().setColorFilter(Color.rgb(214, 0, 21), PorterDuff.Mode.SRC_IN);
                    break;
            }
            Toast.makeText(act, getString(R.string.m_duplicateSensor), Toast.LENGTH_SHORT).show();
        }else{
            strIDs.add(currID);
            switch(checkSensor){
                case 0: //LF
                    prgLF.getProgressDrawable().setColorFilter(Color.rgb(51, 102, 0), PorterDuff.Mode.SRC_IN);
                    prgLF.setProgress(100);
                    btnLF.setBackgroundColor(Color.rgb(51, 102, 0));
                    break;
                case 1: //RF
                    prgRF.getProgressDrawable().setColorFilter(Color.rgb(51, 102, 0), PorterDuff.Mode.SRC_IN);
                    prgRF.setProgress(100);
                    btnRF.setBackgroundColor(Color.rgb(51, 102, 0));
                    break;
                case 2: //RR
                    prgRR.getProgressDrawable().setColorFilter(Color.rgb(51, 102, 0), PorterDuff.Mode.SRC_IN);
                    prgRR.setProgress(100);
                    btnRR.setBackgroundColor(Color.rgb(51, 102, 0));
                    break;
                case 3: //LR
                    prgLR.getProgressDrawable().setColorFilter(Color.rgb(51, 102, 0), PorterDuff.Mode.SRC_IN);
                    prgLR.setProgress(100);
                    btnLR.setBackgroundColor(Color.rgb(51, 102, 0));
                    break;
                case 4: //SP
                    prgSP.getProgressDrawable().setColorFilter(Color.rgb(51, 102, 0), PorterDuff.Mode.SRC_IN);
                    prgSP.setProgress(100);
                    btnSP.setBackgroundColor(Color.rgb(51, 102, 0));
                    break;
            }
            Toast.makeText(act, getString(R.string.m_sensorCheckComplete), Toast.LENGTH_SHORT).show();
        }
    }
    public void restoreValues(){
        String storedLF = act.preferences.getString("storedOBDLF", "");
        if(storedLF.length() > 0){
            checkSensor = 0;
            String[] values = storedLF.split("[|]");
            checkResult(values[0], values[1], values[2], values[3], values[4]);
        }
        String storedRF = act.preferences.getString("storedOBDRF", "");
        if(storedRF.length() > 0){
            checkSensor = 1;
            String[] values = storedRF.split("[|]");
            checkResult(values[0], values[1], values[2], values[3], values[4]);
        }
        String storedRR = act.preferences.getString("storedOBDRR", "");
        if(storedRR.length() > 0){
            checkSensor = 2;
            String[] values = storedRR.split("[|]");
            checkResult(values[0], values[1], values[2], values[3], values[4]);
        }
        String storedLR = act.preferences.getString("storedOBDLR", "");
        if(storedLR.length() > 0){
            checkSensor = 3;
            String[] values = storedLR.split("[|]");
            checkResult(values[0], values[1], values[2], values[3], values[4]);
        }
        String storedSP = act.preferences.getString("storedOBDSP", "");
        if(storedSP.length() > 0){
            checkSensor = 4;
            String[] values = storedSP.split("[|]");
            checkResult(values[0], values[1], values[2], values[3], values[4]);
        }
    }
    View.OnClickListener clkRelearn = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            //if(strIDs.size() > 0){
                if(act.btSocketOBD != null){
                    if(act.btSocketOBD.isConnected()){
                        act.discoverOBDProtocol();
                    }else{
                        showAlert();
                    }
                }else{
                    showAlert();
                }
            //}else{
            //    Toast.makeText(act, getString(R.string.m_sensorBeforeOBD), Toast.LENGTH_SHORT).show();
            //}
        }
    };
    public void showAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which){
                act.openBT();
                dialog.dismiss();
            }
        });
        builder.setNeutralButton(getString(R.string.m_cancel), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.setTitle(getString(R.string.m_obd));
        builder.setMessage(getString(R.string.m_connectOBD));
        builder.show();
    }
}
