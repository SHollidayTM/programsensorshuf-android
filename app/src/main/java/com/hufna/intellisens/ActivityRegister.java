package com.hufna.intellisens;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sholliday on 12/10/15.
 */
public class ActivityRegister extends Activity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public boolean successful;
    EditText txtName;
    EditText txtCompany;
    EditText txtAddressOne;
    EditText txtAddressTwo;
    EditText txtCity;
    EditText txtState;
    EditText txtPostal;
    EditText txtPhone;
    EditText txtEmail;
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_register);
        txtName = (EditText)findViewById(R.id.txtName);
        txtCompany = (EditText)findViewById(R.id.txtCompany);
        txtAddressOne = (EditText)findViewById(R.id.txtAddressOne);
        txtAddressTwo = (EditText)findViewById(R.id.txtAddressTwo);
        txtCity = (EditText)findViewById(R.id.txtCity);
        txtState = (EditText)findViewById(R.id.txtState);
        txtPostal = (EditText)findViewById(R.id.txtPostal);
        txtPhone = (EditText)findViewById(R.id.txtPhone);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        Button btnGo = (Button)findViewById(R.id.btnGo);
        btnGo.setOnClickListener(clkGo);
        preferences = getApplicationContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        editor = preferences.edit();
        if(preferences.getInt("registered", 0) == 1){
            txtName.setText(preferences.getString("name", ""));
            txtCompany.setText(preferences.getString("companyName", ""));
            txtAddressOne.setText(preferences.getString("addressOne", ""));
            txtAddressTwo.setText(preferences.getString("addressTwo", ""));
            txtCity.setText(preferences.getString("city", ""));
            txtState.setText(preferences.getString("state", ""));
            txtPostal.setText(preferences.getString("postal", ""));
            txtEmail.setText(preferences.getString("email", ""));
            txtPhone.setText(preferences.getString("phone", ""));
        }
    }
    View.OnClickListener clkGo = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            String errMsg = "";
            if(!(txtName.length() > 0)){
                errMsg = getString(R.string.m_firstLastName);
            }else if(!(txtCompany.length() > 0)){
                errMsg = getString(R.string.m_companyName);
            }else if(!(txtAddressOne.length() > 0)){
                errMsg = getString(R.string.m_companyAddress);
            }else if(!(txtCity.length() > 0)){
                errMsg = getString(R.string.m_companyCity);
            }else if(!(txtState.length() > 0)){
                errMsg = getString(R.string.m_companyState);
            }else if(!(txtPostal.length() > 0)){
                errMsg = getString(R.string.m_companyZip);
            }else if(!(txtPhone.length() > 0)){
                errMsg = getString(R.string.m_companyPhone);
            }else if(!(txtEmail.length() > 0)){
                errMsg = getString(R.string.m_companyEmail);
            }
            if(errMsg.equals("")){
                Pattern patUsername = Pattern.compile(".+@.+\\.[A-Za-z]{2,4}");
                Matcher matcher = patUsername.matcher(txtEmail.getText().toString());
                if(!matcher.find()){
                    Toast.makeText(ActivityRegister.this, getString(R.string.m_invalidEmail), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            if(errMsg.length() > 0){
                Toast.makeText(ActivityRegister.this, getString(R.string.m_pleaseFillOut) + " " + errMsg, Toast.LENGTH_SHORT).show();
            }else{
                editor.putString("name", txtName.getText().toString());
                editor.putString("companyName", txtCompany.getText().toString());
                editor.putString("addressOne", txtAddressOne.getText().toString());
                editor.putString("addressTwo", txtAddressTwo.getText().toString());
                editor.putString("city", txtCity.getText().toString());
                editor.putString("state", txtState.getText().toString());
                editor.putString("postal", txtPostal.getText().toString());
                editor.putString("email", txtEmail.getText().toString());
                editor.putString("phone", txtPhone.getText().toString());
                editor.commit();
                ModuleRegister register = new ModuleRegister();
                register.setContext(ActivityRegister.this);
                register.execute();
            }
        }
    };
    public void returnRegistration(){
        if(successful){
            editor.putInt("registered", 1);
            Toast.makeText(this, getString(R.string.m_regSuccessful), Toast.LENGTH_SHORT).show();
            finish();
        }else{
            editor.putInt("registered", 0);
            Toast.makeText(this, getString(R.string.m_regUnsuccessful), Toast.LENGTH_SHORT).show();
        }
        editor.commit();
    }
    @Override
    public void onBackPressed(){
        finish();
    }
}
