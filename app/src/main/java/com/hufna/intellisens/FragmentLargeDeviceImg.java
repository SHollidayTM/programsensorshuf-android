package com.hufna.intellisens;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by sholliday on 12/22/15.
 */
public class FragmentLargeDeviceImg extends Fragment {
    ActivityMain act;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_largedeviceimg, container, false);
        ImageView imvLargeProduct = (ImageView)view.findViewById(R.id.imvDevice);
        imvLargeProduct.setImageBitmap(act.bmpDevice);
        imvLargeProduct.setOnClickListener(clkBack);
        return view;
    }
    View.OnClickListener clkBack = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.back();
        }
    };
}
