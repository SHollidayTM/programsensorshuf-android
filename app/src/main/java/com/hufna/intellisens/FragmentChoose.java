package com.hufna.intellisens;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/17/15.
 */
public class FragmentChoose extends Fragment {
    ActivityMain act;
    ArrayList<String> sensorNames;
    public boolean fromCopyOrCopySet;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_choose, container, false);
        sensorNames = new ArrayList<String>();
        for(SensorNameFreq sensorNameFreq : act.vehicle.sensorNameFreqs){
            sensorNames.add(sensorNameFreq.name);
        }
        final ListView lstSensors = (ListView)view.findViewById(R.id.lstSensors);
        ArrayAdapter<String> ada = new ArrayAdapter<String>(act, R.layout.item_centered, sensorNames);
        lstSensors.setAdapter(ada);
        lstSensors.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3){
                act.sensorNameFreq = act.vehicle.sensorNameFreqs.get(position);
                act.openCreate(fromCopyOrCopySet);
            }
        });
        return view;
    }
}