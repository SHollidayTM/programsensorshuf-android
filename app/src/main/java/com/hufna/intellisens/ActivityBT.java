package com.hufna.intellisens;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by sholliday on 12/16/15.
 */
public class ActivityBT extends Activity{
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String passedAddressHC;
    String serialNumHC;
    String firmwareVersionHC;
    String hardwareVersionHC;
    String databaseVersionHC;
    String passedAddressOBD;
    String serialNumOBD;
    String firmwareVersionOBD;
    String hardwareVersionOBD;
    String databaseVersionOBD;
    ListView lstDevices;
    BluetoothAdapter btAdapter;
    Set<BluetoothDevice> pairedDevices;
    ArrayList<BTObject> btObjects;
    Button btnScan;
    Button btnShowPaired;
    boolean scanning;
    boolean showingPaired = true;
    @Override
    public void onCreate(Bundle icicle){
        super.onCreate(icicle);
        preferences = getApplicationContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        editor = preferences.edit();
        Bundle extras = getIntent().getExtras();
        passedAddressHC = null;
        serialNumHC = null;
        firmwareVersionHC = null;
        hardwareVersionHC = null;
        databaseVersionHC = null;
        passedAddressOBD = null;
        serialNumOBD = null;
        firmwareVersionOBD = null;
        hardwareVersionOBD = null;
        databaseVersionOBD = null;
        if(extras != null){
            passedAddressHC = extras.getString("addressHC");
            serialNumHC = extras.getString("serialNumHC");
            firmwareVersionHC = extras.getString("fwVersionHC");
            hardwareVersionHC = extras.getString("hwVersionHC");
            databaseVersionHC = extras.getString("dbVersionHC");
            passedAddressOBD = extras.getString("addressOBD");
            serialNumOBD = extras.getString("serialNumOBD");
            firmwareVersionOBD = extras.getString("fwVersionOBD");
            hardwareVersionOBD = extras.getString("hwVersionOBD");
            databaseVersionOBD = extras.getString("dbVersionOBD");
        }
        setContentView(R.layout.activity_bt);
        btnScan = (Button)findViewById(R.id.btnScan);
        btnScan.setOnClickListener(clkScan);
        btnShowPaired = (Button)findViewById(R.id.btnShowPaired);
        btnShowPaired.setOnClickListener(clkShowPaired);
        setupList();
    }
    public void setupList(){
        btnShowPaired.setVisibility(View.INVISIBLE);
        lstDevices = (ListView)findViewById(R.id.lstDevices);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        pairedDevices = btAdapter.getBondedDevices();
        btObjects = new ArrayList<BTObject>();
        if(pairedDevices.size() > 0){
            for(BluetoothDevice device : pairedDevices){
                BTObject btObject = new BTObject();
                btObject.device = device;
                btObject.address = device.getAddress();
                if(device.getName().substring(0, 2).equals("DT") || device.getName().substring(0, 2).equals("RG")){
                    //if(device.getName().substring(0, 2).equals("DT")) {
                        btObject.name = device.getName().replace(device.getName().substring(0, 2), "HC");
                        btObject.obdDevice = false;
                        if(btObject.address.equals(passedAddressHC)){
                            btObject.serialNum = serialNumHC;
                            btObject.firmwareVersion = firmwareVersionHC;
                            btObject.hardwareVersion = hardwareVersionHC;
                            btObject.databaseVersion = databaseVersionHC;
                            btnShowPaired.setVisibility(View.VISIBLE);
                            btObject.connected = true;
                            editor.putString("btID", btObject.address);
                            editor.commit();
                        }
                    //}else{
                    //    btObject.name = device.getName().replace(device.getName().substring(0, 2), "OBD");
                    //    btObject.obdDevice = true;
                    //    if(btObject.address.equals(passedAddressOBD)){
                    //        btObject.serialNum = serialNumOBD;
                    //        btObject.firmwareVersion = firmwareVersionOBD;
                    //        btObject.hardwareVersion = hardwareVersionOBD;
                    //        btObject.databaseVersion = databaseVersionOBD;
                    //        btnShowPaired.setVisibility(View.VISIBLE);
                    //        btObject.connected = true;
                    //    }
                    //}
                    if(!showingPaired){
                        if(btObject.connected){
                            btObjects.add(btObject);
                        }
                    }else{
                        btObjects.add(btObject);
                    }
                }
            }
        }
        refreshList();
    }
    public void refreshList(){
        AdapterBT ada = new AdapterBT(this, btObjects);
        lstDevices.setAdapter(ada);
        lstDevices.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3){
                final BTObject btObject = btObjects.get(position);
                if(!btObject.connected){
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityBT.this);
                    builder.setTitle(getString(R.string.m_connectDevice));
                    builder.setMessage(btObject.name + " @ " + btObject.address);
                    builder.setPositiveButton(getString(R.string.m_yes), new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which){
                            dialog.dismiss();
                            Intent i = getIntent();
                            i.putExtra("btDevice", btObject.device);
                            setResult(RESULT_OK, i);
                            if(scanning){
                                stopScanning();
                            }
                            finish();
                        }
                    });
                    builder.setNeutralButton(getString(R.string.m_no), null);
                    builder.show();
                }
            }
        });
    }
    View.OnClickListener clkScan = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            Button btn = (Button)v;
            if(!scanning){
                scanning = true;
                btn.setText(getString(R.string.m_scanTapToStop));
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityBT.this);
                        builder.setTitle(getString(R.string.m_locationAccess));
                        builder.setMessage(getString(R.string.m_pleaseGrantLocation));
                        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int which){
                                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    }else{
                        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                        registerReceiver(mReceiver, filter);
                        btAdapter.startDiscovery();
                    }
                }else{
                    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    registerReceiver(mReceiver, filter);
                    btAdapter.startDiscovery();
                }
            }else{
                stopScanning();
            }
        }
    };
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch(requestCode){
            case 1:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    registerReceiver(mReceiver, filter);
                    btAdapter.startDiscovery();
                }else{
                    scanning = false;
                    btnScan.setText(getString(R.string.m_scanNewDevices));
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(getString(R.string.m_functionalityLimited));
                    builder.setMessage(getString(R.string.m_noBluetooth));
                    builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
                break;
        }
    }
    View.OnClickListener clkShowPaired = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            Button btn = (Button)v;
            if(!showingPaired){
                showingPaired = true;
                btn.setText(getString(R.string.m_hideDevices));
            }else{
                showingPaired = false;
                btn.setText(getString(R.string.m_showDevices));
            }
            setupList();
        }
    };
    private final BroadcastReceiver mReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, Intent intent){
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                BTObject btObject = new BTObject();
                btObject.device = device;
                btObject.address = device.getAddress();
                if(device.getName().substring(0, 2).equals("DT") || device.getName().substring(0, 2).equals("RG")){
                    btObject.name = device.getName().replace(device.getName().substring(0, 2), "HC");
                    boolean objectAlreadyInList = false;
                    for(BTObject listBTObject : btObjects){
                        if(listBTObject.name.equals(btObject.name)){
                            objectAlreadyInList = true;
                            break;
                        }
                    }
                    if(!objectAlreadyInList){
                        btObjects.add(btObject);
                        stopScanning();
                        device.fetchUuidsWithSdp();
                        refreshList();
                    }
                }
            }else if(BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action)){
                setupList();
            }
        }
    };
    public void stopScanning(){
        scanning = false;
        btnScan.setText(getString(R.string.m_scanNewDevices));
        unregisterReceiver(mReceiver);
        btAdapter.cancelDiscovery();
    }
    @Override
    public void onBackPressed(){
        close();
    }
    public void close(){
        if(scanning){
            stopScanning();
        }
        Intent i = getIntent();
        setResult(RESULT_CANCELED, i);
        finish();
    }
}
