package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class FragmentTechDocs extends Fragment {
    ActivityMore act;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMore)getActivity();
        View view = inflater.inflate(R.layout.fragment_techdocs, container, false);
        setListContent(view);
        return view;
    }
    private void setListContent(View view){
        ArrayList<String> docValues = new ArrayList<String>();
        docValues.add(getString(R.string.m_tipsTechs));
        docValues.add(getString(R.string.m_initialReport));
        docValues.add(getString(R.string.m_finalReport));
        ListView lstDocs = (ListView)view.findViewById(R.id.lstDocs);
        ArrayAdapter<String> ada = new ArrayAdapter<String>(act, R.layout.item_centered, docValues);
        lstDocs.setAdapter(ada);
        lstDocs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                String fileName = null;
                switch(position){
                    case 0:
                        fileName = "tips_techniques.pdf";
                        break;
                    case 1:
                        fileName = "initial_customer.pdf";
                        break;
                    case 2:
                        fileName = "final_customer.pdf";
                        break;
                }
                AssetManager assetManager = act.getAssets();
                InputStream in = null;
                OutputStream out = null;
                File pdfFile = null;
                try{
                    in = assetManager.open(fileName);
                    pdfFile = new File(act.getExternalFilesDir(null), fileName);
                    out = new FileOutputStream(pdfFile);
                    byte[] buffer = new byte[1024];
                    int read;
                    while((read = in.read(buffer)) != -1){
                        out.write(buffer, 0, read);
                    }
                }catch(IOException e){
                }
                if(pdfFile != null){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                }
            }
        });
    }
}