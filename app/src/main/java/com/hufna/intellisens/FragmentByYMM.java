package com.hufna.intellisens;

import android.app.Dialog;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/8/15.
 */
public class FragmentByYMM extends Fragment{
    ActivityMain act;
    Spinner spnYears;
    Spinner spnMakes;
    Spinner spnModels;
    ArrayList<String> years;
    ArrayList<Make> makes;
    ArrayList<Model> models;
    boolean preload;
    LinearLayout layBtnRam;
    Button btnRam;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_byymm, container, false);
        years = act.moduleDB.getAllYears();
        spnYears = (Spinner)view.findViewById(R.id.spnYears);
        ArrayAdapter<String> ada = new ArrayAdapter<String>(act, R.layout.item_spinner, years);
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnYears.setAdapter(ada);
        spnYears.setOnItemSelectedListener(clkYear);
        spnMakes = (Spinner)view.findViewById(R.id.spnMakes);
        ArrayList<String>selectMake = new ArrayList<String>();
        selectMake.add(getString(R.string.m_selectMake));
        ArrayAdapter<String> adaSelectMake = new ArrayAdapter<String>(act, R.layout.item_spinner, selectMake);
        adaSelectMake.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnMakes.setAdapter(adaSelectMake);
        spnModels = (Spinner)view.findViewById(R.id.spnModels);
        ArrayList<String>selectModel = new ArrayList<String>();
        selectModel.add(getString(R.string.m_selectModel));
        ArrayAdapter<String> adaSelectModel = new ArrayAdapter<String>(act, R.layout.item_spinner, selectModel);
        adaSelectModel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnModels.setAdapter(adaSelectModel);
        layBtnRam = (LinearLayout)view.findViewById(R.id.layBtnRam);
        btnRam = new Button(act);
        btnRam.setText("Go to Ram Trucks");
        btnRam.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL));
        btnRam.setBackgroundColor(Color.TRANSPARENT);
        btnRam.setTextColor(Color.BLUE);
        btnRam.setOnClickListener(clkRam);
        Button btnGo = (Button)view.findViewById(R.id.btnGo);
        btnGo.setOnClickListener(clkGo);
        if((act.selectedYear != null) && (act.selectedMake != null) && (act.selectedModel != null)){
            preload();
        }
        return view;
    }
    public void preload(){
        preload = true;
        for(int i = 0; i < years.size(); i++){
            String year = years.get(i);
            if(year.equals(act.selectedYear)){
                spnYears.setSelection(i);
                break;
            }
        }
        setupMakes();
        for(int i = 0; i < makes.size(); i++){
            Make make = makes.get(i);
            if(make.makeName.equals(act.selectedMake.makeName)){
                final int finalI = i;
                new Handler().postDelayed(new Runnable(){
                    public void run(){
                        spnMakes.setSelection(finalI);
                    }
                }, 400);
                break;
            }
        }
        setupModels();
        for(int i = 0; i < models.size(); i++){
            Model model = models.get(i);
            if(model.modelName.equals(act.selectedModel.modelName)){
                final int finalI = i;
                new Handler().postDelayed(new Runnable(){
                    public void run(){
                        preload = false;
                        spnModels.setSelection(finalI);
                        if(act.fromVIN){
                            act.fromVIN = false;
                            qualifierGo();
                        }
                    }
                }, 800);
                break;
            }
        }
    }
    AdapterView.OnItemSelectedListener clkYear = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            if(position > 0){
                act.selectedYear = years.get(position);
                if(!preload){
                    setupMakes();
                }
            }else{
                clearSpinners();
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    public void setupMakes(){
        makes = act.moduleDB.getMakesByYear(act.selectedYear);
        ArrayList<String> makeValues = new ArrayList<String>();
        for(Make make : makes){
            makeValues.add(make.makeName);
        }
        ArrayAdapter<String> ada = new ArrayAdapter<String>(act, R.layout.item_spinner, makeValues);
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnMakes.setAdapter(ada);
        spnMakes.setOnItemSelectedListener(clkMake);
        if(act.selectedMake != null){
            if(Integer.valueOf(act.selectedYear) >= 2012 && act.selectedMake.identifier == 40){
                layBtnRam.addView(btnRam);
            }else{
                layBtnRam.removeAllViews();
            }
        }
    }
    AdapterView.OnItemSelectedListener clkMake = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            act.selectedMake = makes.get(position);
            if(!preload){
                setupModels();
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView){
        }
    };
    public void setupModels(){
        models = act.moduleDB.getModelsByYearAndMake(act.selectedYear, act.selectedMake);
        ArrayList<String> modelValues = new ArrayList<String>();
        for(Model model : models){
            modelValues.add(model.modelName);
        }
        ArrayAdapter<String> ada = new ArrayAdapter<String>(act, R.layout.item_spinner, modelValues);
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnModels.setAdapter(ada);
        spnModels.setOnItemSelectedListener(clkModel);
        if(Integer.valueOf(act.selectedYear) >= 2012 && act.selectedMake.identifier == 40){
            layBtnRam.addView(btnRam);
        }else{
            layBtnRam.removeAllViews();
        }
    }
    AdapterView.OnItemSelectedListener clkModel = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            act.selectedModel = models.get(position);
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView){
        }
    };
    public void clearSpinners(){
        act.selectedYear = null;
        act.selectedMake = null;
        act.selectedModel = null;
        spnYears.setSelection(0);
        spnMakes.setSelection(0);
        spnModels.setSelection(0);
        act.editor.putString("selectedYear", "");
        act.editor.putString("selectedMake", "");
        act.editor.putString("selectedModel", "");
        act.editor.commit();
    }
    View.OnClickListener clkGo = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            qualifierGo();
        }
    };
    public void qualifierGo(){
        if((act.selectedYear != null) && (act.selectedMake != null) && (act.selectedModel != null)){
            act.editor.putString("selectedYear", act.selectedYear);
            act.editor.putString("selectedMake", act.selectedMake.makeName);
            act.editor.putString("selectedModel", act.selectedModel.modelName);
            act.editor.commit();
            ArrayList<VehicleConfig> vehicleConfigs = act.moduleDB.getQualifiersByYearMakeAndModel(act.selectedYear, act.selectedMake, act.selectedModel);
            if(vehicleConfigs.size() > 1){
                displayQualifiers(vehicleConfigs);
            }else{
                act.selectedVehicleConfig = vehicleConfigs.get(0);
                go();
            }
        }else{
            Toast.makeText(act, getString(R.string.m_invalidYMM), Toast.LENGTH_SHORT).show();
        }
    }
    public void displayQualifiers(final ArrayList<VehicleConfig> vehicleConfigs){
        final Dialog dialog = new Dialog(act);
        dialog.setContentView(R.layout.dialog_qualifiers);
        dialog.setCancelable(true);
        final RadioGroup rdgAll = (RadioGroup)dialog.findViewById(R.id.rdgAll);
        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 5, 5, 5);
        for(int i = 0; i < vehicleConfigs.size(); i++){
            VehicleConfig vehicleConfig = vehicleConfigs.get(i);
            RadioButton rdo = new RadioButton(act);
            rdo.setId(i);
            rdo.setLayoutParams(params);
            rdo.setText(vehicleConfig.qualifier);
            rdgAll.addView(rdo);
        }
        Button btnOK = (Button)dialog.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int rdoID = rdgAll.getCheckedRadioButtonId();
                if(rdoID > -1){
                    act.selectedVehicleConfig = vehicleConfigs.get(rdoID);
                    dialog.dismiss();
                    go();
                }else{
                    Toast.makeText(act, getString(R.string.m_validSelection), Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }
    public void go(){
        act.sensor = null;
        act.sensorNameFreq = null;
        act.freqChosen = false;
        act.serviceKit = null;
        act.programmables = new ArrayList<SensorTPMS>();
        act.nonprogrammables = new ArrayList<SensorTPMS>();
        act.serviceKits = new ArrayList<ServiceKit>();
        if(act.inService){
            act.openService();
        }else{
            if(act.selectedVehicleConfig.indirect == false){
                ModuleGetProdsByConfig getProds = new ModuleGetProdsByConfig();
                getProds.setContext(act);
                getProds.execute();
            }else{
                Toast.makeText(act, getString(R.string.m_indirectSystem), Toast.LENGTH_SHORT).show();
            }
        }
    }
    View.OnClickListener clkRam = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for(int i = 0; i < makes.size(); i++){
                Make make = makes.get(i);
                if(make.identifier == 1168){
                    act.selectedMake = make;
                    final int finalI = i;
                    new Handler().postDelayed(new Runnable(){
                        public void run(){
                            spnMakes.setSelection(finalI);
                        }
                    }, 200);
                    break;
                }
            }
            setupModels();
            layBtnRam.removeAllViews();
        }
    };
}
