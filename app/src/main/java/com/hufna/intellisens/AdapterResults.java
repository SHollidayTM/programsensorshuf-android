package com.hufna.intellisens;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

class AdapterResults extends BaseAdapter {
    Context ctx;
    ArrayList items;
    private static LayoutInflater inflater = null;
    ImageView imvDevice;
    public AdapterResults(Context context, ArrayList allItems) {
        ctx = context;
        items = allItems;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public Object getItem(int position) {
        return items.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(items.get(position).getClass().equals(String.class)){
            String header = (String)items.get(position);
            convertView = inflater.inflate(R.layout.header_resultlist, null);
            TextView txtHeader = (TextView)convertView.findViewById(R.id.txtHeader);
            txtHeader.setText(header);
        }else if(items.get(position).getClass().equals(SensorTPMS.class)){
            SensorTPMS sensor = (SensorTPMS)items.get(position);
            convertView = inflater.inflate(R.layout.item_result, null);
            imvDevice = (ImageView)convertView.findViewById(R.id.imvDevice);
            File file = new File(ctx.getFilesDir(), sensor.name + ".jpg");
            if(file.exists()){
                setBitmap(file);
            }else{
                imvDevice.setImageResource(R.drawable.noimage);
            }
            TextView txtName = (TextView)convertView.findViewById(R.id.txtName);
            txtName.setText(sensor.name);
            if(sensor.frequency != null){
                TextView txtMHz = (TextView)convertView.findViewById(R.id.txtMHz);
                txtMHz.setText(sensor.frequency + ctx.getString(R.string.m_mhz));
            }
        }else{
            ServiceKit serviceKit = (ServiceKit)items.get(position);
            convertView = inflater.inflate(R.layout.item_result, null);
            imvDevice = (ImageView)convertView.findViewById(R.id.imvDevice);
            File file = new File(ctx.getFilesDir(), serviceKit.name + ".jpg");
            if(file.exists()){
                setBitmap(file);
            }
            TextView txtName = (TextView)convertView.findViewById(R.id.txtName);
            txtName.setText(serviceKit.name);
            TextView txtMHz = (TextView)convertView.findViewById(R.id.txtMHz);
            if(serviceKit.description.contains("Gen 1")){
                txtMHz.setText(ctx.getString(R.string.m_gen) + " 1");
            }else{
                txtMHz.setText(ctx.getString(R.string.m_gen) + " 2");
            }
        }
        return convertView;
    }
    public void setBitmap(File file){
        FileInputStream input;
        try{
            input = new FileInputStream(file);
            BitmapFactory.Options options = new BitmapFactory.Options();
            Rect rect = new Rect(0, 0, 100, 100);
            options.inSampleSize = 4;
            Bitmap bmp = BitmapFactory.decodeStream(input, rect, options);
            imvDevice.setImageBitmap(bmp);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }
}
