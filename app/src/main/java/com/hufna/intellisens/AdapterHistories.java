package com.hufna.intellisens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
* Created by sholliday on 1/7/16.
*/

public class AdapterHistories extends BaseAdapter{
    Context ctx;
    ArrayList histories;
    private static LayoutInflater inflater = null;
    public AdapterHistories(Context context, ArrayList allHistories){
        ctx = context;
        histories = allHistories;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount(){
        return histories.size();
    }
    @Override
    public Object getItem(int position){
        return histories.get(position);
    }
    @Override
    public long getItemId(int position){
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        History history = (History)histories.get(position);
        View view = inflater.inflate(R.layout.item_history, null);
        TextView txtDate = (TextView)view.findViewById(R.id.txtDate);
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        txtDate.setText(sdf.format(history.date));
        TextView txtYear = (TextView)view.findViewById(R.id.txtYear);
        txtYear.setText(history.year);
        TextView txtMake = (TextView)view.findViewById(R.id.txtMake);
        txtMake.setText(history.make);
        TextView txtModel = (TextView)view.findViewById(R.id.txtModel);
        txtModel.setText(history.model);
        return view;
    }
}
