package com.hufna.intellisens;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by scottholliday on 5/4/16.
 */
public class ActivityHistory extends Activity{
    ModuleDB moduleDB;
    ArrayList<History> checkHistories;
    ArrayList<History> createHistories;
    ArrayList<History> copyHistories;
    ArrayList<History> shownHistories;
    @Override
    public void onCreate(Bundle icicle){
        super.onCreate(icicle);
        moduleDB = new ModuleDB(this, "vehicles.sqlite", null, 1);
        try{
            moduleDB.openDatabase();
        }catch(IOException e){
            Toast.makeText(this, getString(R.string.m_unableDB), Toast.LENGTH_SHORT).show();
        }
        setContentView(R.layout.activity_history);
        checkHistories = moduleDB.getHistory(0);
        Collections.sort(checkHistories, new DateComparator());
        createHistories = moduleDB.getHistory(1);
        Collections.sort(createHistories, new DateComparator());
        copyHistories = moduleDB.getHistory(2);
        Collections.sort(copyHistories, new DateComparator());
        TextView txtTitle = (TextView)findViewById(R.id.txtTitle);
        txtTitle.setText(getString(R.string.m_history));
        RadioButton rdoCheck = (RadioButton)findViewById(R.id.rdoCheck);
        rdoCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    shownHistories = checkHistories;
                    setupList();
                }
            }
        });
        RadioButton rdoCreate = (RadioButton)findViewById(R.id.rdoCreate);
        rdoCreate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    shownHistories = createHistories;
                    setupList();
                }
            }
        });
        RadioButton rdoCopy = (RadioButton)findViewById(R.id.rdoCopy);
        rdoCopy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    shownHistories = copyHistories;
                    setupList();
                }
            }
        });
        rdoCheck.setChecked(true);
        setupList();
    }
    public void setupList(){
        ListView lstHistories = (ListView)findViewById(R.id.lstHistories);
        AdapterHistories ada = new AdapterHistories(this, shownHistories);
        lstHistories.setAdapter(ada);
    }
    public class DateComparator implements Comparator<History>{
        public int compare(History left, History right){
            return right.date.compareTo(left.date);
        }
    }
}
