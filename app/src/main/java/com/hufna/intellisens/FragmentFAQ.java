package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class FragmentFAQ extends Fragment {
    ActivityMore act;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMore)getActivity();
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        setListContent(view);
        return view;
    }
    private void setListContent(View view){
        ArrayList<String> allItems = new ArrayList<String>();
        allItems.add("que:1. " + getString(R.string.m_queOne));
        allItems.add("ans:" + getString(R.string.m_ansOne));
        allItems.add("que:2. " + getString(R.string.m_queTwo));
        allItems.add("ans:" + getString(R.string.m_ansTwo));
        allItems.add("que:3. " + getString(R.string.m_queThree));
        allItems.add("ans:" + getString(R.string.m_ansThree));
        allItems.add("que:4. " + getString(R.string.m_queFour));
        allItems.add("ans:" + getString(R.string.m_ansFour));
        allItems.add("que:5. " + getString(R.string.m_queFive));
        allItems.add("ans:" + getString(R.string.m_ansFive));
        allItems.add("que:6. " + getString(R.string.m_queSix));
        allItems.add("ans:" + getString(R.string.m_ansSix));
        allItems.add("que:7. " + getString(R.string.m_queSeven));
        allItems.add("ans:" + getString(R.string.m_ansSeven));
        allItems.add("que:8. " + getString(R.string.m_queEight));
        allItems.add("ans:" + getString(R.string.m_ansEight));
        allItems.add("que:9. " + getString(R.string.m_queNine));
        allItems.add("ans:" + getString(R.string.m_ansNine));
        ListView lstFAQ = (ListView)view.findViewById(R.id.lstFAQ);
        AdapterFAQ ada = new AdapterFAQ(act.getApplicationContext(), allItems);
        lstFAQ.setAdapter(ada);
        lstFAQ.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            Intent i;
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3){
                switch(position){
                    case 11:
                        i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.products.intellisens.com/ConfigureIntellisens"));
                        startActivity(i);
                        break;
                    case 17:
                        i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.intellisens.com/index.php?id=342&no_cache=1"));
                        startActivity(i);
                        break;
                }
            }
        });
    }
}