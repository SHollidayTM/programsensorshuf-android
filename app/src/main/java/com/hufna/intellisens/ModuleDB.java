package com.hufna.intellisens;

/**
 * Created by sholliday on 12/8/15.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ModuleDB extends SQLiteOpenHelper{
    Context ctx;
    String dbName;
    SQLiteDatabase db;
    File file;
    public ModuleDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
        ctx = context;
        dbName = name;
    }
    public void openDatabase() throws IOException{
        file = new File(ctx.getFilesDir(), dbName);
        if(!file.exists()){
            copyDB();
        }else{
            db = SQLiteDatabase.openDatabase(file.getPath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
            Cursor crs = db.rawQuery("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'version'", null);
            if(!crs.moveToFirst()){
                db.close();
                file.delete();
                copyDB();
            }else{
                Cursor crs2 = db.rawQuery("SELECT ID FROM version", null);
                if(crs2.moveToFirst()){
                    String strId = crs2.getString(0);
                    if(!strId.equals(BuildConfig.VERSION_NAME)){
                        //here we'll eventually have to transfer data, but for now, just replace database
                        db.close();
                        file.delete();
                        copyDB();
                    }
                }
                crs2.close();
            }
            crs.close();
        }
        db = SQLiteDatabase.openDatabase(file.getPath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
    }
    private void copyDB(){
        try{
            InputStream input = ctx.getAssets().open(dbName);
            OutputStream output = new FileOutputStream(file.getPath());
            byte[] buffer = new byte[1024];
            int length;
            while((length = input.read(buffer)) > 0){
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            input.close();
        }catch(IOException e){
        }
    }
    @Override
    public synchronized void close(){
        if(db != null){
            db.close();
        }
        super.close();
    }
    @Override
    public void onCreate(SQLiteDatabase db){
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
    }
    public ArrayList<String> getAllYears(){
        ArrayList<String> years = new ArrayList<String>();
        years.add(ctx.getString(R.string.m_selectYear));
        try{
            Cursor crs = db.rawQuery("SELECT DISTINCT YearID FROM ymm ORDER BY YearID DESC", null);
            while(crs.moveToNext()){
                int year = crs.getInt(0);
                years.add(Integer.toString(year));
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return years;
    }
    public ArrayList<Make> getMakesByYear(String year){
        ArrayList<Make> makes = new ArrayList<Make>();
        try{
            Cursor crs = db.rawQuery("SELECT DISTINCT m.ID, m.Make FROM makes m JOIN ymm y ON m.ID = y.MakeID WHERE y.YearID = ? ORDER BY m.Make ASC", new String[] {year});
            while(crs.moveToNext()){
                Make make = new Make();
                make.identifier = crs.getInt(0);
                make.makeName = crs.getString(1).trim();
                makes.add(make);
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return makes;
    }
    public ArrayList<Model> getModelsByYearAndMake(String year, Make make){
        ArrayList<Model> models = new ArrayList<Model>();
        try{
            Cursor crs = db.rawQuery("SELECT DISTINCT m.ID, m.Model FROM models m JOIN ymm y ON m.ID = y.ModelID WHERE y.YearID = ? AND y.MakeID = ? ORDER BY m.Model ASC", new String[] {year, Integer.toString(make.identifier)});
            while(crs.moveToNext()){
                Model model = new Model();
                model.identifier = crs.getInt(0);
                model.modelName = crs.getString(1).trim();
                models.add(model);
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return models;
    }
    public Make getMakeByMakeName(String makeName){
        Make make = new Make();
        try{
            Cursor crs = db.rawQuery("SELECT ID, Make FROM makes WHERE Make = ?", new String[] {makeName});
            while(crs.moveToNext()){
                make.identifier = crs.getInt(0);
                make.makeName = crs.getString(1).trim();
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return make;
    }
    public Model getModelByModelName(String modelName){
        Model model = new Model();
        try{
            Cursor crs = db.rawQuery("SELECT ID, Model FROM models WHERE Model = ?", new String[] {modelName});
            while(crs.moveToNext()){
                model.identifier = crs.getInt(0);
                model.modelName = crs.getString(1).trim();
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return model;
    }
    public ArrayList<VehicleConfig> getQualifiersByYearMakeAndModel(String year, Make make, Model model){
        ArrayList<VehicleConfig> vehicleConfigs = new ArrayList<VehicleConfig>();
        try{
            Cursor crs = db.rawQuery("SELECT ConfigID, Qualifier, Indirect FROM ymm WHERE YearID = ? AND MakeID = ? AND ModelID = ? GROUP BY Qualifier", new String[] {year, Integer.toString(make.identifier), Integer.toString(model.identifier)});
            while(crs.moveToNext()){
                VehicleConfig vehicleConfig = new VehicleConfig();
                vehicleConfig.configID = crs.getInt(0);
                vehicleConfig.qualifier = "";
                if(!crs.isNull(1)){
                    vehicleConfig.qualifier = crs.getString(1).trim();
                }
                if(crs.getInt(2) > 0){
                    vehicleConfig.indirect = true;
                }
                vehicleConfigs.add(vehicleConfig);
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return vehicleConfigs;
    }
    public Vehicle getVehicleByConfigID(int configID){
        Vehicle vehicle = new Vehicle();
        try{
            String strQuery = "SELECT y.ConfigID, y.YearID, ma.ID, ma.Make, mo.ID, mo.Model FROM ymm y JOIN makes ma ON y.MakeID = ma.ID JOIN models mo ON y.ModelID = mo.ID WHERE y.ConfigID = " + configID;
            Cursor crs = db.rawQuery(strQuery, null);
            if(crs.moveToFirst()){
                vehicle.configID = crs.getInt(0);
                vehicle.yearID = crs.getInt(1);
                vehicle.makeID = crs.getInt(2);
                vehicle.makeName = crs.getString(3).trim();
                vehicle.makeID = crs.getInt(4);
                vehicle.modelName = crs.getString(5).trim();
                vehicle.commandWords = new ArrayList<CommandWord>();
                String strQuery2 = "SELECT c.CheckWord, c.SelectWord, c.SoftwareVersion, sw.LowByteZero, sw.LowByteOne, sw.LowByteTwo, sw.LowByteThree, sw.HighByteZero, sw.HighByteOne, sw.HighByteTwo, sw.HighByteThree FROM commands c JOIN ymmcommands y ON c.ID = y.CommandID LEFT JOIN selectwordidrange sw ON c.SelectWord = sw.SelectWord WHERE y.ConfigID = " + vehicle.configID;
                Cursor crs2 = db.rawQuery(strQuery2, null);
                while(crs2.moveToNext()){
                    CommandWord cmdWord = new CommandWord();
                    cmdWord.checkWord = crs2.getInt(0);
                    cmdWord.selectWord = crs2.getInt(1);
                    cmdWord.softwareVersion = crs2.getInt(2);
                    if(!crs2.isNull(3)){
                        cmdWord.hasRange = true;
                        cmdWord.lowByteZero = crs2.getInt(3);
                        cmdWord.lowByteOne = crs2.getInt(4);
                        cmdWord.lowByteTwo = crs2.getInt(5);
                        cmdWord.lowByteThree = crs2.getInt(6);
                        cmdWord.highByteZero = crs2.getInt(7);
                        cmdWord.highByteOne = crs2.getInt(8);
                        cmdWord.highByteTwo = crs2.getInt(9);
                        cmdWord.highByteThree = crs2.getInt(10);
                    }
                    vehicle.commandWords.add(cmdWord);
                }
                vehicle.sensorNameFreqs = new ArrayList<SensorNameFreq>();
                strQuery2 = "SELECT s.Name, s.Frequency, s.Material FROM sensors s JOIN ymmsensors y ON s.ID = y.SensorID WHERE y.ConfigID = " + vehicle.configID;
                crs2 = db.rawQuery(strQuery2, null);
                while(crs2.moveToNext()){
                    SensorNameFreq sensorNameFreq = new SensorNameFreq();
                    sensorNameFreq.name = crs2.getString(0);
                    sensorNameFreq.frequency = crs2.getString(1);
                    sensorNameFreq.valveMaterial = crs2.getString(2);
                    vehicle.sensorNameFreqs.add(sensorNameFreq);
                }
                crs2.close();
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return vehicle;
    }
    public ArrayList<Vehicle> getVehiclesByConfigIDs(String configIDs){
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
        try{
            String strQuery = "SELECT y.ConfigID, y.YearID, ma.ID, ma.Make, mo.ID, mo.Model FROM ymm y JOIN makes ma ON y.MakeID = ma.ID JOIN models mo ON y.ModelID = mo.ID WHERE y.ConfigID IN " + configIDs;
            Cursor crs = db.rawQuery(strQuery, null);
            while(crs.moveToNext()){
                Vehicle vehicle = new Vehicle();
                vehicle.yearID = crs.getInt(1);
                vehicle.makeID = crs.getInt(2);
                vehicle.makeName = crs.getString(3).trim();
                vehicle.makeID = crs.getInt(4);
                vehicle.modelName = crs.getString(5).trim();
                vehicles.add(vehicle);
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return vehicles;
    }
    public ArrayList<Attachment> getAttachmentsForVehicle(Vehicle vehicle){
        ArrayList<Attachment> attachments = new ArrayList<Attachment>();
        try{
            String strQuery = "SELECT a.Level, a.Name, a.Text, a.Url FROM attachments a JOIN ymmattachments ya ON a.ID = ya.AttachmentID WHERE ya.ConfigID = " + vehicle.configID;
            Cursor crs = db.rawQuery(strQuery, null);
            while(crs.moveToNext()){
                Attachment attachment = new Attachment();
                attachment.level = crs.getString(0).trim();
                attachment.name = crs.getString(1).trim();
                attachment.text = crs.getString(2).trim();
                attachment.url = crs.getString(3).trim();
                attachments.add(attachment);
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return attachments;
    }
    public void addAttachments(ArrayList<Attachment> attachments){
        for(Attachment attachment : attachments){
            ContentValues cv = new ContentValues();
            cv.put("ID", attachment.attachmentID);
            cv.put("Level", attachment.level);
            cv.put("Name", attachment.name);
            cv.put("Text", attachment.text);
            cv.put("Url", attachment.url);
            db.insert("attachments", null, cv);
        }
    }
    public void addVehicles(ArrayList<Vehicle> vehicles){
        for(Vehicle vehicle : vehicles){
            ContentValues cv = new ContentValues();
            cv.put("YearID", vehicle.yearID);
            cv.put("MakeID", vehicle.makeID);
            cv.put("ModelID", vehicle.modelID);
            cv.put("QualifierID", vehicle.qualifierID);
            cv.put("Qualifier", vehicle.qualifier);
            cv.put("Indirect", vehicle.indirect);
            String strQuery = "SELECT ConfigID FROM ymm WHERE ConfigID = " + vehicle.configID;
            Cursor crs = db.rawQuery(strQuery, null);
            if(crs.moveToFirst()){
                db.update("ymm", cv, "ConfigID=" + vehicle.configID, null);
            }else{
                cv.put("ConfigID", vehicle.configID);
                db.insert("ymm", null, cv);
            }
            crs.close();
            cv = new ContentValues();
            cv.put("ID", vehicle.makeID);
            cv.put("Make", vehicle.makeName);
            db.insert("makes", null, cv);
            cv = new ContentValues();
            cv.put("ID", vehicle.modelID);
            cv.put("Model", vehicle.modelName);
            db.insert("models", null, cv);
            if(vehicle.commandWords.size() > 0){
                for(CommandWord cmdWord : vehicle.commandWords){
                    strQuery = "SELECT ID FROM commands WHERE CheckWord = " + cmdWord.checkWord + " AND SelectWord = " + cmdWord.selectWord + " AND SoftwareVersion = " + cmdWord.softwareVersion;
                    crs = db.rawQuery(strQuery, null);
                    int cmdID = 0;
                    if(crs.moveToFirst()){
                        cmdID = crs.getInt(0);
                    }else{
                        cv = new ContentValues();
                        cv.put("CheckWord", cmdWord.checkWord);
                        cv.put("SelectWord", cmdWord.selectWord);
                        cv.put("SoftwareVersion", cmdWord.softwareVersion);
                        cmdID = (int)db.insert("commands", null, cv);
                    }
                    crs.close();
                    strQuery = "SELECT ConfigID, CommandID FROM ymmcommands WHERE ConfigID = " + vehicle.configID + " AND CommandID = " + cmdID;
                    crs = db.rawQuery(strQuery, null);
                    if(!crs.moveToFirst()){
                        cv = new ContentValues();
                        cv.put("ConfigID", vehicle.configID);
                        cv.put("CommandID", cmdID);
                        db.insert("ymmcommands", null, cv);
                    }
                    crs.close();
                }
            }
            for(int attID : vehicle.attachmentIDs){
                strQuery = "SELECT ConfigID, AttachmentID FROM ymmattachments WHERE ConfigID = " + vehicle.configID + " AND AttachmentID = " + attID;
                crs = db.rawQuery(strQuery, null);
                if(!crs.moveToFirst()){
                    cv = new ContentValues();
                    cv.put("ConfigID", vehicle.configID);
                    cv.put("AttachmentID", attID);
                    db.insert("ymmattachments", null, cv);
                }
                crs.close();
            }
            if(vehicle.sensorNameFreqs.size() > 0){
                for(SensorNameFreq sensorNameFreq : vehicle.sensorNameFreqs){
                    strQuery = "SELECT ID FROM sensors WHERE Name = '" + sensorNameFreq.name + "' AND Frequency = '" + sensorNameFreq.frequency + "'";
                    crs = db.rawQuery(strQuery, null);
                    int snfID = 0;
                    if(crs.moveToFirst()){
                        snfID = crs.getInt(0);
                    }else{
                        cv = new ContentValues();
                        cv.put("Name", sensorNameFreq.name);
                        cv.put("Frequency", sensorNameFreq.frequency);
                        snfID = (int)db.insert("sensors", null, cv);
                    }
                    crs.close();
                    strQuery = "SELECT ConfigID, SensorID FROM ymmsensors WHERE ConfigID = " + vehicle.configID + " AND SensorID = " + snfID;
                    crs = db.rawQuery(strQuery, null);
                    if(!crs.moveToFirst()){
                        cv = new ContentValues();
                        cv.put("ConfigID", vehicle.configID);
                        cv.put("SensorID", snfID);
                        db.insert("ymmsensors", null, cv);
                    }
                    crs.close();
                }
            }
        }
    }
    public void addHistory(int configID, int action){
        ContentValues cv = new ContentValues();
        cv.put("ConfigID", configID);
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
        String date = sdf.format(new Date());
        cv.put("Date", date);
        cv.put("ActionID", action);
        db.insert("history", null, cv);
    }
    public ArrayList<History> getHistory(int action){
        ArrayList<History> histories = new ArrayList<History>();
        String strQuery = "SELECT h.Date, y.YearID, ma.Make, mo.Model, a.Action FROM history h JOIN ymm y ON h.ConfigID = y.ConfigID JOIN makes ma ON y.MakeID = ma.ID JOIN models mo ON y.ModelID = mo.ID JOIN actions a ON h.ActionID = a.ID WHERE h.ActionID = " + action;
        Cursor crs = db.rawQuery(strQuery, null);
        while(crs.moveToNext()){
            History history = new History();
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
            try{
                history.date = sdf.parse(crs.getString(0));
            }catch(ParseException e){
                e.printStackTrace();
            }
            history.year = crs.getString(1);
            history.make = crs.getString(2).trim();
            history.model = crs.getString(3).trim();
            history.action = crs.getString(4).trim();
            histories.add(history);
        }
        return histories;
    }
    public ArrayList<Action>getActions(){
        ArrayList<Action> actions = new ArrayList<Action>();
        String strQuery = "SELECT ConfigID, ActionID FROM history";
        Cursor crs = db.rawQuery(strQuery, null);
        while(crs.moveToNext()){
            Action action = new Action();
            action.configID = crs.getInt(0);
            action.actionID = crs.getInt(1);
            actions.add(action);
        }
        return actions;
    }
    public void deleteHistory(){
        db.delete("history", null, null);
    }
    public String getPartNumByUpc(String upc){
        try{
            Cursor crs = db.rawQuery("SELECT PartNum FROM upcpartnums WHERE Upc = ?", new String[] {upc});
            if(crs.moveToNext()){
                return crs.getString(0).trim();
            }
            crs.close();
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return "";
    }
}