package com.hufna.intellisens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

class AdapterFAQ extends BaseAdapter {
    Context ctx;
    ArrayList items;
    private static LayoutInflater inflater = null;

    public AdapterFAQ(Context context, ArrayList allItems) {
        ctx = context;
        items = allItems;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public Object getItem(int position) {
        return items.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        String str = (String)items.get(position);
        if(str.contains("que:")){
            str = str.replace("que:", "");
            view = inflater.inflate(R.layout.header_question, null);
            TextView txtHeader = (TextView)view.findViewById(R.id.txtQue);
            txtHeader.setText(str);
        }else{
            str = str.replace("ans:", "");
            view = inflater.inflate(R.layout.item_answer, null);
            TextView txtName = (TextView)view.findViewById(R.id.txtAns);
            txtName.setText(str);
        }
        return view;
    }
}
