package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by sholliday on 12/23/15.
 */
public class FragmentTroubleshooting extends Fragment{
    ActivityMain act;
    WebView webTroubleshooting;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_troubleshooting, container, false);
        TextView txtDesc = (TextView)view.findViewById(R.id.txtDesc);
        txtDesc.setText(getString(R.string.m_troubleInst) + " " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        webTroubleshooting = (WebView)view.findViewById(R.id.webTroubleshooting);
        webTroubleshooting.setWebViewClient(webClient);
        return view;
    }
    public void loadTipsAndWarnings(){
        String message = "";
        for(Attachment attachment : act.attachments){
            byte[] data = Base64.decode(attachment.text, Base64.DEFAULT);
            String text = null;
            try {
                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            message = message + attachment.name + "</br>" + text + "</br>";
            message = message + "</br><a href=\"" + attachment.url + "\">Click here to view</a></br></br>";
        }
        webTroubleshooting.loadData(message, "text/html", null);
    }
    WebViewClient webClient = new WebViewClient(){
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            if(url.contains(".pdf")){
                String key = act.preferences.getString("securityKey", "");
                try {
                    key = URLEncoder.encode(key, "utf-8").replace("+", "%2B");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                url = url.replace("{SecurityKey}", key);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                return true;
            }
            return false;
        }
        @Override
        public void onLoadResource(WebView view, String url){
            if(url.substring(0, 4).equals("http")){
                if(url.contains(".pdf")){
                    String strUrl = "https://docs.google.com/gview?embedded=true&url=" + url;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(strUrl));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                }
            }
        }
    };
}