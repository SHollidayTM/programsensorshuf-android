package com.hufna.intellisens;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by sholliday on 12/8/15.
 */
public class FragmentByVIN extends Fragment{
    ActivityMain act;
    EditText txtVIN;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain) getActivity();
        View view = inflater.inflate(R.layout.fragment_byvin, container, false);
        txtVIN = (EditText)view.findViewById(R.id.txtVIN);
        txtVIN.setOnEditorActionListener(edtTxtVIN);
        Button btnScan = (Button)view.findViewById(R.id.btnScan);
        btnScan.setOnClickListener(clkScan);
        Button btnGo = (Button)view.findViewById(R.id.btnGo);
        btnGo.setOnClickListener(clkGo);
        return view;
    }
    TextView.OnEditorActionListener edtTxtVIN = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if(actionId == EditorInfo.IME_ACTION_GO){
                InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                go();
                return true;
            }
            return false;
        }
    };
    View.OnClickListener clkScan = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.scanForVin = true;
            IntentIntegrator integrator = new IntentIntegrator(act);
            integrator.initiateScan();
        }
    };
    View.OnClickListener clkGo = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            go();
        }
    };
    public void go(){
        new VINtoYMM(txtVIN.getText().toString()).execute();
    }
    private class VINtoYMM extends AsyncTask<Void, Void, Void> {
        ProgressDialog prgDialog;
        String vin;
        StringBuilder response = new StringBuilder();
        public VINtoYMM(String sentVin){
            prgDialog = new ProgressDialog(act);
            prgDialog.setCancelable(false);
            vin = sentVin;
        }
        @Override
        protected void onPreExecute(){
            prgDialog.setMessage(getString(R.string.m_pleaseWait));
            prgDialog.show();
            prgDialog.setCancelable(false);
            prgDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.m_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    VINtoYMM.this.cancel(true);
                    dialog.dismiss();
                }
            });
        }
        @Override
        protected Void doInBackground(Void... params){
            String soapMsg = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:description7a.services.chrome.com\"><soapenv:Header/><soapenv:Body><urn:VehicleDescriptionRequest><urn:accountInfo number=\"283234\" secret=\"845b638aea2d4f37\" country=\"US\" language=\"en\" behalfOf=\"?\"/><urn:vin>" + vin + "</urn:vin></urn:VehicleDescriptionRequest></soapenv:Body></soapenv:Envelope>";
            try{
                URL url = new URL("http://services.chromedata.com/Description/7a?wsdl");
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "text/xml;charset=utf-8");
                connection.setRequestProperty("Content-Length", Integer.toString(soapMsg.length()));
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(soapMsg);
                writer.close();
                String line;
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while((line = reader.readLine()) != null){
                    response.append(line);
                }
                reader.close();
            }catch(IOException e){
                showError();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result){
            parseXml(response.toString());
            if(prgDialog.isShowing()){
                prgDialog.dismiss();
            }
        }
    }
    public void parseXml(String xml){
        boolean foundVIN = false;
        try{
            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();
            InputStream input = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            parser.setInput(input, null);
            int event = parser.getEventType();
            while(event != XmlPullParser.END_DOCUMENT){
                String name = parser.getName();
                switch(event){
                    case XmlPullParser.START_TAG:
                        if(name.equals("vinDescription")){
                            foundVIN = true;
                            String year = parser.getAttributeValue(null, "modelYear");
                            String make = parser.getAttributeValue(null, "division");
                            String model = parser.getAttributeValue(null, "modelName");
                            act.byYMMFromVIN(year, make, model);
                        }
                        break;
                }
                event = parser.next();
            }
            if(!foundVIN){
                Toast.makeText(act, getString(R.string.m_invalidVIN), Toast.LENGTH_SHORT).show();
            }
        }catch(XmlPullParserException e){
            showError();
        }catch (UnsupportedEncodingException e){
            showError();
        }catch (IOException e){
            showError();
        }
    }
    public void showError(){
        Toast.makeText(act, getString(R.string.m_errorTryAgain), Toast.LENGTH_SHORT).show();
    }
}
