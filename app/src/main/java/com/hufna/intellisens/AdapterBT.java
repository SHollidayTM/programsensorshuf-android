package com.hufna.intellisens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/16/15.
 */
public class AdapterBT extends BaseAdapter{
    Context ctx;
    ArrayList items;
    private static LayoutInflater inflater = null;
    public AdapterBT(Context context, ArrayList allItems){
        ctx = context;
        items = allItems;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public Object getItem(int position) {
        return items.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_bt, null);
        BTObject btObject = (BTObject)items.get(position);
        TextView txtName = (TextView)view.findViewById(R.id.txtName);
        if(btObject.serialNum != null){
            txtName.setText(btObject.name + " > " + ctx.getString(R.string.m_serialNo) + " " + btObject.serialNum);
        }else{
            txtName.setText(btObject.name);
        }
        TextView txtAddress = (TextView) view.findViewById(R.id.txtAddress);
        if((btObject.firmwareVersion != null) && (btObject.hardwareVersion != null) && (btObject.databaseVersion != null)){
            txtAddress.setText(ctx.getString(R.string.m_bluetoothID) + " " + btObject.address + " > " + ctx.getString(R.string.m_firmwareVersion) + " " + btObject.firmwareVersion);
        }else{
            txtAddress.setText(ctx.getString(R.string.m_bluetoothID) + " " + btObject.address);
        }
        ImageView imvCheck = (ImageView)view.findViewById(R.id.imvCheck);
        if(!btObject.connected) {
            imvCheck.setVisibility(View.INVISIBLE);
        }
        return view;
    }
}
