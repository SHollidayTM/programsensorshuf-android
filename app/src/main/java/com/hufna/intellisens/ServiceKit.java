package com.hufna.intellisens;

/**
 * Created by sholliday on 12/18/15.
 */
public class ServiceKit{
    String catalogID;
    String partNumber;
    String name;
    String category;
    String sameAsNumber;
    String alternateNumber;
    String description;
    String relatedPartNumber;
    String manufacturer;
    String finish;
    String cap;
    String hexNut;
    String metalWasher;
    boolean metalValveIncluded;
    boolean completeValveAssembly;
    String core;
    String torqueCore;
    String torqueNut;
    String torqueScrew;
    String grommet;
    String valveLength;
}