package com.hufna.intellisens;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

/**
 * Created by sholliday on 12/17/15.
 */
public class FragmentService extends Fragment {
    ActivityMain act;
    ImageButton btnCheck;
    ImageButton btnProgram;
    public boolean checking;
    public boolean criticalAlertShown;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        TextView txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        txtTitle.setText(act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        TextView txtDesc = (TextView)view.findViewById(R.id.txtDesc);
        btnCheck = (ImageButton)view.findViewById(R.id.btnCheck);
        btnCheck.setOnClickListener(clkCheck);
        btnProgram = (ImageButton)view.findViewById(R.id.btnProgram);
        btnProgram.setOnClickListener(clkProgram);
        act.vehicle = act.moduleDB.getVehicleByConfigID(act.selectedVehicleConfig.configID);
        if(act.sensor != null){
            for(SensorNameFreq sensorNameFreq : act.vehicle.sensorNameFreqs){
                if(sensorNameFreq.name.equals(act.sensor.name)){
                    act.sensorNameFreq = sensorNameFreq;
                }
            }
        }
        WebView webVehicle = (WebView)view.findViewById(R.id.webVehicle);
        String strUrl = "http://d2ef5twh46wdyk.cloudfront.net/us/chromeveh/" + act.selectedYear + "/_" + act.selectedMake.identifier + "_" + act.selectedModel.identifier + ".jpg";
        String html = "<img src=\"" + strUrl +"\" style=\"width:100%;\">";
        webVehicle.loadData(html, "text/html", null);
        LinearLayout layAdd = (LinearLayout) view.findViewById(R.id.layAdd);
        LinearLayout layRotate = (LinearLayout)view.findViewById(R.id.layRotate);
        LinearLayout layReplace = (LinearLayout)view.findViewById(R.id.layReplace);
        if(act.relearn != null){
            TextView txtAdd = (TextView)view.findViewById(R.id.txtAdd);
            TextView txtRotate = (TextView)view.findViewById(R.id.txtRotate);
            TextView txtReplace = (TextView)view.findViewById(R.id.txtReplace);
            if(act.relearn != null){
                txtAdd.setText(act.relearn.addAir);
                txtRotate.setText(act.relearn.rotateTires);
                txtReplace.setText(act.relearn.replaceSensor);
            }
        }else{
            layAdd.setVisibility(View.INVISIBLE);
            layRotate.setVisibility(View.INVISIBLE);
            layReplace.setVisibility(View.INVISIBLE);
            txtDesc.setText(getString(R.string.m_noNetRelearn));

        }
        if(act.selectedVehicleConfig.indirect){
            LinearLayout layButtons = (LinearLayout)view.findViewById(R.id.layButtons);
            layButtons.setVisibility(View.GONE);
            LinearLayout layRelearn = (LinearLayout)view.findViewById(R.id.layRelearn);
            TextView txtIndirect = new TextView(act);
            txtIndirect.setText(getString(R.string.m_hasIndirect));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL);
            params.setMargins(8, 8, 8, 8);
            txtIndirect.setLayoutParams(params);
            txtIndirect.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            layRelearn.addView(txtIndirect);
            if(act.relearn != null){
                Button btnRelearnInst = new Button(act);
                btnRelearnInst.setText(act.getString(R.string.m_instructions));
                btnRelearnInst.setLayoutParams(params);
                btnRelearnInst.setOnClickListener(clkRelearn);
                layRelearn.addView(btnRelearnInst);
            }
        }else{
            if(act.relearn != null){
                ImageButton btnRelearn = (ImageButton)view.findViewById(R.id.btnRelearn);
                btnRelearn.setOnClickListener(clkRelearn);
            }else{
                LinearLayout layRelearnBtn = (LinearLayout)view.findViewById(R.id.layRelearnBtn);
                layRelearnBtn.setVisibility(View.GONE);
            }
        }
        act.attachments = act.moduleDB.getAttachmentsForVehicle(act.vehicle);
        if(!criticalAlertShown){
            if(act.attachments.size() > 0){
                String message = "";
                boolean showIt = false;
                for(Attachment attachment : act.attachments){
                    if(attachment.level.equals("Critical Alert")){
                        showIt = true;
                        byte[] data = Base64.decode(attachment.text, Base64.DEFAULT);
                        String text = null;
                        try {
                            text = new String(data, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        message = message + attachment.name + "</br>" + text + "</br></br>";
                    }
                }
                if(showIt){
                    WebView webView = new WebView(act);
                    webView.setWebViewClient(webClient);
                    webView.loadData(message, "text/html", "UTF-8");
                    AlertDialog.Builder alert = new AlertDialog.Builder(act);
                    alert.setTitle(getString(R.string.m_criticalAlert));
                    alert.setView(webView);
                    alert.setCancelable(true);
                    alert.show();
                }
            }
        }
        return view;
    }
    View.OnClickListener clkCheck = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checking = true;
            if(act.vehicle.commandWords.size() > 0){
                CommandWord cmdWord = act.vehicle.commandWords.get(0);
                if(cmdWord.checkWord != 0){
                    if(act.btSocket != null){
                        if(act.btSocket.isConnected()){
                            act.openCheck();
                        }else{
                            suggestBT();
                        }
                    }else{
                        suggestBT();
                    }
                }else{
                    Toast.makeText(act, getString(R.string.m_noProtocol), Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(act, getString(R.string.m_noProtocol), Toast.LENGTH_SHORT).show();
            }
        }
    };
    View.OnClickListener clkProgram = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checking = false;
            if(act.vehicle.commandWords.size() > 0){
                CommandWord cmdWord = act.vehicle.commandWords.get(0);
                if(cmdWord.selectWord != 0){
                    if((act.vehicle.sensorNameFreqs.size() > 0) || (act.sensor != null)){
                        if(act.btSocket != null){
                            if(act.btSocket.isConnected()){
                                act.openProgram();
                            }else{
                                suggestBT();
                            }
                        }else{
                            suggestBT();
                        }
                    }else{
                        Toast.makeText(act, getString(R.string.m_noProgrammables), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(act, getString(R.string.m_noProtocol), Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(act, getString(R.string.m_noProtocol), Toast.LENGTH_SHORT).show();
            }
        }
    };
    View.OnClickListener clkRelearn = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(act.relearn.instructions != null){
                act.openRelearnInst();
            }else{
                Toast.makeText(act, getString(R.string.m_noRelearnInst), Toast.LENGTH_SHORT).show();
            }
        }
    };
    public void suggestBT(){
        act.checkBT();
    }
    WebViewClient webClient = new WebViewClient(){
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            return false;
        }
        @Override
        public void onLoadResource(WebView view, String url){
            if(url.substring(0, 4).equals("http")){
                if(url.contains(".pdf")){
                    String strUrl = "https://docs.google.com/gview?embedded=true&url=" + url;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(strUrl));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                }
            }
        }
    };
}
