package com.hufna.intellisens;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

public class ActivityMain extends AppCompatActivity {
    ModuleDB moduleDB;
    LinearLayout laySplash;
    LinearLayout layButtons;
    boolean buttonsMoved;
    LinearLayout layFrags;
    LinearLayout laySearch;
    ImageButton btnProdSearch;
    ImageButton btnTPMSServ;
    ImageButton btnByYMM;
    boolean inService;
    ImageButton btnByVIN;
    ImageButton btnByProd;
    LinearLayout layByProd;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean scanForVin;
    public BluetoothDevice btDevice;
    public BluetoothDevice btDeviceHC;
    public BluetoothDevice btDeviceOBD;
    public String selectedYear;
    public Make selectedMake;
    public Model selectedModel;
    public VehicleConfig selectedVehicleConfig;
    public ArrayList<SensorTPMS> programmables;
    public ArrayList<SensorTPMS> nonprogrammables;
    public ArrayList<ServiceKit> serviceKits;
    public ArrayList<Vehicle> newVehicles;
    public SensorTPMS sensor;
    public SensorNameFreq sensorNameFreq;
    public ServiceKit serviceKit;
    public Relearn relearn;
    public ArrayList<Attachment> attachments;
    public Bitmap bmpDevice;
    public String productNum;
    public ModuleBT moduleBTHC;
    public ModuleBT moduleBTOBD;
    BluetoothSocket btSocket;
    BluetoothSocket btSocketHC;
    BluetoothSocket btSocketOBD;
    public ProgressDialog prgDialogHC;
    FragmentService fragService;
    FragmentCheck fragCheck;
    FragmentCreate fragCreate;
    FragmentCopy fragCopy;
    FragmentCopySet fragCopySet;
    FragmentOBD fragOBD;
    int copySensor;
    public Vehicle vehicle;
    byte checkWord;
    byte mhz;
    public boolean checkingFreq;
    public boolean creating;
    public boolean freqChosen;
    boolean fromVIN;
    public String serialNumHC;
    public String firmwareVersionHC;
    public String hardwareVersionHC;
    public String databaseVersionHC;
    public String serialNumOBD;
    public String firmwareVersionOBD;
    public String hardwareVersionOBD;
    public String databaseVersionOBD;
    NetworkInfo activeNetwork;
    boolean obdFirmware;
    public boolean obdCheck;
    ImageView imvBluetooth;
    int cameFromCopyOrSet;
    String webNewsUrl = "https://portal.tpmsmanager.com/hufnews/";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        preferences = getApplicationContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        editor = preferences.edit();
        String lang = "en";
        int language = preferences.getInt("language", 0);
        if(language != 0){
            switch(language){
                case 2:
                    lang = "es";
                    break;
                case 3:
                    lang = "fr";
                    break;
            }
            Locale locale = new Locale(lang);
            Resources res = getResources();
            Configuration conf = res.getConfiguration();
            conf.locale = locale;
            res.updateConfiguration(conf, null);
        }
        setContentView(R.layout.activity_main);
        moduleDB = new ModuleDB(this, "vehicles.sqlite", null, 1);
        try{
            moduleDB.openDatabase();
        }catch(IOException e){
            Toast.makeText(this, getString(R.string.m_unableDB), Toast.LENGTH_SHORT).show();
        }
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setLogo(R.drawable.top_banner);
        toolbar.setOnClickListener(clkHome);
        layButtons = (LinearLayout)findViewById(R.id.layButtons);
        layFrags = (LinearLayout)findViewById(R.id.layFrags);
        laySearch = (LinearLayout)findViewById(R.id.laySearch);
        btnProdSearch = (ImageButton)findViewById(R.id.btnProdSearch);
        btnProdSearch.setOnClickListener(clkProdSearch);
        btnTPMSServ = (ImageButton)findViewById(R.id.btnTPMSService);
        btnTPMSServ.setOnClickListener(clkTPMSServ);
        btnByYMM = (ImageButton)findViewById(R.id.btnByYMM);
        btnByYMM.setOnClickListener(clkByYMM);
        btnByVIN = (ImageButton)findViewById(R.id.btnByVIN);
        btnByVIN.setOnClickListener(clkByVIN);
        btnByProd = (ImageButton)findViewById(R.id.btnByProd);
        btnByProd.setOnClickListener(clkByProdNum);
        layByProd = (LinearLayout)findViewById(R.id.layByProd);
        laySplash = (LinearLayout)findViewById(R.id.laySplash);
        imvBluetooth = (ImageView)findViewById(R.id.imvBluetooth);
        editor.putString("apiUrl", "https://portal.tpmsmanager.com/hufapi/");
        //editor.putString("newVehicleDate", "2000-01-01");
        if(preferences.getString("newVehicleDate", "").equals("")){
            editor.putString("newVehicleDate", "2017-02-17");
        }
        //
        editor.putInt("registered", 1);
        //
        editor.commit();
        String yearID = preferences.getString("selectedYear", "");
        String makeName = preferences.getString("selectedMake", "");
        String modelName = preferences.getString("selectedModel", "");
        if((yearID.length() > 0) && (makeName.length() > 0) && (modelName.length() > 0)){
            selectedYear = yearID;
            selectedMake = moduleDB.getMakeByMakeName(makeName);
            selectedModel = moduleDB.getModelByModelName(modelName);
        }
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if(!isConnected){
            showNoNetwork();
        }else{
            WebView webNews = (WebView)findViewById(R.id.webNews);
            webNews.getSettings().setJavaScriptEnabled(true);
            webNews.loadUrl(webNewsUrl);
            webNews.setWebViewClient(new webClient());
        }
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        String strLastChecked = preferences.getString("dteLastChecked", null);
        if(strLastChecked == null){
            editor.putString("dteLastChecked", sdf.format(cal.getTime()));
            editor.commit();
        }else{
            try{
                Date dteLastChecked = sdf.parse(strLastChecked);
                if(new Date().after(dteLastChecked)){
                    if(isConnected){
                        checkData();
                        editor.putString("dteLastChecked", sdf.format(cal.getTime()));
                        editor.commit();
                    }
                }
            }catch(ParseException e){
                e.printStackTrace();
            }
        }
        Intent intent = getIntent();
        String action = intent.getAction();
        if(action != null){
            String type = intent.getType();
            if(action.equals(Intent.ACTION_SEND) && type != null){
                if(type.equals("text/plain")){
                    String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                    if(sharedText != null){
                        String[] values = sharedText.split("[|]");
                        if(values[0].equals("find")){
                            byYMMFromVIN(values[1], values[2], values[3]);
                        }else{
                            selectedYear = values[1];
                            selectedMake = moduleDB.getMakeByMakeName(values[2]);
                            selectedModel = moduleDB.getModelByModelName(values[3]);
                            btnTPMSServ.callOnClick();
                        }
                    }
                }
            }
            laySplash.setVisibility(View.INVISIBLE);
        }else{
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run(){
                    laySplash.setVisibility(View.INVISIBLE);
                }
            }, 2000);
        }
    }
    public class webClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            if(!url.equals(webNewsUrl)){
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
                return true;
            }
            return false;
        }
    }
    public void showNoNetwork(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setTitle(getString(R.string.m_noNetworkTitle));
        builder.setMessage(getString(R.string.m_noNetworkText));
        builder.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.action_home){
            goHome();
        }else if(id == R.id.action_settings){
            FragmentSettings fragSettings = new FragmentSettings();
            openFrag(fragSettings, "settings", true);
            return true;
        }else if(id == R.id.action_support){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.m_contactHufSupport));
            //TelephonyManager manager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
            //if(manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
                //builder.setMessage(getString(R.string.m_callSupportOne));
                builder.setMessage("Email support at huf@tpmssupport.com.");
                builder.setPositiveButton(getString(R.string.m_email), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"huf@tpmssupport.com"});
                        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.m_emailFromApp));
                        try{
                            startActivity(i);
                        }catch(android.content.ActivityNotFoundException ex){
                            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
                            builder.setCancelable(true);
                            builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.setTitle(getString(R.string.m_noEmailClient));
                            builder.setMessage(getString(R.string.m_noEmailClientDevice));
                            builder.show();
                        }
                    }
                });
                builder.setNeutralButton(getString(R.string.m_cancel), null);
            /*
            }else{
                builder.setMessage(getString(R.string.m_callSupportTwo));
                builder.setPositiveButton(getString(R.string.m_email), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"huf@tpmssupport.com"});
                        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.m_emailFromApp));
                        try{
                            startActivity(i);
                        }catch(android.content.ActivityNotFoundException ex){
                            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
                            builder.setCancelable(true);
                            builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.setTitle(getString(R.string.m_noEmailClient));
                            builder.setMessage(getString(R.string.m_noEmailClientDevice));
                            builder.show();
                        }
                    }
                });
                builder.setNegativeButton(getString(R.string.m_call), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:18554838767"));
                        startActivity(i);
                    }
                });
                builder.setNeutralButton(getString(R.string.m_cancel), null);
            }
            */
            builder.show();
        }else if(id == R.id.action_more){
            Intent i = new Intent(ActivityMain.this, ActivityMore.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
    View.OnClickListener clkHome = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            goHome();
        }
    };
    private void goHome(){
        closeFrags();
        if(buttonsMoved){
            moveButtons(false);
        }
    }
    View.OnClickListener clkProdSearch = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                inService = false;
                layByProd.setVisibility(View.VISIBLE);
                moveButtons(true);
            }else{
                showNoNetwork();
            }
        }
    };
    View.OnClickListener clkTPMSServ = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            if(btSocketHC != null){
                if(btSocketHC.isConnected()){
                    if(preferences.getInt("registered", 0) == 1){
                        inService = true;
                        layByProd.setVisibility(View.GONE);
                        moveButtons(true);
                    }else{
                        if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                            Intent i = new Intent(ActivityMain.this, ActivityRegister.class);
                            startActivity(i);
                        }else{
                            showNoNetwork();
                        }
                    }
                }else{
                    checkBT();
                }
            }else{
                checkBT();
            }
        }
    };
    public void moveButtons(Boolean down){
        int move = layButtons.getMeasuredHeight();
        TranslateAnimation anim;
        if(down){
            laySearch.invalidate();
            buttonsMoved = true;
            anim = new TranslateAnimation(0, 0, 0, move);
        }else{
            buttonsMoved = false;
            anim = new TranslateAnimation(0, 0, move, 0);
        }
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation){
                layButtons.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationEnd(Animation animation){
                if(buttonsMoved){
                    btnProdSearch.setClickable(false);
                    btnTPMSServ.setClickable(false);
                    layButtons.setClickable(false);
                    layButtons.setVisibility(View.INVISIBLE);
                }else{
                    btnProdSearch.setClickable(true);
                    btnTPMSServ.setClickable(true);
                    layButtons.setClickable(true);
                }
            }
            @Override
            public void onAnimationRepeat(Animation animation){
            }
        });
        anim.setDuration(250);
        anim.setFillAfter(true);
        layButtons.startAnimation(anim);
    }
    View.OnClickListener clkByYMM = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            productNum = null;
            fromVIN = false;
            FragmentByYMM frag = new FragmentByYMM();
            openFrag(frag, "byYMM", false);
        }
    };
    View.OnClickListener clkByVIN = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                FragmentByVIN frag = new FragmentByVIN();
                openFrag(frag, "byVIN", false);
            }else{
                showNoNetwork();
            }
        }
    };
    View.OnClickListener clkByProdNum = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                FragmentByProdNum frag = new FragmentByProdNum();
                openFrag(frag, "byProdNum", false);
            }else{
                showNoNetwork();
            }
        }
    };
    public void openService(){
        ModuleGetRelearns getRelearns = new ModuleGetRelearns();
        getRelearns.setContext(this);
        getRelearns.execute();
    }
    public void openCheck(){
        closeUnusedFrags();
        moduleBTHC.copy = false;
        moduleBTHC.copySet = false;
        fragCheck = new FragmentCheck();
        openFrag(fragCheck, "check", true);
    }
    public void openProgram(){
        FragmentProgram frag = new FragmentProgram();
        openFrag(frag, "program", true);
    }
    //public void openRelearns(){
    //    FragmentRelearns frag = new FragmentRelearns();
    //    openFrag(frag, "relearns", true);
    //}
    public void openCreate(boolean fromCopyOrCopySet){
        closeUnusedFrags();
        if(!fromCopyOrCopySet){
            moduleBTHC.copy = false;
            moduleBTHC.copySet = false;
        }
        fragCreate = new FragmentCreate();
        openFrag(fragCreate, "create", true);
    }
    //public void openChoose(boolean fromCopyOrCopySet){
    //    FragmentChoose frag = new FragmentChoose();
    //    frag.fromCopyOrCopySet = fromCopyOrCopySet;
    //    openFrag(frag, "choose", true);
    //}
    public void openCopy(){
        closeUnusedFrags();
        moduleBTHC.copy = true;
        moduleBTHC.copySet = false;
        fragCopy = new FragmentCopy();
        openFrag(fragCopy, "copy", true);
    }
    public void openCopySet(){
        closeUnusedFrags();
        moduleBTHC.copy = false;
        moduleBTHC.copySet = true;
        fragCopySet = new FragmentCopySet();
        openFrag(fragCopySet, "copySet", true);
    }
    public void closeUnusedFrags(){
        fragCheck = null;
        fragCreate = null;
        fragCopy = null;
        fragCopySet = null;
    }
    public void openDeviceResults(){
        FragmentDeviceResults frag = new FragmentDeviceResults();
        openFrag(frag, "deviceResults", true);
    }
    public void openDeviceDetails(){
        FragmentDeviceDetails frag = new FragmentDeviceDetails();
        openFrag(frag, "deviceDetails", true);
    }
    public void openLargeDeviceImg(Bitmap bmp){
        bmpDevice = bmp;
        FragmentLargeDeviceImg frag = new FragmentLargeDeviceImg();
        openFrag(frag, "largeDeviceImg", true);
    }
    public void openRelearnInst(){
        FragmentRelearnInst frag = new FragmentRelearnInst();
        openFrag(frag, "relearnInst", true);
    }
    public void openTroubleshooting(){
        final FragmentTroubleshooting frag = new FragmentTroubleshooting();
        openFrag(frag, "troubleshooting", true);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                frag.loadTipsAndWarnings();
            }
        }, 500);

    }
    public void openTechDocs(){
        FragmentTechDocsDevice frag = new FragmentTechDocsDevice();
        openFrag(frag, "techDocs", true);
    }
    public void openOBD(){
        fragOBD = new FragmentOBD();
        fragOBD.shouldRestore = false;
        openFrag(fragOBD, "obd", true);
    }
    public void openFrag(Fragment frag, String tag, Boolean replace){
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        if(replace){
            fragmentTransaction.replace(R.id.layFrags, frag, tag);
        }else{
            fragmentTransaction.add(R.id.layFrags, frag, tag);
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        layFrags.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        switch(requestCode){
            case 0:
                switch(resultCode){
                    case -1:
                        openBT();
                        break;
                }
                break;
            case 1:
                if(resultCode == RESULT_OK){
                    if(data.getExtras().containsKey("btDevice")){
                        btDevice = data.getParcelableExtra("btDevice");
                        new asyncDevice().execute();
                    }
                }else{
                    if(btDevice == null){
                        Toast.makeText(this, getString(R.string.m_noBTSelected), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                if(scanResult != null){
                    if(scanResult.getContents() != null){
                        if(scanForVin) {
                            FragmentByVIN frag = (FragmentByVIN)getFragmentManager().findFragmentByTag("byVIN");
                            frag.txtVIN.setText(scanResult.getContents().toString());
                            frag.go();
                        }else{
                            String partNum = moduleDB.getPartNumByUpc(scanResult.getContents().toString());
                            if(partNum.length() > 0){
                                FragmentByProdNum frag = (FragmentByProdNum)getFragmentManager().findFragmentByTag("byProdNum");
                                frag.txtProdNum.setText(partNum);
                                frag.go();
                            }else{
                                Toast.makeText(this, getString(R.string.m_productNotFound), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }else{
                        Toast.makeText(this, getString(R.string.m_scanUnsuccessful), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this, getString(R.string.m_scanUnsuccessful), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    public void checkBT(){
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter == null){
            Toast.makeText(this, getString(R.string.m_noBTSupport), Toast.LENGTH_SHORT).show();
        }else{
            if(!btAdapter.isEnabled()){
                Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(i, 0);
            }else{
                final String savedAddress = preferences.getString("btID", "");
                if(savedAddress.length() > 0){
                    Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
                    if(pairedDevices.size() > 0){
                        for(BluetoothDevice device : pairedDevices){
                            if(device.getAddress().equals(savedAddress)){
                                btDevice = device;
                                new asyncDevice().execute();
                            }
                        }
                    }
                }else{
                    openBT();
                }
            }
        }
    }
    public void openBT(){
        Intent i = new Intent(this, ActivityBT.class);
        if(btDeviceHC != null && btSocketHC != null){
            if(btSocketHC.isConnected()){
                i.putExtra("addressHC", btDeviceHC.getAddress());
                i.putExtra("serialNumHC", serialNumHC);
                i.putExtra("fwVersionHC", firmwareVersionHC);
                i.putExtra("hwVersionHC", hardwareVersionHC);
                i.putExtra("dbVersionHC", databaseVersionHC);
                editor.putString("deviceKey", serialNumHC);
                editor.commit();
            }
        }
        if(btDeviceOBD != null && btSocketOBD != null){
            if(btSocketOBD.isConnected()){
                i.putExtra("addressOBD", btDeviceOBD.getAddress());
                i.putExtra("serialNumOBD", serialNumOBD);
                i.putExtra("fwVersionOBD", firmwareVersionOBD);
                i.putExtra("hwVersionOBD", hardwareVersionOBD);
                i.putExtra("dbVersionOBD", databaseVersionOBD);
            }
        }
        startActivityForResult(i, 1);
        Toast.makeText(ActivityMain.this, getString(R.string.m_pleaseConnect), Toast.LENGTH_SHORT).show();
    }
    public void byYMMFromVIN(String sentYear, String sentMake, String sentModel){
        fromVIN = true;
        closeFrags();
        selectedYear = sentYear;
        selectedMake = moduleDB.getMakeByMakeName(sentMake);
        selectedModel = moduleDB.getModelByModelName(sentModel);
        FragmentByYMM frag = new FragmentByYMM();
        openFrag(frag, "byYMM", false);
    }
    public void returnSensors(){
        if((programmables.size() > 0) || (nonprogrammables.size() > 0) || (serviceKits.size() > 0)){
            openDeviceResults();
        }else{
            Toast.makeText(this, getString(R.string.m_noResults), Toast.LENGTH_SHORT).show();
        }
    }
    public void returnRelearns(){
        fragService = new FragmentService();
        openFrag(fragService, "service", true);
    }
    public void returnAttachments(){
        if(attachments.size() > 0){
            moduleDB.addAttachments(attachments);
        }
    }
    public void returnNewVehicles(){
        if(newVehicles.size() > 0){
            moduleDB.addVehicles(newVehicles);
        }
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        editor.putString("newVehicleDate", sdf.format(cal.getTime()));
        editor.commit();
        ArrayList<Action> actions = moduleDB.getActions();
        if(actions.size() > 0){
            ModuleSendActions sendActions = new ModuleSendActions();
            sendActions.setContext(ActivityMain.this, actions);
            sendActions.execute();
        }
    }
    public void returnSentActions(boolean success){
        if(success){
            moduleDB.deleteHistory();
        }
    }
    public void closeFrags(){
        while(getFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStackImmediate();
        }
        layFrags.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    }
    private class asyncDevice extends AsyncTask<Void, Void, Void> {
        ProgressDialog prgDialog;
        @Override
        protected void onPreExecute(){
            prgDialog = new ProgressDialog(ActivityMain.this);
            prgDialog.setCancelable(false);
            prgDialog.setTitle(getString(R.string.m_bt));
            prgDialog.setMessage(getString(R.string.m_connectToDevice));
            prgDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params){
            try{
                ParcelUuid list[] = btDevice.getUuids();
                ParcelUuid uuid = list[0];
                btSocket = btDevice.createInsecureRfcommSocketToServiceRecord(uuid.getUuid());
                btSocket.connect();
            }catch(IOException e){
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result){
            prgDialog.dismiss();
            if(btSocket != null){
                if(btSocket.isConnected()){
                    imvBluetooth.setSelected(true);
                    String name;
                    if(btDevice.getName().substring(0, 2).equals("DT") || btDevice.getName().substring(0, 2).equals("RG")){
                        obdFirmware = false;
                        name = btDevice.getName().replace(btDevice.getName().substring(0, 2), "HC");
                        btSocketHC = btSocket;
                        btDeviceHC = btDevice;
                        moduleBTHC = new ModuleBT(ActivityMain.this, btSocketHC);
                        if(moduleBTHC.connected){
                            moduleBTHC.start();
                        }
                        Toast.makeText(ActivityMain.this, name + " " + getString(R.string.m_connected), Toast.LENGTH_SHORT).show();
                        editor.putString("btID", btDevice.getAddress());
                        editor.commit();
                        if(fragService != null){
                            if(fragService.checking){
                                openCheck();
                            }else{
                                openProgram();
                            }
                        }else{
                            btnTPMSServ.callOnClick();
                        }
                        fetchFirmware();
                    }else{
                        obdFirmware = true;
                        btSocketOBD = btSocket;
                        btDeviceOBD = btDevice;
                        moduleBTOBD = new ModuleBT(ActivityMain.this, btSocketOBD);
                        if(moduleBTOBD.connected){
                            moduleBTOBD.start();
                        }
                        byte[] txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x50, (byte)0xE3};
                        moduleBTOBD.write(txArray);
                        StringBuilder sb = new StringBuilder();
                        for(byte b: txArray){
                            sb.append(String.format("0x%x ", b));
                        }
                        Log.d("BTCOMM TX", sb.toString());
                        if(fragOBD != null){
                            discoverOBDProtocol();
                        }
                    }
                }else{
                    btDevice = null;
                    Toast.makeText(ActivityMain.this, getString(R.string.m_deviceNotConnected), Toast.LENGTH_SHORT).show();
                    openBT();
                }
            }else{
                btDevice = null;
                Toast.makeText(ActivityMain.this, getString(R.string.m_deviceNotConnected), Toast.LENGTH_SHORT).show();
                openBT();
            }
        }
    }
    public void checkData(){
        ModuleGetLatestUpdate getLatestUpdate = new ModuleGetLatestUpdate();
        getLatestUpdate.setContext(ActivityMain.this);
        getLatestUpdate.execute();
    }
    public void doCheck(){
        if(moduleBTHC.connected){
            moduleDB.addHistory(vehicle.configID, 0);
            checkingFreq = false;
            creating = false;
            createHCDialog(getString(R.string.m_checkingSensor), 800);
            moduleBTHC.cmdWordsCount = 0;
            if(sensorNameFreq != null){
                mhz = (byte)0x01;
                if(sensorNameFreq.frequency.equals("433")){
                    mhz = (byte)0x02;
                }
                finishFreq(0);
            }else{
                if(!freqChosen){
                    getVehicleFreq(0);
                }else{
                    finishFreq(0);
                }
            }
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
        }
    }
    public void doCreate(){
        cameFromCopyOrSet = 0;
        sendCreate();
    }
    public void doCopy(){
        if(moduleBTHC.connected){
            moduleDB.addHistory(vehicle.configID, 2);
            checkingFreq = false;
            creating = false;
            createHCDialog(getString(R.string.m_readingSensor), 800);
            moduleBTHC.cmdWordsCount = 0;
            if(sensorNameFreq != null){
                mhz = (byte)0x01;
                if(sensorNameFreq.frequency.equals("433")){
                    mhz = (byte)0x02;
                }
                finishFreq(0);
            }else{
                if(!freqChosen){
                    getVehicleFreq(0);
                }else{
                    finishFreq(0);
                }
            }
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
        }
    }
    public void doCopyCreate(){
        cameFromCopyOrSet = 1;
        sendCreate();
    }
    public void doCopySet(){
        if(moduleBTHC.connected){
            moduleDB.addHistory(vehicle.configID, 2);
            checkingFreq = false;
            creating = false;
            createHCDialog(getString(R.string.m_readingSensor), 800);
            moduleBTHC.cmdWordsCount = 0;
            if(sensorNameFreq != null){
                mhz = (byte)0x01;
                if(sensorNameFreq.frequency.equals("433")){
                    mhz = (byte)0x02;
                }
                finishFreq(0);
            }else{
                if(!freqChosen){
                    getVehicleFreq(0);
                }else{
                    finishFreq(0);
                }
            }
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
        }
    }
    public void doCopySetCreate(){
        cameFromCopyOrSet = 2;
        sendCreate();
    }
    public void sendCreate(){
        if(moduleBTHC.connected){
            moduleDB.addHistory(vehicle.configID, 1);
            checkingFreq = false;
            creating = true;
            createHCDialog(getString(R.string.m_programmingSensor) + " " + sensorNameFreq.name, 1600);
            moduleBTHC.cmdWordsCount = 0;
            if(sensorNameFreq != null){
                mhz = (byte)0x01;
                if(sensorNameFreq.frequency.equals("433")){
                    mhz = (byte)0x02;
                }
                finishFreq(1);
            }else{
                if(!freqChosen){
                    getVehicleFreq(1);
                }else{
                    finishFreq(1);
                }
            }
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
        }
    }
    public void getVehicleFreq(final int checkOrCreate){
        if(vehicle.sensorNameFreqs.size() > 0){
            if(vehicle.sensorNameFreqs.size() > 1){
                final ArrayList<String> frequencies = new ArrayList<String>();
                String frequency = "";
                for(SensorNameFreq sNFreq : vehicle.sensorNameFreqs){
                    if((frequency.length() < 1) || (!frequency.equals(sNFreq.frequency))){
                        frequency = sNFreq.frequency;
                        frequencies.add(frequency);
                    }
                }
                if(frequencies.size() > 1){
                    final Dialog dialog = new Dialog(this);
                    dialog.setContentView(R.layout.dialog_frequencies);
                    dialog.setCancelable(true);
                    final RadioGroup rdgAll = (RadioGroup)dialog.findViewById(R.id.rdgAll);
                    RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 5, 5, 5);
                    String freq = "";
                    int i;
                    for(i = 0; i < frequencies.size(); i++){
                        freq = frequencies.get(i);
                        RadioButton rdo = new RadioButton(this);
                        rdo.setId(i);
                        rdo.setLayoutParams(params);
                        rdo.setText(freq + getString(R.string.m_mhz));
                        rdgAll.addView(rdo);
                    }
                    RadioButton rdo = new RadioButton(this);
                    rdo.setId(i);
                    rdo.setLayoutParams(params);
                    rdo.setText(getString(R.string.m_discoverAutomatically));
                    rdgAll.addView(rdo);
                    Button btnOK = (Button)dialog.findViewById(R.id.btnOK);
                    btnOK.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v){
                            int rdoID = rdgAll.getCheckedRadioButtonId();
                            if(rdoID > -1){
                                if(rdoID < frequencies.size()){
                                    String freq = frequencies.get(rdoID);
                                    mhz = (byte)0x01;
                                    if(freq.equals("433")){
                                        mhz = (byte)0x02;
                                    }
                                    freqChosen = true;
                                    dialog.dismiss();
                                    finishFreq(checkOrCreate);
                                }else{
                                    helpWithFrequency();
                                }
                                dialog.dismiss();
                            }else{
                                Toast.makeText(ActivityMain.this, getString(R.string.m_validSelection), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    dialog.show();
                }else{
                    sensorNameFreq = vehicle.sensorNameFreqs.get(0);
                    finishFreq(checkOrCreate);
                }
            }else{
                sensorNameFreq = vehicle.sensorNameFreqs.get(0);
                finishFreq(checkOrCreate);
            }
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_noSensors), Toast.LENGTH_SHORT).show();
        }
    }
    public void helpWithFrequency(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                doDiscoverFreq();
                dialog.dismiss();
            }
        });
        builder.setNeutralButton(getString(R.string.m_cancel), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.setTitle(getString(R.string.m_helpFindFreq));
        builder.setMessage(getString(R.string.m_placeIntellisens));
        builder.show();
    }
    public void finishFreq(int checkOrCreate){
        switch(checkOrCreate){
            case 0:
                writeCCCS();
                break;
            case 1:
                writeCreate();
                break;
        }
    }
    public void doDiscoverFreq(){
        if(moduleBTHC.connected){
            checkingFreq = true;
            moduleBTHC.copy = false;
            moduleBTHC.copySet = false;
            prgDialogHC = new ProgressDialog(this);
            prgDialogHC.setCancelable(false);
            prgDialogHC.setTitle(getString(R.string.m_appName));
            prgDialogHC.setMessage(getString(R.string.m_discoverFreq));
            moduleBTHC.cmdWordsCount = 0;
            mhz = (byte)0x01;
            writeCCCS();
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
        }
    }
    public void writeCCCS(){
        prgDialogHC.show();
        CommandWord cmdWord = vehicle.commandWords.get(0);
        checkWord = (byte)cmdWord.checkWord;
        byte sum = (byte)((byte)0x8E + (byte)0x00 + (byte)0x07 + (byte)0x60 + checkWord + mhz);
        moduleBTHC.write(new byte[]{(byte) 0x8E, (byte) 0x00, (byte) 0x07, (byte)0x60, checkWord, mhz, sum});
    }
    public void writeCreate(){
        prgDialogHC.show();
        moduleBTHC.write(new byte[]{(byte) 0x8E, (byte) 0x00, (byte) 0x06, (byte) 0x80, (byte) 0x03, (byte) 0x17});
    }
    public void checkResult(byte[] sensorID, byte pressure, byte temp, byte bat){
        if(!obdCheck){
            sendResult(0, sensorID, pressure, temp, bat);
        }else{
            sendResult(4, sensorID, pressure, temp, bat);
        }
    }
    public void createResult(byte[] sensorID, byte pressure, byte temp, byte bat){
        sendResult(1, sensorID, pressure, temp, bat);
    }
    public void copyResult(byte[] sensorID, byte pressure, byte temp, byte bat){
        sendResult(2, sensorID, pressure, temp, bat);
    }
    public void copySetResult(byte[] sensorID, byte pressure, byte temp, byte bat){
        sendResult(3, sensorID, pressure, temp, bat);
    }
    public void sendResult(int resultType, byte[] sensorID, byte pressure, byte temp, byte bat){
        String currID;
        final StringBuilder sb = new StringBuilder();
        for(final Byte b : sensorID){
            if(b != null){
                int i = b;
                if(i < 0){
                    i += 256;
                }
                final String hex = Integer.toHexString(i);
                if(hex.length() == 1){
                    sb.append('0');
                }
                sb.append(hex);
            }
        }
        currID = sb.toString().toUpperCase();
        if(preferences.getInt("hexDec", 0) == 1){
            currID = String.format("%d", Integer.parseInt(sb.toString(), 16));
        }
        float fPressure;
        if(preferences.getInt("psiBar", 0) == 0){
            fPressure = (float)(pressure * 1.45038);
        }else{
            fPressure = (float)(pressure * .001);
        }
        String strPressure = String.format("%.2f", fPressure);
        String strTemp = Integer.toString(temp);
        if(preferences.getInt("fahCel", 0) == 1){
            float fTemp = (float)((float)(temp - 32) * 0.555);
            int cTemp = (int)fTemp;
            strTemp = Integer.toString(cTemp);
        }
        String strFreq;
        if(sensorNameFreq != null){
           strFreq = sensorNameFreq.frequency;
        }else{
            strFreq = "315";
            if(mhz == (byte)0x02){
                strFreq = "433";
            }
        }
        String strBat = getString(R.string.m_low);
        if(bat > 0){
            strBat = getString(R.string.m_ok);
        }
        switch(resultType){
            case 0:
                fragCheck.checkResult(currID, strPressure, strTemp, strFreq, strBat);
                break;
            case 1:
                switch(cameFromCopyOrSet){
                    case 0:
                        fragCreate.createResult(currID, strPressure, strTemp, strFreq, strBat);
                        break;
                    case 1:
                        fragCopy.createResult();
                        break;
                    case 2:
                        fragCopySet.createResult(currID, strFreq);
                        break;
                }
                break;
            case 2:
                fragCopy.copyResult(currID, strPressure, strTemp, strFreq, strBat);
                break;
            case 3:
                fragCopySet.copySetResult(currID, strPressure, strTemp, strFreq, strBat);
                break;
            case 4:
                fragOBD.checkResult(currID, strPressure, strTemp, strFreq, strBat);
                break;
        }
    }
    public void fetchFirmware(){
        prgDialogHC = new ProgressDialog(this);
        prgDialogHC.setCancelable(false);
        prgDialogHC.setTitle(getString(R.string.m_appName));
        prgDialogHC.setMessage(getString(R.string.m_loadingResults));
        byte[] txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x9B, (byte)0x2E};
        if(!obdFirmware){
            if(moduleBTHC.connected){
                moduleBTHC.fetchingFirmware = true;
                moduleBTHC.write(txArray);
            }else{
                Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
                return;
            }
        }else{
            if(moduleBTOBD.connected){
                moduleBTOBD.fetchingFirmware = true;
                moduleBTOBD.write(txArray);
            }else{
                Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
                return;
            }
        }
        prgDialogHC.show();
        StringBuilder sb = new StringBuilder();
        for(byte b: txArray){
            sb.append(String.format("0x%x ", b));
        }
        Log.d("BTCOMM TX", sb.toString());
    }
    public void returnFirmwareDetails(String fwVersion, byte[] fetchedData){
        try{
            if(!obdFirmware){
                int fwVersionNew = Integer.valueOf(new String(fetchedData, "UTF-8").substring(10, 13));
                if(Integer.valueOf(fwVersion) > fwVersionNew){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setCancelable(false);
                    builder.setNeutralButton(getString(R.string.m_no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setPositiveButton(getString(R.string.m_yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            updateFirmware();
                        }
                    });
                    builder.setMessage(getString(R.string.m_newFirmware));
                    builder.show();
                }

                serialNumHC = new String(fetchedData, "UTF-8").substring(0, 10);
                firmwareVersionHC = new String(fetchedData, "UTF-8").substring(10, 11) + "." + new String(fetchedData, "UTF-8").substring(11, 12) + "." + new String(fetchedData, "UTF-8").substring(12, 13);
                hardwareVersionHC = new String(fetchedData, "UTF-8").substring(13, 14);
                databaseVersionHC = new String(fetchedData, "UTF-8").substring(14, 15) + "." + new String(fetchedData, "UTF-8").substring(15, 16) + "." + new String(fetchedData, "UTF-8").substring(16, 17);
                editor.putString("deviceKey", serialNumHC);
                editor.putString("firmwareVersion", firmwareVersionHC);
                editor.commit();
            }else{
                serialNumOBD = new String(fetchedData, "UTF-8").substring(0, 10);
                firmwareVersionOBD = new String(fetchedData, "UTF-8").substring(10, 11) + "." + new String(fetchedData, "UTF-8").substring(11, 12) + "." + new String(fetchedData, "UTF-8").substring(12, 13);
                hardwareVersionOBD = new String(fetchedData, "UTF-8").substring(13, 14);
                databaseVersionOBD = new String(fetchedData, "UTF-8").substring(14, 15) + "." + new String(fetchedData, "UTF-8").substring(15, 16) + "." + new String(fetchedData, "UTF-8").substring(16, 17);
            }
        }catch(UnsupportedEncodingException e){
        }
    }
    public void updateFirmware(){
        if(moduleBTHC.connected){
            moduleBTHC.fetchingFirmware = false;
            prgDialogHC = new ProgressDialog(this);
            prgDialogHC.setCancelable(false);
            prgDialogHC.setTitle(getString(R.string.m_appName));
            prgDialogHC.setMessage(getString(R.string.m_updatingFirmware));
            prgDialogHC.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            prgDialogHC.setMax(1000);
            prgDialogHC.setProgressNumberFormat(null);
            prgDialogHC.setProgressPercentFormat(null);
            prgDialogHC.show();
            byte[] txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x9B, (byte)0x2E};
            moduleBTHC.write(txArray);
            StringBuilder sb = new StringBuilder();
            for(byte b: txArray){
                sb.append(String.format("0x%x ", b));
            }
            Log.d("BTCOMM TX", sb.toString());
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
        }
    }
    public void firmwareUpdated(){
        moduleBTHC = null;
        try{
            btSocket.close();
            btSocket = null;
        }catch(IOException e){
        }
        prgDialogHC.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setMessage(getString(R.string.m_firmwareUpdateComplete));
        builder.show();
    }
    public void frequencyDiscovered(){
        prgDialogHC.dismiss();
        freqChosen = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setTitle(getString(R.string.m_freqDiscovered));
        String freq = "315";
        if(mhz == (byte)0x02){
            freq = "433";
        }
        builder.setMessage(getString(R.string.m_frequencyIs) + " " + freq + getString(R.string.m_mhz) + ".");
        builder.show();
    }
    public void frequencyUndiscovered(){
        prgDialogHC.dismiss();
        freqChosen = false;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setTitle(getString(R.string.m_freqNotDiscovered));
        builder.setMessage(getString(R.string.m_freqUnable));
        builder.show();
    }
    public void discoverOBDProtocol(){
        if(moduleBTOBD.connected){
            moduleBTOBD.obdIdsGathered = (fragOBD.strIDs.size() != 0);
            prgDialogHC = new ProgressDialog(this);
            prgDialogHC.setCancelable(false);
            prgDialogHC.setTitle(getString(R.string.m_obd));
            prgDialogHC.setMessage(getString(R.string.m_obdProtocol));
            byte[] txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x07, (byte)0x1B, (byte)0x00, (byte)0x00, (byte)0xAE};
            moduleBTOBD.write(txArray);
            StringBuilder sb = new StringBuilder();
            for(byte b: txArray){
                sb.append(String.format("0x%x ", b));
            }
            Log.d("BTCOMM TX", sb.toString());
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
        }
    }
    public void doRelearn(){
        if(moduleBTOBD.connected){
            prgDialogHC = new ProgressDialog(this);
            prgDialogHC.setCancelable(false);
            prgDialogHC.setTitle(getString(R.string.m_obd));
            prgDialogHC.setMessage(getString(R.string.m_performingRelearn));
            Calendar cal = Calendar.getInstance();
            String strYear = String.valueOf(cal.get(Calendar.YEAR));
            byte year0 = Integer.valueOf(strYear.substring(0, 1)).byteValue();
            byte year1 = Integer.valueOf(strYear.substring(1, 2)).byteValue();
            byte year2 = Integer.valueOf(strYear.substring(2, 3)).byteValue();
            byte year3 = Integer.valueOf(strYear.substring(3, 4)).byteValue();
            String strMonth = String.valueOf(cal.get(Calendar.MONTH)+1);
            byte month0;
            byte month1;
            if(strMonth.length() < 2){
                month0 = (byte)0x00;
                month1 = Integer.valueOf(strMonth.substring(0, 1)).byteValue();
            }else{
                month0 = Integer.valueOf(strMonth.substring(0, 1)).byteValue();
                month1 = Integer.valueOf(strMonth.substring(1, 2)).byteValue();
            }
            String strDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            byte day0;
            byte day1;
            if(strDay.length() < 2){
                day0 = (byte)0x00;
                day1 = Integer.valueOf(strDay.substring(0, 1)).byteValue();
            }else{
                day0 = Integer.valueOf(strDay.substring(0, 1)).byteValue();
                day1 = Integer.valueOf(strDay.substring(1, 2)).byteValue();
            }
            String strHour = String.valueOf(cal.get(Calendar.HOUR));
            byte hour0;
            byte hour1;
            if(strHour.length() < 2){
                hour0 = (byte)0x00;
                hour1 = Integer.valueOf(strHour.substring(0, 1)).byteValue();
            }else{
                hour0 = Integer.valueOf(strHour.substring(0, 1)).byteValue();
                hour1 = Integer.valueOf(strHour.substring(1, 2)).byteValue();
            }
            String strMinute = String.valueOf(cal.get(Calendar.MINUTE));
            byte minute0;
            byte minute1;
            if(strMinute.length() < 2){
                minute0 = (byte)0x00;
                minute1 = Integer.valueOf(strMinute.substring(0, 1)).byteValue();
            }else{
                minute0 = Integer.valueOf(strMinute.substring(0, 1)).byteValue();
                minute1 = Integer.valueOf(strMinute.substring(1, 2)).byteValue();
            }
            byte sum = (byte)((byte)0x8E + (byte)0x00 + (byte)0x11 + (byte)0x10 + year0 + year1 + year2 + year3 + month0 + month1 + day0 + day1 + hour0 + hour1 + minute0 + minute1);
            byte[] txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x11, (byte)0x10, year0, year1, year2, year3, month0, month1, day0, day1, hour0, hour1, minute0, minute1, sum};
            moduleBTOBD.write(txArray);
            StringBuilder sb = new StringBuilder();
            for(byte b: txArray){
                sb.append(String.format("0x%x ", b));
            }
            Log.d("BTCOMM TX", sb.toString());
        }else{
            Toast.makeText(ActivityMain.this, getString(R.string.m_notConnected), Toast.LENGTH_SHORT).show();
        }
    }
    public void back(){
        int count = getFragmentManager().getBackStackEntryCount();
        if(count == 0){
            if(buttonsMoved){
                moveButtons(false);
            }else{
                super.onBackPressed();
            }
        }else{
            if(count == 1){
                closeFrags();
            }else{
                if(fragService != null){
                    fragService.criticalAlertShown = true;
                }
                boolean alreadyPopped = false;
                if(fragCreate != null){
                    if(fragCreate.layRelearnInst.getVisibility() == View.VISIBLE){
                        alreadyPopped = true;
                        fragCreate.layRelearnInst.setVisibility(View.GONE);
                    }
                }
                if(fragCopy != null){
                    if(fragCopy.layRelearnInst.getVisibility() == View.VISIBLE){
                        alreadyPopped = true;
                        fragCopy.layRelearnInst.setVisibility(View.GONE);
                    }else if(fragCopy.laySelect.getVisibility() == View.VISIBLE){
                        alreadyPopped = true;
                        fragCopy.laySelect.setVisibility(View.GONE);
                    }
                }
                if(fragCopySet != null){
                    if(fragCopySet.layRelearnInst.getVisibility() == View.VISIBLE){
                        alreadyPopped = true;
                        fragCopySet.layRelearnInst.setVisibility(View.GONE);
                    }else if(fragCopySet.laySelect.getVisibility() == View.VISIBLE){
                        alreadyPopped = true;
                        fragCopySet.laySelect.setVisibility(View.GONE);
                    }
                }
                if(!alreadyPopped){
                    bmpDevice = null;
                    getFragmentManager().popBackStack();
                }
            }
        }
    }
    public void createHCDialog(String msg, int length){
        prgDialogHC = new ProgressDialog(this);
        prgDialogHC.setCancelable(false);
        prgDialogHC.setTitle(getString(R.string.m_appName));
        prgDialogHC.setMessage(msg);
        if(!creating){
            prgDialogHC.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.m_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    moduleBTHC.cancelSensor();
                }
            });
        }
        prgDialogHC.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        prgDialogHC.setMax(length);
        prgDialogHC.setProgressNumberFormat(null);
        prgDialogHC.setProgressPercentFormat(null);
    }
    public void updateProgressHC(){
        if(prgDialogHC != null){
            prgDialogHC.setProgress(prgDialogHC.getProgress() + 1);
            if((prgDialogHC.getProgress() == prgDialogHC.getMax()) && !creating){
                moduleBTHC.cancelSensor();
                ActivityMain.this.runOnUiThread(new Runnable(){
                    public void run(){
                        prgDialogHC.dismiss();
                        Toast.makeText(ActivityMain.this, getString(R.string.m_errorTimeout), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }
    public void updateProgressFirmware(int prog, int max){
        if(prgDialogHC != null){
            prgDialogHC.setMax(max);
            prgDialogHC.setProgress(prog);
        }
    }
    @Override
    public void onBackPressed(){
        back();
    }
}
