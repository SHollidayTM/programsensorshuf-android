package com.hufna.intellisens;

/**
 * Created by scottholliday on 3/9/16.
 */
public class CommandWord {
    public int selectWord;
    public int checkWord;
    public int softwareVersion;
    public boolean hasRange;
    public int lowByteZero;
    public int lowByteOne;
    public int lowByteTwo;
    public int lowByteThree;
    public int highByteZero;
    public int highByteOne;
    public int highByteTwo;
    public int highByteThree;
}