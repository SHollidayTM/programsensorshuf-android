package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/17/15.
 */
public class FragmentCreate extends Fragment {
    ActivityMain act;
    TextView txtInst;
    TextView txtID;
    TextView txtMhz;
    TextView txtID2;
    TextView txtMhz2;
    Button btnProgram;
    Button btnRelearnInst;
    LinearLayout layRelearnInst;
    TextView txtRelearnTitle;
    WebView webRelearnInst;
    Button btnPrint;
    LinearLayout laySelect;
    ArrayList<SensorNameFreq> metal;
    ArrayList<SensorNameFreq> rubber;
    ArrayList<String> metalNames;
    ArrayList<String> rubberNames;
    ListView lstMetal;
    ListView lstRubber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_create, container, false);
        txtInst = (TextView)view.findViewById(R.id.txtInst);
        txtID = (TextView)view.findViewById(R.id.txtID);
        txtMhz = (TextView)view.findViewById(R.id.txtMhz);
        txtID2 = (TextView)view.findViewById(R.id.txtID2);
        txtMhz2 = (TextView)view.findViewById(R.id.txtMhz2);
        btnProgram = (Button)view.findViewById(R.id.btnProgram);
        btnProgram.setOnClickListener(clkProgram);
        btnRelearnInst = (Button)view.findViewById(R.id.btnRelearnInst);
        btnRelearnInst.setOnClickListener(clkRelearnInst);
        layRelearnInst = (LinearLayout)view.findViewById(R.id.layRelearnInst);
        txtRelearnTitle = (TextView)view.findViewById(R.id.txtRelearnTitle);
        txtRelearnTitle.setText(getString(R.string.m_relearnReqs) + " " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        btnPrint = (Button)view.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(clkPrint);
        webRelearnInst = (WebView)view.findViewById(R.id.webRelearnInst);
        laySelect = (LinearLayout)view.findViewById(R.id.laySelect);
        lstMetal = (ListView)view.findViewById(R.id.lstMetal);
        lstRubber = (ListView)view.findViewById(R.id.lstRubber);
        if(act.sensor == null){
            txtInst.setText(getString(R.string.m_copyFromList));
            metal = new ArrayList<SensorNameFreq>();
            rubber = new ArrayList<SensorNameFreq>();
            metalNames = new ArrayList<String>();
            rubberNames = new ArrayList<String>();
            for(SensorNameFreq sensorNameFreq : act.vehicle.sensorNameFreqs){
                if(sensorNameFreq.valveMaterial != null){
                    if(sensorNameFreq.valveMaterial.equals("M")){
                        metal.add(sensorNameFreq);
                        metalNames.add(sensorNameFreq.name);
                    }else{
                        rubber.add(sensorNameFreq);
                        rubberNames.add(sensorNameFreq.name);
                    }
                }
            }
            ArrayAdapter<String> adaMetal = new ArrayAdapter<String>(act, R.layout.item_centered, metalNames);
            lstMetal.setAdapter(adaMetal);
            lstMetal.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3){
                    act.sensorNameFreq = metal.get(position);
                    act.doCreate();
                }
            });
            ArrayAdapter<String> adaRubber = new ArrayAdapter<String>(act, R.layout.item_centered, rubberNames);
            lstRubber.setAdapter(adaRubber);
            lstRubber.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3){
                    act.sensorNameFreq = rubber.get(position);
                    act.doCreate();
                }
            });
        }else{
            laySelect.setVisibility(View.GONE);
            act.doCreate();
        }
        return view;
    }
    View.OnClickListener clkProgram = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            act.doCreate();
        }
    };
    public void createResult(String currID, String strPressure, String strTemp, String strFreq, String strBat){
        txtID2.setText(currID);
        txtMhz2.setText(strFreq);
        laySelect.setVisibility(View.GONE);
        Toast.makeText(act, getString(R.string.m_sensorProgrammed), Toast.LENGTH_SHORT).show();
    }
    View.OnClickListener clkRelearnInst = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            webRelearnInst.loadData(act.relearn.instructions, "text/html", null);
            layRelearnInst.setVisibility(View.VISIBLE);
        }
    };
    View.OnClickListener clkPrint = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PrintManager mgrPrint = (PrintManager)act.getSystemService(Context.PRINT_SERVICE);
            PrintDocumentAdapter adaPrint = webRelearnInst.createPrintDocumentAdapter("IntelliSens Print");
            PrintJob jobPrint = mgrPrint.print(txtRelearnTitle.getText().toString(), adaPrint, new PrintAttributes.Builder().build());
        }
    };
}
