package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class FragmentVideos extends Fragment {
    ActivityMore act;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMore)getActivity();
        View view = inflater.inflate(R.layout.fragment_videos, container, false);
        setListContent(view);
        return view;
    }
    private void setListContent(View view){
        ArrayList<String> videos = new ArrayList<String>();
        videos.add(getString(R.string.m_hufTrainingVideo) + " 1");
        videos.add(getString(R.string.m_hufTrainingVideo) + " 2");
        videos.add(getString(R.string.m_hufTrainingVideo) + " 3");
        ListView lstVideos = (ListView)view.findViewById(R.id.lstVideos);
        ArrayAdapter<String> ada = new ArrayAdapter<String>(act, R.layout.item_centered, videos);
        lstVideos.setAdapter(ada);
        lstVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                String url = null;
                switch(position){
                    case 0:
                        url = "https://www.youtube.com/watch?v=kC31FvaPAD4";
                        break;
                    case 1:
                        url = "https://www.youtube.com/watch?v=KOOUb_NCya0";
                        break;
                    case 2:
                        url = "https://www.youtube.com/watch?v=FnKofX-krmc";
                        break;
                }
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
            }
        });
    }
}