package com.hufna.intellisens;

/**
 * Created by sholliday on 12/18/15.
 */
public class SensorTPMS{
    boolean programmable;
    String catalogID;
    String partNumber;
    String name;
    String category;
    String sameAsNumber;
    String alternateNumber;
    String description;
    String relatedPartNumber;
    String manufacturer;
    String frequency;
    String torqueNut;
    String torqueScrew;
    String torqueCore;
    String housingColor;
    String type;
    String partDesign;
    String valveMaterial;
    String shape;
    String attCategory;
    String idLength;
    String idType;
    String madeFor;
}
