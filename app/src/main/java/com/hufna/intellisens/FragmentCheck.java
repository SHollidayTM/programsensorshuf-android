package com.hufna.intellisens;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/23/15.
 */
public class FragmentCheck extends Fragment{
    ActivityMain act;
    ImageButton btnLeftFront;
    ImageButton btnRightFront;
    ImageButton btnRightRear;
    ImageButton btnLeftRear;
    ImageButton btnSpare;
    TextView txtLeftFront;
    TextView txtRightFront;
    TextView txtRightRear;
    TextView txtLeftRear;
    TextView txtSpare;
    int checkSensor;
    ArrayList<String> strIDs;
    int checkCount;
    boolean rechecking;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        act.obdCheck = false;
        View view = inflater.inflate(R.layout.fragment_check, container, false);
        TextView txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        txtTitle.setText(getString(R.string.m_check) + ": " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        btnLeftFront = (ImageButton)view.findViewById(R.id.btnLeftFront);
        btnLeftFront.setOnClickListener(clkLeftFront);
        btnRightFront = (ImageButton)view.findViewById(R.id.btnRightFront);
        btnRightFront.setOnClickListener(clkRightFront);
        btnRightRear = (ImageButton)view.findViewById(R.id.btnRightRear);
        btnRightRear.setOnClickListener(clkRightRear);
        btnLeftRear = (ImageButton)view.findViewById(R.id.btnLeftRear);
        btnLeftRear.setOnClickListener(clkLeftRear);
        btnSpare = (ImageButton)view.findViewById(R.id.btnSpare);
        btnSpare.setOnClickListener(clkSpare);
        txtLeftFront = (TextView)view.findViewById(R.id.txtLeftFront);
        txtLeftFront.setMovementMethod(new ScrollingMovementMethod());
        txtRightFront = (TextView)view.findViewById(R.id.txtRightFront);
        txtRightFront.setMovementMethod(new ScrollingMovementMethod());
        txtRightRear = (TextView)view.findViewById(R.id.txtRightRear);
        txtRightRear.setMovementMethod(new ScrollingMovementMethod());
        txtLeftRear = (TextView)view.findViewById(R.id.txtLeftRear);
        txtLeftRear.setMovementMethod(new ScrollingMovementMethod());
        txtSpare = (TextView)view.findViewById(R.id.txtSpare);
        txtSpare.setMovementMethod(new ScrollingMovementMethod());
        strIDs = new ArrayList<String>();
        return view;
    }
    View.OnClickListener clkLeftFront = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 0;
            if(btnLeftFront.isSelected()){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    View.OnClickListener clkRightFront = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 1;
            if(btnRightFront.isSelected()){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    View.OnClickListener clkRightRear = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 2;
            if(btnRightRear.isSelected()){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    View.OnClickListener clkLeftRear = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 3;
            if(btnLeftRear.isSelected()){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    View.OnClickListener clkSpare = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            checkSensor = 4;
            if(btnSpare.isSelected()){
                askForCheckAgain();
            }else{
                act.doCheck();
            }
        }
    };
    public void askForCheckAgain(){
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
                rechecking = true;
                act.doCheck();
            }
        });
        builder.setNeutralButton(getString(R.string.m_cancel), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.setMessage(getString(R.string.m_checkSensorAgain));
        builder.show();
    }
    public void checkResult(String currID, String strPressure, String strTemp, String strFreq, String strBat){
        boolean duplicate = false;
        if(!rechecking){
            for(String prevID : strIDs){
                if(currID.equals(prevID)){
                    if(checkCount < 2){
                        checkCount++;
                        act.doCheck();
                        return;
                    }
                    duplicate = true;
                    break;
                }
            }
        }
        rechecking = false;
        if(duplicate){
            switch(checkSensor){
                case 0: //LF
                    btnLeftFront.setSelected(false);
                    break;
                case 1: //RF
                    btnRightFront.setSelected(false);
                    break;
                case 2: //RR
                    btnRightRear.setSelected(false);
                    break;
                case 3: //LR
                    btnLeftRear.setSelected(false);
                    break;
                case 4: //SP
                    btnSpare.setSelected(false);;
                    break;
            }
            Toast.makeText(act, getString(R.string.m_duplicateSensor), Toast.LENGTH_SHORT).show();
        }else{
            strIDs.add(currID);
            String strTitle = "";
            TextView txtView = null;
            switch(checkSensor){
                case 0: //LF
                    btnLeftFront.setSelected(true);
                    strTitle = getString(R.string.m_lfCheckSuccess);
                    txtView = txtLeftFront;
                    break;
                case 1: //RF
                    btnRightFront.setSelected(true);
                    strTitle = getString(R.string.m_rfCheckSuccess);
                    txtView = txtRightFront;
                    break;
                case 2: //RR
                    btnRightRear.setSelected(true);
                    strTitle = getString(R.string.m_rrCheckSuccess);
                    txtView = txtRightRear;
                    break;
                case 3: //LR
                    btnLeftRear.setSelected(true);
                    strTitle = getString(R.string.m_lrCheckSuccess);
                    txtView = txtLeftRear;
                    break;
                case 4: //SP
                    btnSpare.setSelected(true);
                    strTitle = getString(R.string.m_spCheckSuccess);
                    txtView = txtSpare;
                    break;
            }
            String strPsiBar = getString(R.string.m_poundsSquareInch);
            if(act.preferences.getInt("psiBar", 0) == 1){
                strPsiBar = getString(R.string.m_bar);
            }
            final String strMsg = getString(R.string.m_id) + ": " + currID + "\r\n" + strPsiBar + ": " + strPressure + "\r\n" + getString(R.string.m_temp) + ": " + strTemp + "\r\n" + getString(R.string.m_mhz) + ": " + strFreq + "\r\n" + getString(R.string.m_battery) + ": " + strBat;
            AlertDialog.Builder builder = new AlertDialog.Builder(act);
            builder.setCancelable(false);
            final TextView finalTxtView = txtView;
            builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    finalTxtView.setText(strMsg);
                    finalTxtView.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            });
            builder.setTitle(strTitle);
            builder.setMessage(strMsg);
            builder.show();
        }
    }
}
