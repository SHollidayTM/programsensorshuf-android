package com.hufna.intellisens;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by sholliday on 12/23/15.
 */
public class ModuleGetRelearns extends AsyncTask<Void, Void, Void> {
    ActivityMain act;
    ProgressDialog prgDialog = null;
    HttpURLConnection connection;
    public void setContext(ActivityMain sentAct){
        act = sentAct;
        act.relearn = null;
        prgDialog = new ProgressDialog(act);
        prgDialog.setCancelable(false);
        prgDialog.setButton(DialogInterface.BUTTON_NEGATIVE, act.getString(R.string.m_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ModuleGetRelearns.this.cancel(true);
                dialog.dismiss();
            }
        });
    }
    @Override
    protected void onPreExecute(){
        prgDialog.setMessage(act.getString(R.string.m_loadingResults));
        prgDialog.show();
    }
    @Override
    protected Void doInBackground(Void... params){
        if(tryToConnect() == HttpURLConnection.HTTP_OK){
            getResults();
        }else{
            ModuleGetKey getKey = new ModuleGetKey();
            String key = getKey.execute(act);
            if(key.length() > 0){
                act.editor.putString("securityKey", key);
                act.editor.commit();
                if(tryToConnect() == HttpURLConnection.HTTP_OK){
                    getResults();
                }
            }else{
                return null;
            }
        }
        return null;
    }
    @Override
    protected void onPostExecute(Void result){
        act.returnRelearns();
        prgDialog.dismiss();
    }
    public int tryToConnect(){
        int status = 500;
        try{
            URL url = new URL(act.preferences.getString("apiUrl", "") + "Vehicle/Relearn");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            String body =  "{\"securityKey\":\"" + act.preferences.getString("securityKey", "") + "\",\"configID\":\"" + act.selectedVehicleConfig.configID + "\"}";
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body);
            writer.close();
            status = connection.getResponseCode();
        }catch(IOException e){
            return status;
        }
        return status;
    }
    public void getResults(){
        StringBuilder response = new StringBuilder();
        String line;
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while((line = reader.readLine()) != null){
                response.append(line);
            }
            reader.close();
            parseJson(response.toString());
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    public void parseJson(String json){
        try{
            JSONObject object = new JSONObject(json);
            if(object != null){
                act.relearn = new Relearn();
                if(!object.getString("addAirRequired").equals("null")){
                    act.relearn.addAir = object.getString("addAirRequired").toUpperCase();
                }else{
                    act.relearn.addAir = "";
                }
                if(!object.getString("rotateTireRequired").equals("null")){
                    act.relearn.rotateTires = object.getString("rotateTireRequired").toUpperCase();
                }else{
                    act.relearn.rotateTires = "";
                }
                if(!object.getString("replaceSensorRequired").equals("null")){
                    act.relearn.replaceSensor = object.getString("replaceSensorRequired").toUpperCase();
                }else{
                    act.relearn.replaceSensor = "";
                }
                String html = object.getString("Base64Relearn");
                if(!html.equals("null")){
                    byte[] data = Base64.decode(html, Base64.DEFAULT);
                    act.relearn.instructions = new String(data, "UTF-8");
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
    }
}

