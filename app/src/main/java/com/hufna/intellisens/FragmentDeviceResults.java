package com.hufna.intellisens;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by sholliday on 12/21/15.
 */
public class FragmentDeviceResults extends Fragment{
    ActivityMain act;
    ListView lstResults;
    ArrayList allItems = new ArrayList();
    String frequency = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_deviceresults, container, false);
        TextView txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        if(act.productNum != null){
            txtTitle.setText(act.productNum + " " + getString(R.string.m_results));
        }else if(act.selectedYear != null && act.selectedMake.makeName != null && act.selectedModel.modelName != null){
            txtTitle.setText(act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        }
        RadioGroup rdgFilter = (RadioGroup)view.findViewById(R.id.rdgFilter);
        lstResults = (ListView)view.findViewById(R.id.lstResults);
        allItems = new ArrayList();
        Collections.sort(act.programmables, new FreqComparator());
        Collections.sort(act.nonprogrammables, new FreqComparator());
        boolean multiple = false;
        for(int i = 0; i < act.programmables.size(); i++){
            SensorTPMS sensor = act.programmables.get(i);
            if(frequency.length() < 1){
                frequency = sensor.frequency;
            }else if(!frequency.equals(sensor.frequency)){
                multiple = true;
                break;
            }
        }
        if(!multiple){
            frequency = "";
            for(int i = 0; i < act.nonprogrammables.size(); i++){
                SensorTPMS sensor = act.nonprogrammables.get(i);
                if(frequency.length() < 1){
                    frequency = sensor.frequency;
                }else if(!frequency.equals(sensor.frequency)){
                    multiple = true;
                    break;
                }
            }
        }
        if(!multiple){
            rdgFilter.setVisibility(View.GONE);
            frequency = "";
            setupList();
        }else{
            RadioButton rdoFreq0 = (RadioButton)view.findViewById(R.id.rdoFreq0);
            rdoFreq0.setText("315 " + getString(R.string.m_mhz));
            rdoFreq0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                    if(isChecked){
                        allItems = new ArrayList();
                        frequency = "315";
                        setupList();
                    }
                }
            });
            RadioButton rdoFreq1 = (RadioButton)view.findViewById(R.id.rdoFreq1);
            rdoFreq1.setText("433 " + getString(R.string.m_mhz));
            rdoFreq1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                    if(isChecked){
                        allItems = new ArrayList();
                        frequency = "433";
                        setupList();
                    }
                }
            });
            rdoFreq0.setChecked(true);
        }
        return view;
    }
    public void setupList(){
        allItems.add(getString(R.string.m_appName) + ": " + getString(R.string.m_configurable));
        for(SensorTPMS sensor : act.programmables){
            if(!frequency.equals("")){
                if(sensor.frequency.equals(frequency)){
                    allItems.add(sensor);
                }
            }else{
                allItems.add(sensor);
            }

        }
        allItems.add("Huf: " + getString(R.string.m_directFit));
        for(SensorTPMS sensor : act.nonprogrammables){
            if(!frequency.equals("")){
                if(sensor.frequency.equals(frequency)){
                    allItems.add(sensor);
                }
            }else{
                allItems.add(sensor);
            }
        }
        allItems.add("Huf: " + getString(R.string.m_serviceKits));
        for(ServiceKit serviceKit : act.serviceKits){
            allItems.add(serviceKit);
        }
        AdapterResults ada = new AdapterResults(act.getApplicationContext(), allItems);
        lstResults.setAdapter(ada);
        lstResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                if(allItems.get(position).getClass().equals(SensorTPMS.class)){
                    act.sensor = (SensorTPMS)allItems.get(position);
                    act.serviceKit = null;
                    act.openDeviceDetails();
                    if(act.sensor.programmable) {
                        Toast.makeText(act, getString(R.string.m_sensorRequiresProg), Toast.LENGTH_SHORT).show();
                    }
                }else if(allItems.get(position).getClass().equals(ServiceKit.class)){
                    act.sensor = null;
                    act.serviceKit = (ServiceKit) allItems.get(position);
                    act.openDeviceDetails();
                }
            }
        });
    }
    public class FreqComparator implements Comparator<SensorTPMS> {
        public int compare(SensorTPMS left, SensorTPMS right){
            return left.frequency.compareTo(right.frequency);
        }
    }
}
