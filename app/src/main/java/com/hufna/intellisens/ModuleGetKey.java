package com.hufna.intellisens;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by sholliday on 1/11/16.
 */
public class ModuleGetKey{
    SharedPreferences preferences;
    public String execute(Activity sentAct){
        preferences = sentAct.getApplicationContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        try{
            //use preferences to get user id, password, and tool id
            URL url = new URL(preferences.getString("apiUrl", "") + "User/Login");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            String body = "{userName: \"HufUser1\", password: \"Doesn'tMatter'\", toolID: \"FAKETOOLID\"}";
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body);
            writer.close();
            int status = connection.getResponseCode();
            if(status == HttpURLConnection.HTTP_OK){
                StringBuilder response = new StringBuilder();
                String line;
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while((line = reader.readLine()) != null){
                    response.append(line);
                }
                reader.close();
                JSONObject object = new JSONObject(response.toString());
                return object.getString("SecurityKey");
            }
        }catch(IOException e){
            return "";
        }catch(JSONException e){
            return "";
        }
        return "";
    }
}
