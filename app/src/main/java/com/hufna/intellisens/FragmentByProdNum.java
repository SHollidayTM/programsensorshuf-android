package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/8/15.
 */
public class FragmentByProdNum extends Fragment{
    ActivityMain act;
    EditText txtProdNum;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain) getActivity();
        View view = inflater.inflate(R.layout.fragment_byprodnum, container, false);
        txtProdNum = (EditText)view.findViewById(R.id.txtProdNum);
        txtProdNum.setOnEditorActionListener(edtTxtProdNum);
        Button btnScan = (Button)view.findViewById(R.id.btnScan);
        btnScan.setOnClickListener(clkScan);
        Button btnGo = (Button)view.findViewById(R.id.btnGo);
        btnGo.setOnClickListener(clkGo);
        return view;
    }
    TextView.OnEditorActionListener edtTxtProdNum = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if(actionId == EditorInfo.IME_ACTION_GO){
                InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                go();
                return true;
            }
            return false;
        }
    };
    View.OnClickListener clkScan = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            act.scanForVin = false;
            IntentIntegrator integrator = new IntentIntegrator(act);
            integrator.initiateScan();
        }
    };
    View.OnClickListener clkGo = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            go();
        }
    };
    public void go(){
        if((txtProdNum.getText().length() > 0) && !(txtProdNum.getText().toString().equals("*"))){
            act.sensor = null;
            act.serviceKit = null;
            act.selectedYear = null;
            act.selectedMake = null;
            act.selectedModel = null;
            act.programmables = new ArrayList<SensorTPMS>();
            act.nonprogrammables = new ArrayList<SensorTPMS>();
            act.serviceKits = new ArrayList<ServiceKit>();
            act.productNum = txtProdNum.getText().toString();
            ModuleGetProdsByPart getProdsByPart = new ModuleGetProdsByPart();
            getProdsByPart.setContext(act);
            getProdsByPart.execute();
        }else{
            Toast.makeText(act, getString(R.string.m_enterProdNum), Toast.LENGTH_SHORT).show();
        }
    }
}
