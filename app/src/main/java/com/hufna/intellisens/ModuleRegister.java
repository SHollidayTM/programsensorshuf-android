package com.hufna.intellisens;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by scottholliday on 5/18/16.
 */
public class ModuleRegister extends AsyncTask<Void, Void, Void> {
    ActivityRegister act;
    ProgressDialog prgDialog = null;
    HttpURLConnection connection;
    public void setContext(ActivityRegister sentAct){
        act = sentAct;
        prgDialog = new ProgressDialog(act);
        prgDialog.setCancelable(false);
        prgDialog.setButton(DialogInterface.BUTTON_NEGATIVE, act.getString(R.string.m_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ModuleRegister.this.cancel(true);
                dialog.dismiss();
            }
        });
    }
    @Override
    protected void onPreExecute(){
        prgDialog.setMessage(act.getString(R.string.m_loadingResults));
        prgDialog.show();
    }
    @Override
    protected Void doInBackground(Void... params){
        if(tryToConnect() == HttpURLConnection.HTTP_OK){
            getResults();
        }else{
            ModuleGetKey getKey = new ModuleGetKey();
            String key = getKey.execute(act);
            if(key.length() > 0){
                act.editor.putString("securityKey", key);
                act.editor.commit();
                if(tryToConnect() == HttpURLConnection.HTTP_OK){
                    getResults();
                }
            }else{
                return null;
            }
        }
        return null;
    }
    @Override
    protected void onPostExecute(Void result){
        act.returnRegistration();
        prgDialog.dismiss();
    }
    public int tryToConnect(){
        int status = 500;
        try{
            URL url = new URL(act.preferences.getString("apiUrl", "") + "User/Register");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            String body = "{\"securityKey\":\"" + act.preferences.getString("securityKey", "") + "\",\"regionCode\":\"NorthAmerica\",\"deviceKey\":\"" + act.preferences.getString("deviceKey", "") + "\",\"email\":\"" + act.preferences.getString("email", "") + "\",\"name\":\"" + act.preferences.getString("name", "") + "\",\"companyName\":\"" + act.preferences.getString("companyName", "") + "\",\"companyAddress1\":\"" + act.preferences.getString("addressOne", "") + "\",\"companyAddress2\":\"" + act.preferences.getString("addressTwo", "") + "\",\"city\":\"" + act.preferences.getString("city","") + "\",\"state\":\"" + act.preferences.getString("state","") + "\",\"postal\":\"" + act.preferences.getString("postal","") + "\",\"companyPhone\":\"" + act.preferences.getString("phone","") + "\"}";
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body);
            writer.close();
            status = connection.getResponseCode();
        }catch(IOException e){
            return status;
        }
        return status;
    }
    public void getResults(){
        StringBuilder response = new StringBuilder();
        String line;
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while((line = reader.readLine()) != null){
                response.append(line);
            }
            reader.close();
            parseJson(response.toString());
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    public void parseJson(String json){
        try{
            JSONObject object = new JSONObject(json);
            if(object != null){
                String success = object.getString("Successful");
                if(success.toUpperCase().equals("TRUE")){
                    act.successful = true;
                }else{
                    act.successful = false;
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
