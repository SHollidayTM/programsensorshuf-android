package com.hufna.intellisens;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by sholliday on 12/21/15.
 */
public class ModuleParseAndReturnProds {
    ActivityMain act;
    public void setContext(ActivityMain sentAct){
        act = sentAct;
    }
    public void parseJson(String json){
        try{
            JSONArray jsonArray = new JSONArray(json);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject object = jsonArray.getJSONObject(i);
                String catalogID = object.getString("CatalogItemID");
                String partNumber = object.getString("PartNumber");
                String name = object.getString("Name");
                String category = object.getString("Category");
                String sameAsNumber = object.getString("SameAsPartNumber");
                String alternateNumber = object.getString("AlternatePartNumber");
                String description = object.getString("Description");
                String relatedPartNumber = object.getString("RelatedPartNumber");
                String manufacturer = object.getString("Manufacturer");
                String imageUrl = object.getString("ImageUrl");
                String fileName = name + ".jpg";
                File file = new File(act.getFilesDir(), fileName);
                if(!file.exists()){
                    try{
                        InputStream inputStream = act.getAssets().open(fileName);
                        Bitmap bmp = BitmapFactory.decodeStream(inputStream);
                        FileOutputStream output;
                        output = act.openFileOutput(fileName, Context.MODE_PRIVATE);
                        bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
                        output.close();
                    }catch(IOException e){
                        try{
                            InputStream inputStream = new URL(imageUrl).openStream();
                            Bitmap bmp = BitmapFactory.decodeStream(inputStream);
                            FileOutputStream output;
                            output = act.openFileOutput(fileName, Context.MODE_PRIVATE);
                            bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
                            output.close();
                        }catch(IOException ex){
                        }
                    }
                }
                JSONArray attributes = object.optJSONArray("Attributes");
                if(category.equals("Sensors")){
                    SensorTPMS sensor = new SensorTPMS();
                    sensor.catalogID = catalogID;
                    sensor.partNumber = partNumber;
                    sensor.name = name;
                    sensor.category = category;
                    sensor.sameAsNumber = sameAsNumber;
                    sensor.alternateNumber = alternateNumber;
                    sensor.description = description;
                    sensor.relatedPartNumber = relatedPartNumber;
                    sensor.manufacturer = manufacturer;
                    for(int x = 0; x < attributes.length(); x++){
                        JSONObject attribute = attributes.getJSONObject(x);
                        String attName = attribute.getString("Name");
                        String attValue = attribute.getString("Value");
                        if(attName.equals("Frequency")){
                            String freq = attValue.replace(" MHz", "");
                            sensor.frequency = freq;
                        }else if(attName.equals("Torque Nut")){
                            sensor.torqueNut = attValue;
                        }else if(attName.equals("Torque Screw")){
                            sensor.torqueScrew = attValue;
                        }else if(attName.equals("Torque Core")){
                            sensor.torqueCore = attValue;
                        }else if(attName.equals("Housing Color")){
                            sensor.housingColor = attValue;
                        }else if(attName.equals("Sensor Type")){
                            sensor.type = attValue;
                        }else if(attName.equals("Frequency")){
                            sensor.frequency = attValue;
                        }else if(attName.equals("Part Design")){
                            sensor.partDesign = attValue;
                        }else if(attName.equals("Valve Material")){
                            sensor.valveMaterial = attValue;
                        }else if(attName.equals("Shape")){
                            sensor.shape = attValue;
                        }else if(attName.equals("Category")){
                            sensor.attCategory = attValue;
                            sensor.programmable = false;
                            if(sensor.attCategory.toUpperCase().contains("PROGRAMMABLE")){
                                sensor.programmable = true;
                            }
                        }else if(attName.equals("Sensor ID Length")){
                            sensor.idLength = attValue;
                        }else if(attName.equals("Sensor ID Type")){
                            sensor.idType = attValue;
                        }else if(attName.equals("Who sensor was made for")){
                            sensor.madeFor = attValue;
                        }
                    }
                    if(sensor.attCategory != null){
                        if(!sensor.attCategory.equals("OEM")){
                            if(sensor.programmable){
                                act.programmables.add(sensor);
                            }else{
                                if(partNumber.substring(0, 4).equals("RDE0")){
                                    String version = partNumber.substring(partNumber.length()-3, partNumber.length());
                                    if(version.equals("V21") || version.equals("V41")){
                                        act.nonprogrammables.add(sensor);
                                    }
                                }else{
                                    act.nonprogrammables.add(sensor);
                                }
                            }
                        }
                    }
                }else if(category.equals("Service Kits")){
                    if(partNumber.substring(0, 4).equals("RDV0")){
                        String[] splitPartNum = partNumber.split("[0]");
                        String svNum = splitPartNum[splitPartNum.length-1];
                        if(svNum.length() > 0){
                            if((Integer.parseInt(svNum) > 20) && (Integer.parseInt(svNum) < 28)){
                                ServiceKit serviceKit = new ServiceKit();
                                serviceKit.catalogID = catalogID;
                                serviceKit.partNumber = partNumber;
                                serviceKit.name = name;
                                serviceKit.category = category;
                                serviceKit.sameAsNumber = sameAsNumber;
                                serviceKit.alternateNumber = alternateNumber;
                                serviceKit.description = description;
                                serviceKit.relatedPartNumber = relatedPartNumber;
                                serviceKit.manufacturer = manufacturer;
                                for(int x = 0; x < attributes.length(); x++){
                                    JSONObject attribute = attributes.getJSONObject(x);
                                    String attName = attribute.getString("Name");
                                    String attValue = attribute.getString("Value");
                                    if(attName.equals("Finish")){
                                        serviceKit.finish = attValue;
                                    }else if(attName.equals("Cap")){
                                        serviceKit.cap = attValue;
                                    }else if(attName.equals("Hex Nut")){
                                        serviceKit.hexNut = attValue;
                                    }else if(attName.equals("Metal Washer")){
                                        serviceKit.metalWasher = attValue;
                                    }else if(attName.equals("Metal Valve Included")){
                                        serviceKit.metalValveIncluded = false;
                                        if(attValue.equals("Yes")){
                                            serviceKit.metalValveIncluded = true;
                                        }
                                    }else if(attName.equals("Complete Valve Assembly")){
                                        serviceKit.completeValveAssembly = false;
                                        if(attValue.equals("Yes")){
                                            serviceKit.completeValveAssembly = true;
                                        }
                                    }else if(attName.equals("Core")){
                                        serviceKit.core = attValue;
                                    }else if(attName.equals("Torque Core")){
                                        serviceKit.torqueCore = attValue;
                                    }else if(attName.equals("Torque Nut")){
                                        serviceKit.torqueNut = attValue;
                                        serviceKit.torqueScrew = attValue; //get this fixed
                                    }else if(attName.equals("Grommet")){
                                        serviceKit.grommet = attValue;
                                    }else if(attName.equals("Valve Length")){
                                        serviceKit.valveLength = attValue;
                                    }
                                }
                                act.serviceKits.add(serviceKit);
                            }
                        }
                    }
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
