package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragmentHC1000 extends Fragment {
    ActivityMore act;
    Bitmap bmp;
    boolean doOnce;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        act = (ActivityMore)getActivity();
        View view = inflater.inflate(R.layout.fragment_hc1000, container, false);
        setContent(view);
        return view;
    }
    private void setContent(View view) {
        TextView txtDesc = (TextView)view.findViewById(R.id.txtDesc);
        txtDesc.setText(getString(R.string.m_aboutHC1000));
        Button btnGuide = (Button)view.findViewById(R.id.btnGuide);
        btnGuide.setOnClickListener(clkGuide);
        Button btnBuy = (Button)view.findViewById(R.id.btnBuy);
        btnBuy.setOnClickListener(clkBuy);

    }
    View.OnClickListener clkGuide = new View.OnClickListener() {
        @Override
        public void onClick(View v){

        }
    };
    View.OnClickListener clkBuy = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.intellisens.com/distributor-locator/"));
            startActivity(i);
        }
    };
}