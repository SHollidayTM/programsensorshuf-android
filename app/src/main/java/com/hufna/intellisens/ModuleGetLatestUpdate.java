package com.hufna.intellisens;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by scottholliday on 5/17/16.
 */
public class ModuleGetLatestUpdate extends AsyncTask<Void, Void, Void> {
    ActivityMain act;
    ProgressDialog prgDialog = null;
    HttpURLConnection connection;
    Date lastUpdate;
    public void setContext(ActivityMain sentAct){
        act = sentAct;
        prgDialog = new ProgressDialog(act);
        prgDialog.setCancelable(false);
        prgDialog.setButton(DialogInterface.BUTTON_NEGATIVE, act.getString(R.string.m_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ModuleGetLatestUpdate.this.cancel(true);
                dialog.dismiss();
            }
        });
    }
    @Override
    protected void onPreExecute(){
        prgDialog.setMessage(act.getString(R.string.m_checkNewData));
        prgDialog.show();
    }
    @Override
    protected Void doInBackground(Void... params){
        if(tryToConnect() == HttpURLConnection.HTTP_OK){
            getResults();
        }else{
            ModuleGetKey getKey = new ModuleGetKey();
            String key = getKey.execute(act);
            if(key.length() > 0){
                act.editor.putString("securityKey", key);
                act.editor.commit();
                if(tryToConnect() == HttpURLConnection.HTTP_OK){
                    getResults();
                }
            }else{
                return null;
            }
        }
        return null;
    }
    @Override
    protected void onPostExecute(Void result){
        prgDialog.dismiss();
        if(lastUpdate != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try{
                Date lastLocal = sdf.parse(act.preferences.getString("newVehicleDate", "2000-01-01"));
                if(lastUpdate.after(lastLocal)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(act);
                    builder.setCancelable(false);
                    builder.setMessage(act.getString(R.string.m_newVehicleData));
                    builder.setPositiveButton(act.getString(R.string.m_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            act.attachments = new ArrayList<Attachment>();
                            act.newVehicles = new ArrayList<Vehicle>();
                            ModuleGetNewVehicles getNewVehicles = new ModuleGetNewVehicles();
                            getNewVehicles.setContext(act);
                            getNewVehicles.execute();
                            dialog.dismiss();
                        }
                    });
                    builder.setNeutralButton(act.getString(R.string.m_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
            }catch (ParseException e){
                e.printStackTrace();
            }
        }
    }
    public int tryToConnect(){
        int status = 500;
        try{
            URL url = new URL(act.preferences.getString("apiUrl", "") + "Updates/LastUpdateDate");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            String body =  "{\"securityKey\":\"" + act.preferences.getString("securityKey", "") + "\",\"regionCode\":\"NorthAmerica\"}";
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body);
            writer.close();
            status = connection.getResponseCode();
        }catch(IOException e){
            return status;
        }
        return status;
    }
    public void getResults(){
        StringBuilder response = new StringBuilder();
        String line;
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while((line = reader.readLine()) != null){
                response.append(line);
            }
            reader.close();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date = response.toString().substring(1, 11);
            try{
                lastUpdate = sdf.parse(date);
            }catch(ParseException e){
                e.printStackTrace();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
