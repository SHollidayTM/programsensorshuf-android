package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by sholliday on 1/8/16.
 */
public class FragmentRelearnInst extends Fragment {
    ActivityMain act;
    TextView txtTitle;
    Button btnPrint;
    WebView webRelearnInst;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_relearninst, container, false);
        txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        txtTitle.setText(getString(R.string.m_relearnReqs) + " " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        btnPrint = (Button)view.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(clkPrint);
        webRelearnInst = (WebView)view.findViewById(R.id.webRelearnInst);
        webRelearnInst.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(url.equals("http://activate/")){
                    act.fragService.btnCheck.callOnClick();
                    return true;
                }else if(url.equals("http://troubleshoot/")){
                    if(act.attachments.size() > 0){
                        act.openTroubleshooting();
                    }else{
                        Toast.makeText(act, getString(R.string.m_noTroubleshooting), Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });
        String html = "<p>" + getString(R.string.m_obdComing) + "</p></br>" + act.relearn.instructions + "</br><p><a href=\"http://activate\">Activate Sensors</a></p><p>" + getString(R.string.m_activationOnly) + "</p><a href=\"http://troubleshoot\">Troubleshooting</a></p>";
        webRelearnInst.loadData(html, "text/html", null);
        return view;
    }
    View.OnClickListener clkPrint = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PrintManager mgrPrint = (PrintManager)act.getSystemService(Context.PRINT_SERVICE);
            PrintDocumentAdapter adaPrint = webRelearnInst.createPrintDocumentAdapter("IntelliSens Print");
            PrintJob jobPrint = mgrPrint.print(txtTitle.getText().toString(), adaPrint, new PrintAttributes.Builder().build());
        }
    };
}
