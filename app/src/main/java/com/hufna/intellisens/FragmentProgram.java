package com.hufna.intellisens;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by sholliday on 12/22/15.
 */
public class FragmentProgram extends Fragment{
    ActivityMain act;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_program, container, false);
        TextView txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        txtTitle.setText(getString(R.string.m_program) + " " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        ImageButton btnCreate = (ImageButton)view.findViewById(R.id.btnCreate);
        btnCreate.setOnClickListener(clkCreate);
        ImageButton btnCopy = (ImageButton)view.findViewById(R.id.btnCopy);
        btnCopy.setOnClickListener(clkCopy);
        ImageButton btnCopySet = (ImageButton)view.findViewById(R.id.btnCopySet);
        btnCopySet.setOnClickListener(clkCopySet);
        return view;
    }
    View.OnClickListener clkCreate = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            act.openCreate(false);
        }
    };
    View.OnClickListener clkCopy = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            act.openCopy();
        }
    };
    View.OnClickListener clkCopySet = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.openCopySet();
        }
    };
}
