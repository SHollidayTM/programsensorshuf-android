package com.hufna.intellisens;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/10/15.
 */
public class FragmentSettings extends Fragment {
    ActivityMain act;
    RadioButton rdoDefault;
    RadioButton rdoEnglish;
    RadioButton rdoSpanish;
    RadioButton rdoFrench;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    int currLanguage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        preferences = act.getApplicationContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        editor = preferences.edit();
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        TextView txtAppVersion = (TextView)view.findViewById(R.id.txtAppVersion);
        txtAppVersion.setText(getString(R.string.m_appVersion) + " " + BuildConfig.VERSION_NAME + "\r\n" + getString(R.string.m_device) + " " + preferences.getString("deviceKey", "unknown") + " " + getString(R.string.m_firmwareVersion) + " " + preferences.getString("firmwareVersion", "unknown"));
        RadioButton rdoPsi = (RadioButton)view.findViewById(R.id.rdoPsi);
        rdoPsi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putInt("psiBar", 0);
                    editor.commit();
                }
            }
        });
        RadioButton rdoBar = (RadioButton)view.findViewById(R.id.rdoBar);
        rdoBar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putInt("psiBar", 1);
                    editor.commit();
                }
            }
        });
        int psiBar = preferences.getInt("psiBar", 0);
        switch(psiBar){
            case 0:
                rdoPsi.setChecked(true);
                rdoBar.setChecked(false);
                break;
            case 1:
                rdoPsi.setChecked(false);
                rdoBar.setChecked(true);
                break;
        }
        RadioButton rdoHex = (RadioButton)view.findViewById(R.id.rdoHex);
        rdoHex.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putInt("hexDec", 0);
                    editor.commit();
                }
            }
        });
        RadioButton rdoDec = (RadioButton)view.findViewById(R.id.rdoDec);
        rdoDec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putInt("hexDec", 1);
                    editor.commit();
                }
            }
        });
        int hexDec = preferences.getInt("hexDec", 0);
        switch(hexDec){
            case 0:
                rdoHex.setChecked(true);
                rdoDec.setChecked(false);
                break;
            case 1:
                rdoHex.setChecked(false);
                rdoDec.setChecked(true);
                break;
        }
        RadioButton rdoFahrenheit = (RadioButton)view.findViewById(R.id.rdoFahrenheit);
        rdoFahrenheit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putInt("fahCel", 0);
                    editor.commit();
                }
            }
        });
        RadioButton rdoCelsius = (RadioButton)view.findViewById(R.id.rdoCelsius);
        rdoCelsius.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putInt("fahCel", 1);
                    editor.commit();
                }
            }
        });
        int fahCel = preferences.getInt("fahCel", 0);
        switch(fahCel){
            case 0:
                rdoFahrenheit.setChecked(true);
                rdoCelsius.setChecked(false);
                break;
            case 1:
                rdoFahrenheit.setChecked(false);
                rdoCelsius.setChecked(true);
                break;
        }
        rdoDefault = (RadioButton)view.findViewById(R.id.rdoDefault);
        rdoEnglish = (RadioButton)view.findViewById(R.id.rdoEnglish);
        rdoSpanish = (RadioButton)view.findViewById(R.id.rdoSpanish);
        rdoFrench = (RadioButton)view.findViewById(R.id.rdoFrench);
        currLanguage = preferences.getInt("language", 0);
        switch(currLanguage){
            case 0:
                rdoDefault.setChecked(true);
                rdoEnglish.setChecked(false);
                rdoSpanish.setChecked(false);
                rdoFrench.setChecked(false);
                break;
            case 1:
                rdoDefault.setChecked(false);
                rdoEnglish.setChecked(true);
                rdoSpanish.setChecked(false);
                rdoFrench.setChecked(false);
                break;
            case 2:
                rdoDefault.setChecked(false);
                rdoEnglish.setChecked(false);
                rdoSpanish.setChecked(true);
                rdoFrench.setChecked(false);
                break;
            case 3:
                rdoDefault.setChecked(false);
                rdoEnglish.setChecked(false);
                rdoSpanish.setChecked(false);
                rdoFrench.setChecked(true);
                break;
        }
        rdoDefault.setOnCheckedChangeListener(chgDefault);
        rdoEnglish.setOnCheckedChangeListener(chgEnglish);
        rdoSpanish.setOnCheckedChangeListener(chgSpanish);
        rdoFrench.setOnCheckedChangeListener(chgFrench);
        Button btnBT = (Button)view.findViewById(R.id.btnBT);
        btnBT.setOnClickListener(clkBT);
        Button btnUpdate = (Button)view.findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(clkUpdate);
        Button btnFirmware = (Button)view.findViewById(R.id.btnFirmware);
        btnFirmware.setOnClickListener(clkFirmware);
        Button btnRegister = (Button)view.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(clkRegister);
        return view;
    }
    CompoundButton.OnCheckedChangeListener chgDefault = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){
                langChanged(buttonView, 0);
            }
        }
    };
    CompoundButton.OnCheckedChangeListener chgEnglish = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){
                langChanged(buttonView, 1);
            }
        }
    };
    CompoundButton.OnCheckedChangeListener chgSpanish = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){
                langChanged(buttonView, 2);
            }
        }
    };
    CompoundButton.OnCheckedChangeListener chgFrench = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){
                langChanged(buttonView, 3);
            }
        }
    };
    View.OnClickListener clkBT = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.openBT();
        }
    };
    View.OnClickListener clkUpdate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(act.activeNetwork != null && act.activeNetwork.isConnectedOrConnecting()){
                act.attachments = new ArrayList<Attachment>();
                act.newVehicles = new ArrayList<Vehicle>();
                ModuleGetNewVehicles getNewVehicles = new ModuleGetNewVehicles();
                getNewVehicles.setContext(act);
                getNewVehicles.execute();
            }else{
                act.showNoNetwork();
            }
        }
    };
    View.OnClickListener clkFirmware = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(act.activeNetwork != null && act.activeNetwork.isConnectedOrConnecting()){
                if(act.btSocket != null){
                    if(act.btSocket.isConnected()){
                        act.updateFirmware();
                    }else{
                        act.openBT();
                        Toast.makeText(act, getString(R.string.m_pleaseConnect), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    act.openBT();
                    Toast.makeText(act, getString(R.string.m_pleaseConnect), Toast.LENGTH_SHORT).show();
                }
            }else{
                act.showNoNetwork();
            }
        }
    };
    View.OnClickListener clkRegister = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(act.activeNetwork != null && act.activeNetwork.isConnectedOrConnecting()){
                Intent i = new Intent(act, ActivityRegister.class);
                startActivity(i);
            }else{
                act.showNoNetwork();
            }
        }
    };
    public void langChanged(final CompoundButton rdo, final int lang){
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);
        builder.setNeutralButton(getString(R.string.m_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                rdoDefault.setOnCheckedChangeListener(null);
                rdoEnglish.setOnCheckedChangeListener(null);
                rdoSpanish.setOnCheckedChangeListener(null);
                rdoFrench.setOnCheckedChangeListener(null);
                rdo.setChecked(false);
                switch(currLanguage){
                    case 0:
                        rdoDefault.setChecked(true);
                        break;
                    case 1:
                        rdoEnglish.setChecked(true);
                        break;
                    case 2:
                        rdoSpanish.setChecked(true);
                        break;
                    case 3:
                        rdoFrench.setChecked(true);
                        break;
                }
                rdoDefault.setOnCheckedChangeListener(chgDefault);
                rdoEnglish.setOnCheckedChangeListener(chgEnglish);
                rdoSpanish.setOnCheckedChangeListener(chgSpanish);
                rdoFrench.setOnCheckedChangeListener(chgFrench);
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.m_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                editor.putInt("language", lang);
                editor.commit();
                dialog.dismiss();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
        builder.setTitle(getString(R.string.m_langSettings));
        builder.setMessage(getString(R.string.m_langAreYouSure));
        builder.show();
    }
}
