package com.hufna.intellisens;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.provider.Settings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by scottholliday on 5/18/16.
 */
public class ModuleSendActions extends AsyncTask<Void, Void, Void> {
    ActivityMain act;
    ArrayList<Action> actions;
    String strActions;
    boolean success;
    ProgressDialog prgDialog = null;
    HttpURLConnection connection;
    public void setContext(ActivityMain sentAct, ArrayList<Action> sentActions){
        act = sentAct;
        actions = sentActions;
        StringBuilder sb = new StringBuilder();
        sb.append("\"details\":[");
        for(Action action : actions){
            sb.append("{\"eventCode\":" + action.actionID + ",\"eventDetail\":" + action.configID + "},");
        }
        sb.replace(sb.length()-1, sb.length(), "]");
        strActions = sb.toString();
        prgDialog = new ProgressDialog(act);
        prgDialog.setCancelable(false);
        prgDialog.setButton(DialogInterface.BUTTON_NEGATIVE, act.getString(R.string.m_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ModuleSendActions.this.cancel(true);
                dialog.dismiss();
            }
        });
    }
    @Override
    protected void onPreExecute(){
        prgDialog.setMessage(act.getString(R.string.m_updating));
        prgDialog.show();
    }
    @Override
    protected Void doInBackground(Void... params){
        int status = tryToConnect();
        if(status == HttpURLConnection.HTTP_OK || status == HttpURLConnection.HTTP_NO_CONTENT){
            success = true;
        }else{
            ModuleGetKey getKey = new ModuleGetKey();
            String key = getKey.execute(act);
            if(key.length() > 0){
                act.editor.putString("securityKey", key);
                act.editor.commit();
                status = tryToConnect();
                if(status == HttpURLConnection.HTTP_OK || status == HttpURLConnection.HTTP_NO_CONTENT){
                    success = true;
                }
            }else{
                return null;
            }
        }
        return null;
    }
    @Override
    protected void onPostExecute(Void result){
        act.returnSentActions(success);
        prgDialog.dismiss();
    }
    public int tryToConnect(){
        int status = 500;
        try{
            URL url = new URL(act.preferences.getString("apiUrl", "") + "Log/Add");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            String body = "{\"securityKey\":\"" + act.preferences.getString("securityKey", "") + "\",\"regionCode\":\"NorthAmerica\",\"deviceKey\":\"" + act.preferences.getString("deviceKey", "") + "\"," + strActions + "}";
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body);
            writer.close();
            status = connection.getResponseCode();
        }catch(IOException e){
            return status;
        }
        return status;
    }
}
