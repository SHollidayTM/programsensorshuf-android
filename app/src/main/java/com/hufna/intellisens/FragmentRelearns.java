package com.hufna.intellisens;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by sholliday on 12/23/15.
 */
public class FragmentRelearns extends Fragment{
    ActivityMain act;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_relearns, container, false);
        TextView txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        txtTitle.setText(getString(R.string.m_relearnReqs) + " " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        TextView txtDesc = (TextView)view.findViewById(R.id.txtDesc);
        txtDesc.setText(getString(R.string.m_relearnInst) + " " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        ImageView imvAdd = (ImageView)view.findViewById(R.id.imvAdd);
        /*
        if(!act.relearn.addAir){
            imvAdd.setSelected(true);
        }
        ImageView imvRotate = (ImageView)view.findViewById(R.id.imvRotate);
        if(!act.relearn.rotateTires){
            imvRotate.setSelected(true);
        }
        ImageView imvReplace = (ImageView)view.findViewById(R.id.imvReplace);
        if(!act.relearn.replaceSensor){
            imvReplace.setSelected(true);
        }
        */
        Button btnInstructions = (Button)view.findViewById(R.id.btnInstructions);
        btnInstructions.setOnClickListener(clkInstructions);
        Button btnOBD = (Button)view.findViewById(R.id.btnOBD);
        btnOBD.setOnClickListener(clkOBD);
        Button btnTroubleshooting = (Button)view.findViewById(R.id.btnTroubleshooting);
        btnTroubleshooting.setOnClickListener(clkTroubleshooting);
        return view;
    }
    View.OnClickListener clkInstructions = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(act.relearn.instructions != null){
                act.openRelearnInst();
            }else{
                Toast.makeText(act, getString(R.string.m_noRelearnInst), Toast.LENGTH_SHORT).show();
            }
        }
    };
    View.OnClickListener clkOBD = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            if(act.btSocket != null){
                if(act.btSocket.isConnected()){
                    act.openOBD();
                }else{
                    suggestBT();
                }
            }else{
                suggestBT();
            }
        }
    };
    View.OnClickListener clkTroubleshooting = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(act.attachments.size() > 0){
                act.openTroubleshooting();
            }else{
                Toast.makeText(act, getString(R.string.m_noTroubleshooting), Toast.LENGTH_SHORT).show();
            }
        }
    };
    public void suggestBT(){
        act.checkBT();
        Toast.makeText(act, getString(R.string.m_pleaseConnect), Toast.LENGTH_SHORT).show();
    }
}
