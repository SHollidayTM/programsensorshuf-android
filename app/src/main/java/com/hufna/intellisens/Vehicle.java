package com.hufna.intellisens;

import java.util.ArrayList;

/**
 * Created by sholliday on 1/7/16.
 */
public class Vehicle {
    public int configID;
    public int yearID;
    public int makeID;
    public int modelID;
    public String makeName;
    public String modelName;
    public int qualifierID;
    public String qualifier;
    public boolean indirect;
    public ArrayList<CommandWord> commandWords;
    public int[] attachmentIDs;
    public ArrayList<SensorNameFreq> sensorNameFreqs;
}
