package com.hufna.intellisens;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by sholliday on 12/21/15.
 */
public class FragmentDeviceDetails extends Fragment{
    ActivityMain act;
    String fileName;
    Bitmap bmp;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_devicedetails, container, false);
        TextView txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        ImageView imvDevice = (ImageView)view.findViewById(R.id.imvDevice);
        imvDevice.setOnClickListener(clkDevice);
        TextView txtDescHeader = (TextView)view.findViewById(R.id.txtDescHeader);
        TextView txtDesc = (TextView)view.findViewById(R.id.txtDesc);
        TextView txtDescHeader2 = (TextView) view.findViewById(R.id.txtDescHeader2);
        TextView txtDesc2 = (TextView) view.findViewById(R.id.txtDesc2);
        Button btnTechDocs = (Button)view.findViewById(R.id.btnTechDocs);
        btnTechDocs.setOnClickListener(clkTechDocs);
        Button btnProgram = (Button)view.findViewById(R.id.btnProgram);
        btnProgram.setOnClickListener(clkProgram);
        if(act.sensor != null){
            fileName = act.sensor.partNumber;
            txtTitle.setText(fileName);
            txtDescHeader2.setText(getString(R.string.m_torqueReqs));
            txtDesc2.setText(getString(R.string.m_torqueNut) + ": " + act.sensor.torqueNut + "\n" + getString(R.string.m_torqueScrew) + ": " + act.sensor.torqueScrew + "\n" + getString(R.string.m_torqueCore) + ": " + act.sensor.torqueCore);
            if(act.sensor.programmable){
                txtDescHeader.setText(getString(R.string.m_toolCompat));
                txtDesc.setText(getString(R.string.m_sensorRequiresTool) + " Huf HC1000\n" +
                        "Bartech Tech 400\n" +
                        "Bartech Tech 500\n" +
                        "ATEQ VT-55\n" +
                        "ATEQ VT-56\n" +
                        "Tecnomotor TPM-02");
                if((act.selectedYear == null) || (act.selectedMake == null) || (act.selectedModel == null)){
                    btnProgram.setVisibility(View.GONE);
                }
            }else{
                txtDescHeader.setVisibility(View.GONE);
                txtDesc.setVisibility(View.GONE);
                btnProgram.setVisibility(View.GONE);
            }
        }else{
            fileName = act.serviceKit.partNumber;
            txtTitle.setText(fileName);
            txtDescHeader.setVisibility(View.GONE);
            txtDesc.setText(act.serviceKit.description);
            txtDescHeader2.setVisibility(View.GONE);
            txtDesc2.setText(getString(R.string.m_torqueNut) + ": " + act.serviceKit.torqueNut + "\n" + getString(R.string.m_torqueScrew) + ": " + act.serviceKit.torqueScrew + "\n" + getString(R.string.m_torqueCore) + ": " + act.serviceKit.torqueCore);
            btnProgram.setVisibility(View.GONE);
        }
        try{
            File file = new File(act.getFilesDir(), fileName + ".jpg");
            FileInputStream input = new FileInputStream(file);
            bmp = BitmapFactory.decodeStream(input);
            imvDevice.setImageBitmap(bmp);
        }catch(IOException e){
            imvDevice.setImageResource(R.drawable.noimage);
        }
        return view;
    }
    View.OnClickListener clkDevice = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.openLargeDeviceImg(bmp);
        }
    };
    View.OnClickListener clkTechDocs = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.openTechDocs();
        }
    };
    View.OnClickListener clkProgram = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(act.btSocket != null){
                if(act.btSocket.isConnected()){
                    if(act.preferences.getInt("registered", 0) == 1){
                        act.openService();
                    }else{
                        if(act.activeNetwork != null && act.activeNetwork.isConnectedOrConnecting()){
                            Intent i = new Intent(act, ActivityRegister.class);
                            startActivity(i);
                        }else{
                            act.showNoNetwork();
                        }
                    }
                }else{
                    act.openBT();
                    Toast.makeText(act, getString(R.string.m_pleaseConnect), Toast.LENGTH_SHORT).show();
                }
            }else{
                act.openBT();
                Toast.makeText(act, getString(R.string.m_pleaseConnect), Toast.LENGTH_SHORT).show();
            }
        }
    };
}
