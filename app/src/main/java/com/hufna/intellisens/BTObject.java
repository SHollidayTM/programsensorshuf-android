package com.hufna.intellisens;

import android.bluetooth.BluetoothDevice;

/**
 * Created by sholliday on 12/16/15.
 */
public class BTObject {
    BluetoothDevice device;
    String name;
    String address;
    String serialNum;
    String firmwareVersion;
    String hardwareVersion;
    String databaseVersion;
    boolean obdDevice;
    boolean connected;
}
