package com.hufna.intellisens;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by sholliday on 12/23/15.
 */
public class FragmentCopySet extends Fragment{
    ActivityMain act;
    ImageButton btnLeftFront;
    ImageButton btnRightFront;
    ImageButton btnRightRear;
    ImageButton btnLeftRear;
    ImageButton btnSpare;
    ImageButton btnLeftFrontCopy;
    ImageButton btnRightFrontCopy;
    ImageButton btnLeftRearCopy;
    ImageButton btnRightRearCopy;
    ImageButton btnSpareCopy;
    TextView txtLeftFront;
    TextView txtRightFront;
    TextView txtRightRear;
    TextView txtLeftRear;
    TextView txtSpare;
    LinearLayout layRelearnInst;
    TextView txtRelearnTitle;
    WebView webRelearnInst;
    Button btnPrint;
    LinearLayout laySelect;
    ArrayList<SensorNameFreq> metal;
    ArrayList<SensorNameFreq> rubber;
    ArrayList<String> metalNames;
    ArrayList<String> rubberNames;
    ListView lstMetal;
    ListView lstRubber;
    int copySensor;
    ArrayList<String> strIDs;
    int checkCount;
    boolean rechecking;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        act = (ActivityMain)getActivity();
        View view = inflater.inflate(R.layout.fragment_copyset, container, false);
        btnLeftFront = (ImageButton)view.findViewById(R.id.btnLeftFront);
        btnLeftFront.setOnClickListener(clkLeftFront);
        btnRightFront = (ImageButton)view.findViewById(R.id.btnRightFront);
        btnRightFront.setOnClickListener(clkRightFront);
        btnRightRear = (ImageButton)view.findViewById(R.id.btnRightRear);
        btnRightRear.setOnClickListener(clkRightRear);
        btnLeftRear = (ImageButton)view.findViewById(R.id.btnLeftRear);
        btnLeftRear.setOnClickListener(clkLeftRear);
        btnSpare = (ImageButton)view.findViewById(R.id.btnSpare);
        btnSpare.setOnClickListener(clkSpare);
        btnLeftFrontCopy = (ImageButton)view.findViewById(R.id.btnLeftFrontCopy);
        btnLeftFrontCopy.setEnabled(false);
        btnLeftFrontCopy.setOnClickListener(clkLeftFrontCopy);
        btnRightFrontCopy = (ImageButton)view.findViewById(R.id.btnRightFrontCopy);
        btnRightFrontCopy.setEnabled(false);
        btnRightFrontCopy.setOnClickListener(clkRightFrontCopy);
        btnRightRearCopy = (ImageButton)view.findViewById(R.id.btnRightRearCopy);
        btnRightRearCopy.setEnabled(false);
        btnRightRearCopy.setOnClickListener(clkRightRearCopy);
        btnLeftRearCopy = (ImageButton)view.findViewById(R.id.btnLeftRearCopy);
        btnLeftRearCopy.setEnabled(false);
        btnLeftRearCopy.setOnClickListener(clkLeftRearCopy);
        btnSpareCopy = (ImageButton)view.findViewById(R.id.btnSpareCopy);
        btnSpareCopy.setEnabled(false);
        btnSpareCopy.setOnClickListener(clkSpareCopy);
        txtLeftFront = (TextView)view.findViewById(R.id.txtLeftFront);
        txtLeftFront.setMovementMethod(new ScrollingMovementMethod());
        txtRightFront = (TextView)view.findViewById(R.id.txtRightFront);
        txtRightFront.setMovementMethod(new ScrollingMovementMethod());
        txtRightRear = (TextView)view.findViewById(R.id.txtRightRear);
        txtRightRear.setMovementMethod(new ScrollingMovementMethod());
        txtLeftRear = (TextView)view.findViewById(R.id.txtLeftRear);
        txtLeftRear.setMovementMethod(new ScrollingMovementMethod());
        txtSpare = (TextView)view.findViewById(R.id.txtSpare);
        txtSpare.setMovementMethod(new ScrollingMovementMethod());
        layRelearnInst = (LinearLayout)view.findViewById(R.id.layRelearnInst);
        txtRelearnTitle = (TextView)view.findViewById(R.id.txtRelearnTitle);
        txtRelearnTitle.setText(getString(R.string.m_relearnReqs) + " " + act.selectedYear + " " + act.selectedMake.makeName + " " + act.selectedModel.modelName);
        btnPrint = (Button)view.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(clkPrint);
        webRelearnInst = (WebView)view.findViewById(R.id.webRelearnInst);
        laySelect = (LinearLayout)view.findViewById(R.id.laySelect);
        lstMetal = (ListView)view.findViewById(R.id.lstMetal);
        lstRubber = (ListView)view.findViewById(R.id.lstRubber);
        strIDs = new ArrayList<String>();
        for(int i = 0; i < 5; i++){
            strIDs.add("");
        }
        return view;
    }
    View.OnClickListener clkLeftFront = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            copySensor = 0;
            if(btnLeftFront.isSelected()){
                askForCheckAgain();
            }else{
                copy();
            }
        }
    };
    View.OnClickListener clkRightFront = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            copySensor = 1;
            if(btnRightFront.isSelected()){
                askForCheckAgain();
            }else{
                copy();
            }
        }
    };
    View.OnClickListener clkRightRear = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            copySensor = 2;
            if(btnRightRear.isSelected()){
                askForCheckAgain();
            }else{
                copy();
            }
        }
    };
    View.OnClickListener clkLeftRear = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            copySensor = 3;
            if(btnLeftRear.isSelected()){
                askForCheckAgain();
            }else{
                copy();
            }
        }
    };
    View.OnClickListener clkSpare = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            checkCount = 0;
            copySensor = 4;
            if(btnSpare.isSelected()){
                askForCheckAgain();
            }else{
                copy();
            }
        }
    };
    public void askForCheckAgain(){
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
                rechecking = true;
                act.doCheck();
            }
        });
        builder.setNeutralButton(getString(R.string.m_cancel), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.setMessage(getString(R.string.m_checkSensorAgain));
        builder.show();
    }
    public void copy(){
        act.copySensor = copySensor;
        act.doCopySet();
    }
    public void copySetResult(String currID, String strPressure, String strTemp, String strFreq, String strBat){
        boolean duplicate = false;
        if(!rechecking){
            for(String prevID : strIDs){
                if(currID.equals(prevID)){
                    if(checkCount < 2){
                        checkCount++;
                        act.doCheck();
                        return;
                    }
                    duplicate = true;
                    break;
                }
            }
        }
        rechecking = false;
        if(duplicate){
            switch(copySensor){
                case 0: //LF
                    btnLeftFront.setSelected(false);
                    btnLeftFrontCopy.setEnabled(false);
                    break;
                case 1: //RF
                    btnRightFront.setSelected(false);
                    btnRightFrontCopy.setEnabled(false);
                    break;
                case 2: //RR
                    btnRightRear.setSelected(false);
                    btnRightRearCopy.setEnabled(false);
                    break;
                case 3: //LR
                    btnLeftRear.setSelected(false);
                    btnLeftRearCopy.setEnabled(false);
                    break;
                case 4: //SP
                    btnSpare.setSelected(false);
                    btnSpareCopy.setEnabled(false);
                    break;
            }
            Toast.makeText(act, getString(R.string.m_duplicateSensor), Toast.LENGTH_SHORT).show();
        }else{
            strIDs.set(copySensor, currID);
            TextView txtView = null;
            switch(copySensor){
                case 0: //LF
                    btnLeftFront.setSelected(true);
                    btnLeftFrontCopy.setEnabled(true);
                    txtView = txtLeftFront;
                    break;
                case 1: //RF
                    btnRightFront.setSelected(true);
                    btnRightFrontCopy.setEnabled(true);
                    txtView = txtRightFront;
                    break;
                case 2: //RR
                    btnRightRear.setSelected(true);
                    btnRightRearCopy.setEnabled(true);
                    txtView = txtRightRear;
                    break;
                case 3: //LR
                    btnLeftRear.setSelected(true);
                    btnLeftRearCopy.setEnabled(true);
                    txtView = txtLeftRear;
                    break;
                case 4: //SP
                    btnSpare.setSelected(true);
                    btnSpareCopy.setEnabled(true);
                    txtView = txtSpare;
                    break;
            }
            final String strMsg = getString(R.string.m_id) + ": " + currID + "\r\n" + getString(R.string.m_mhz) + ": " + strFreq;
            AlertDialog.Builder builder = new AlertDialog.Builder(act);
            builder.setCancelable(false);
            final TextView finalTxtView = txtView;
            builder.setNeutralButton(getString(R.string.m_copySensor), new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    create();
                    finalTxtView.setText(strMsg);
                    finalTxtView.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    finalTxtView.setText(strMsg);
                    finalTxtView.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            });
            builder.setTitle(getString(R.string.m_readSuccessful));
            builder.setMessage(strMsg);
            builder.show();
        }
    }
    View.OnClickListener clkLeftFrontCopy = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            copySensor = 0;
            create();
        }
    };
    View.OnClickListener clkRightFrontCopy = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            copySensor = 1;
            create();
        }
    };
    View.OnClickListener clkRightRearCopy = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            copySensor = 2;
            create();
        }
    };
    View.OnClickListener clkLeftRearCopy = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            copySensor = 3;
            create();
        }
    };
    View.OnClickListener clkSpareCopy = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            copySensor = 4;
            create();
        }
    };
    public void create(){
        act.copySensor = copySensor;
        if(strIDs.get(copySensor).length() > 0){
            if(act.sensor == null){
                laySelect.setVisibility(View.VISIBLE);
                metal = new ArrayList<SensorNameFreq>();
                rubber = new ArrayList<SensorNameFreq>();
                metalNames = new ArrayList<String>();
                rubberNames = new ArrayList<String>();
                for(SensorNameFreq sensorNameFreq : act.vehicle.sensorNameFreqs){
                    if(sensorNameFreq.valveMaterial != null){
                        if(sensorNameFreq.valveMaterial.equals("M")){
                            metal.add(sensorNameFreq);
                            metalNames.add(sensorNameFreq.name);
                        }else{
                            rubber.add(sensorNameFreq);
                            rubberNames.add(sensorNameFreq.name);
                        }
                    }
                }
                ArrayAdapter<String> adaMetal = new ArrayAdapter<String>(act, R.layout.item_centered, metalNames);
                lstMetal.setAdapter(adaMetal);
                lstMetal.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3){
                        act.sensorNameFreq = metal.get(position);
                        act.doCopySetCreate();
                    }
                });
                ArrayAdapter<String> adaRubber = new ArrayAdapter<String>(act, R.layout.item_centered, rubberNames);
                lstRubber.setAdapter(adaRubber);
                lstRubber.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3){
                        act.sensorNameFreq = rubber.get(position);
                        act.doCopySetCreate();
                    }
                });
            }else{
                act.doCopySetCreate();
            }
        }else{
            Toast.makeText(act, getString(R.string.m_readASensor), Toast.LENGTH_SHORT).show();
        }
    }
    public void createResult(String currID, String strFreq){
        switch(copySensor){
            case 0: //LF
                btnLeftFrontCopy.setSelected(true);
                break;
            case 1: //RF
                btnRightFrontCopy.setSelected(true);
                break;
            case 2: //RR
                btnRightRearCopy.setSelected(true);
                break;
            case 3: //LR
                btnLeftRearCopy.setSelected(true);
                break;
            case 4: //SP
                btnSpareCopy.setSelected(true);
                break;
        }
        laySelect.setVisibility(View.GONE);
        String strMsg = getString(R.string.m_id) + ": " + currID + "\r\n" + getString(R.string.m_mhz) + ": " + strFreq;
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);
        builder.setNeutralButton(getString(R.string.m_relearn), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                webRelearnInst.loadData(act.relearn.instructions, "text/html", null);
                layRelearnInst.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.setTitle(getString(R.string.m_sensorCopied));
        builder.setMessage(strMsg);
        builder.show();
    }
    View.OnClickListener clkPrint = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PrintManager mgrPrint = (PrintManager)act.getSystemService(Context.PRINT_SERVICE);
            PrintDocumentAdapter adaPrint = webRelearnInst.createPrintDocumentAdapter("IntelliSens Print");
            PrintJob jobPrint = mgrPrint.print(txtRelearnTitle.getText().toString(), adaPrint, new PrintAttributes.Builder().build());
        }
    };
}
