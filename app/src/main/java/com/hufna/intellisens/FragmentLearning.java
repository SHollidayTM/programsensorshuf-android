package com.hufna.intellisens;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class FragmentLearning extends Fragment {
    ActivityMore act;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        act = (ActivityMore)getActivity();
        View view = inflater.inflate(R.layout.fragment_learning, container, false);
        setListContent(view);
        return view;
    }
    private void setListContent(View view){
        ArrayList<String> options = new ArrayList<String>();
        options.add(getString(R.string.m_training));
        options.add(getString(R.string.m_techDocs));
        options.add(getString(R.string.m_faq));
        options.add(getString(R.string.m_about) + " HC1000");
        ListView lstOptions = (ListView)view.findViewById(R.id.lstOptions);
        ArrayAdapter<String> ada = new ArrayAdapter<String>(act, R.layout.item_centered, options);
        lstOptions.setAdapter(ada);
        lstOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                Fragment fragment = null;
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                switch(position){
                    case 0:
                        fragment = new FragmentVideos();
                        fragmentTransaction.replace(R.id.layFrags, fragment, "videos");
                        break;
                    case 1:
                        fragment = new FragmentTechDocs();
                        fragmentTransaction.replace(R.id.layFrags, fragment, "techdocs");
                        break;
                    case 2:
                        fragment = new FragmentFAQ();
                        fragmentTransaction.replace(R.id.layFrags, fragment, "faq");
                        break;
                    case 3:
                        fragment = new FragmentHC1000();
                        fragmentTransaction.replace(R.id.layFrags, fragment, "hc1000");
                        break;
                }
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}