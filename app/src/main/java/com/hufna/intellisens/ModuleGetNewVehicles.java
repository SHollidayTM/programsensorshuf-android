package com.hufna.intellisens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by sholliday on 1/11/16.
 */
public class ModuleGetNewVehicles extends AsyncTask<Void, Void, Void>{
    ActivityMain act;
    ProgressDialog prgDialog = null;
    HttpURLConnection connection;
    public void setContext(ActivityMain sentAct){
        act = sentAct;
        prgDialog = new ProgressDialog(act);
        prgDialog.setCancelable(false);
        prgDialog.setButton(DialogInterface.BUTTON_NEGATIVE, act.getString(R.string.m_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ModuleGetNewVehicles.this.cancel(true);
                dialog.dismiss();
            }
        });
    }
    @Override
    protected void onPreExecute(){
        prgDialog.setMessage(act.getString(R.string.m_updating));
        prgDialog.show();
    }
    @Override
    protected Void doInBackground(Void... params){
        if(tryToConnect() == HttpURLConnection.HTTP_OK){
            getResults();
        }else{
            ModuleGetKey getKey = new ModuleGetKey();
            String key = getKey.execute(act);
            if(key.length() > 0){
                act.editor.putString("securityKey", key);
                act.editor.commit();
                if(tryToConnect() == HttpURLConnection.HTTP_OK){
                    getResults();
                }
            }else{
                return null;
            }
        }
        return null;
    }
    @Override
    protected void onPostExecute(Void result){
        act.returnAttachments();
        act.returnNewVehicles();
        prgDialog.dismiss();
    }
    public int tryToConnect(){
        int status = 500;
        try{
            URL url = new URL(act.preferences.getString("apiUrl", "") + "Vehicle/GetNew");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            String body =  "{\"securityKey\":\"" + act.preferences.getString("securityKey", "") + "\",\"minChangeDate\":\"" + act.preferences.getString("newVehicleDate", "") + "\",\"programmableOnly\":1}";
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body);
            writer.close();
            status = connection.getResponseCode();
        }catch(IOException e){
            return status;
        }
        return status;
    }
    public void getResults(){
        StringBuilder response = new StringBuilder();
        String line;
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while((line = reader.readLine()) != null){
                response.append(line);
            }
            reader.close();
            parseJson(response.toString());
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    public void parseJson(String json){
        try{
            JSONObject wholeThing = new JSONObject(json);
            JSONArray arrAttachments = wholeThing.getJSONArray("Attachments");
            if(arrAttachments.length() > 0){
                act.attachments = new ArrayList<Attachment>();
                for(int i = 0; i < arrAttachments.length(); i++){
                    JSONObject object = arrAttachments.getJSONObject(i);
                    Attachment attachment = new Attachment();
                    attachment.attachmentID = object.getInt("AttachmentID");
                    attachment.level = object.getString("Level");
                    attachment.name = object.getString("Name");
                    attachment.text = object.getString("Base64Text");
                    attachment.url = object.getString("DocumentUrl");
                    act.attachments.add(attachment);
                }
            }
            act.newVehicles = new ArrayList<Vehicle>();
            JSONArray arrVehicles = wholeThing.getJSONArray("Configs");
            for(int i = 0; i < arrVehicles.length(); i++){
                JSONObject object = arrVehicles.getJSONObject(i);
                Vehicle vehicle = new Vehicle();
                vehicle.configID = object.getInt("VehicleConfigurationID");
                vehicle.yearID = object.getInt("AcesYearID");
                vehicle.makeID = object.getInt("AcesMakeID");
                vehicle.modelID = object.getInt("AcesModelID");
                vehicle.makeName = object.getString("MakeName");
                vehicle.modelName = object.getString("ModelName");
                if(!object.getString("QualifierID").equals("null")){
                    vehicle.qualifierID = object.getInt("QualifierID");
                    vehicle.qualifier = object.getString("QualifierText");
                }
                vehicle.indirect = false;
                if(object.getString("IndirectSystemFlag").equals("true")){
                    vehicle.indirect = true;
                }
                JSONArray arrCommands = new JSONArray("[" + object.getString("Commands") + "]");
                vehicle.commandWords = new ArrayList<CommandWord>();
                for(int x = 0; x < arrCommands.length(); x++){
                    JSONObject command = arrCommands.getJSONObject(x);
                    CommandWord cmdWord = new CommandWord();
                    if(!command.getString("CheckCommand").equals("null")){
                        cmdWord.checkWord = Integer.parseInt(command.getString("CheckCommand"));
                    }
                    String hexChars = command.getString("SelectCommand");
                    if(!hexChars.equals("null")){
                        hexChars = hexChars.substring(hexChars.length()-2);
                        cmdWord.selectWord = Integer.parseInt(hexChars, 16);
                    }
                    hexChars = command.getString("SoftwareVersion");
                    if(!hexChars.equals("null")){
                        hexChars = hexChars.substring(hexChars.length()-2);
                        cmdWord.softwareVersion = Integer.parseInt(hexChars, 16);
                    }
                    vehicle.commandWords.add(cmdWord);
                }
                JSONArray arrAttachIDs = object.getJSONArray("AttachmentIDs");
                int[] attIDs = new int[arrAttachIDs.length()];
                for(int x = 0; x < attIDs.length; x++){
                    attIDs[x] = arrAttachIDs.getInt(x);
                }
                vehicle.attachmentIDs = attIDs;
                vehicle.sensorNameFreqs = new ArrayList<SensorNameFreq>();
                if(!object.getString("Sensors").equals("null")){
                    JSONArray arrSensors = new JSONArray(object.getString("Sensors"));
                    for(int x = 0; x < arrSensors.length(); x++){
                        JSONObject sensor = arrSensors.getJSONObject(x);
                        SensorNameFreq sensorNameFreq = new SensorNameFreq();
                        sensorNameFreq.name = sensor.getString("SensorName");
                        String freq = sensor.getString("Frequency").replace(" MHz", "");
                        sensorNameFreq.frequency = freq;
                        String fileName = sensorNameFreq.name + ".jpg";
                        File file = new File(act.getFilesDir(), fileName);
                        if(!file.exists()){
                            try{
                                InputStream inputStream = act.getAssets().open(fileName);
                                Bitmap bmp = BitmapFactory.decodeStream(inputStream);
                                FileOutputStream output;
                                output = act.openFileOutput(fileName, Context.MODE_PRIVATE);
                                bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
                                output.close();
                            }catch(IOException e){
                                String imageUrl = sensor.getString("SensorImageUrl");
                                try{
                                    InputStream inputStream = new URL("http:" + imageUrl).openStream();
                                    Bitmap bmp = BitmapFactory.decodeStream(inputStream);
                                    FileOutputStream output;
                                    output = act.openFileOutput(fileName, Context.MODE_PRIVATE);
                                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
                                    output.close();
                                }catch(IOException ex){
                                }
                            }
                        }
                        vehicle.sensorNameFreqs.add(sensorNameFreq);
                    }
                }
                act.newVehicles.add(vehicle);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
