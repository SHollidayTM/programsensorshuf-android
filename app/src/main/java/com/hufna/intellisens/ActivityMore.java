package com.hufna.intellisens;

/**
 * Created by sholliday on 12/10/15.
 */
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class ActivityMore extends Activity {
    LinearLayout layFrags;
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_more);
        layFrags = (LinearLayout)ActivityMore.this.findViewById(R.id.layFrags);
        Button btnLearning = (Button)findViewById(R.id.btnLearning);
        btnLearning.setOnClickListener(clkLearning);
        Button btnHistory = (Button)findViewById(R.id.btnHistory);
        btnHistory.setOnClickListener(clkHistory);
        Button btnAbout = (Button)findViewById(R.id.btnAbout);
        btnAbout.setOnClickListener(clkAbout);
        Button btnBuy = (Button)findViewById(R.id.btnBuy);
        btnBuy.setOnClickListener(clkBuy);
        LinearLayout layCallUs = (LinearLayout)findViewById(R.id.layCallUs);
        layCallUs.setOnClickListener(clkCallUs);
        LinearLayout layEmailUs = (LinearLayout)findViewById(R.id.layEmailUs);
        layEmailUs.setOnClickListener(clkEmailUs);
        LinearLayout layWebsite = (LinearLayout)findViewById(R.id.layWebsite);
        layWebsite.setOnClickListener(clkOpenFullSite);
        LinearLayout layPrivacyPolicy = (LinearLayout) findViewById(R.id.layPrivacyPolicy);
        layPrivacyPolicy.setOnClickListener(clkOpenPrivacyPolicy);

        //for testing only
        LinearLayout layDefects = (LinearLayout)findViewById(R.id.layDefects);
        layDefects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://goo.gl/forms/NDNtS772Tl"));
                startActivity(i);
            }
        });
        //
    }
    View.OnClickListener clkLearning = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment = new FragmentLearning();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.layFrags, fragment, "learning");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            layFrags.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        }
    };
    View.OnClickListener clkHistory = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(ActivityMore.this, ActivityHistory.class);
            startActivity(i);
        }
    };
    View.OnClickListener clkAbout = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMore.this);
            builder.setCancelable(true);
            builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setTitle(getString(R.string.m_aboutHuf));
            builder.setMessage(getString(R.string.m_hufDesc) + "\n\n" + getString(R.string.m_copyright));
            builder.show();
        }
    };
    View.OnClickListener clkBuy = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.intellisens.com/distributor-locator/"));
            startActivity(i);
        }
    };
    View.OnClickListener clkCallUs = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMore.this);
            builder.setTitle(getString(R.string.m_contactHuf));
            TelephonyManager manager = (TelephonyManager)ActivityMore.this.getSystemService(Context.TELEPHONY_SERVICE);
            if(manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
                builder.setMessage(getString(R.string.m_callHuf) + ".");
                builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }else{
                builder.setMessage(getString(R.string.m_callHuf) + "?");
                builder.setPositiveButton(getString(R.string.m_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:18005551212"));
                        startActivity(i);
                    }
                });
                builder.setNeutralButton(getString(R.string.m_no), null);
            }
            builder.show();
        }
    };
    View.OnClickListener clkEmailUs = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMore.this);
            builder.setTitle(getString(R.string.m_contactHuf));
            builder.setMessage(getString(R.string.m_emailHuf) + "?");
            builder.setPositiveButton(getString(R.string.m_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@huf.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.m_emailFromApp));
                    try{
                        startActivity(i);
                    }catch(android.content.ActivityNotFoundException ex){
                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMore.this);
                        builder.setCancelable(true);
                        builder.setPositiveButton(getString(R.string.m_ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.setTitle(getString(R.string.m_noEmailClient));
                        builder.setMessage(getString(R.string.m_noEmailClientDevice));
                        builder.show();
                    }
                }
            });
            builder.setNeutralButton(getString(R.string.m_no), null);
            builder.show();
        }
    };
    View.OnClickListener clkOpenYouTube = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCMYeWf62-sQLF4ON0ITyq7w"));
            startActivity(i);
        }
    };
    View.OnClickListener clkOpenFullSite = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.intellisens.com"));
            startActivity(i);
        }
    };
    View.OnClickListener clkOpenPrivacyPolicy = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.intellisens.com/10/privacy-policy/"));
            startActivity(i);
        }
    };
    public void closeFrags(){
        while(getFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStackImmediate();
        }
        layFrags.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    }
    @Override
    public void onBackPressed(){
        back();
    }
    public void back(){
        int count = getFragmentManager().getBackStackEntryCount();
        if(count == 0){
            finish();
        }else{
            getFragmentManager().popBackStack();
            if(count == 1){
                closeFrags();
            }
        }
    }
}
