package com.hufna.intellisens;

import java.util.Date;

/**
 * Created by scottholliday on 5/3/16.
 */
public class History {
    public Date date;
    public String year;
    public String make;
    public String model;
    public String action;
}
