package com.hufna.intellisens;

import android.app.AlertDialog;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by sholliday on 1/4/16.
 */
public class ModuleBT extends Thread{
    ActivityMain act;
    private final BluetoothSocket btSocket;
    private final InputStream inStream;
    private final OutputStream outStream;
    boolean connected = false;
    StringBuilder sb = new StringBuilder();
    byte[] txArray;
    byte[] resultSensorID;
    byte sensorPressure;
    byte sensorTemp;
    byte sensorBat;
    byte[] foundSensorID;
    boolean writing;
    public boolean copy;
    public boolean copySet;
    byte[][] copySetIDs = new byte[5][4];
    InputStream inputStream;
    byte[] readBytes;
    int partCounter;
    int partTotal;
    byte[] totalParts;
    public int cmdWordsCount;
    public boolean fetchingFirmware;
    byte[] fetchedData;
    boolean activateWithTool;
    boolean obdIdsGathered;
    String fwVersion = "960";
    int hcFWVersion;

    public ModuleBT(ActivityMain activity, BluetoothSocket socket){
        act = activity;
        btSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
        try{
            tmpIn = btSocket.getInputStream();
            tmpOut = btSocket.getOutputStream();
        }catch(IOException e){
            Toast.makeText(act, act.getString(R.string.m_unableToCommunicate), Toast.LENGTH_SHORT).show();
            cancel();
        }
        connected = true;
        inStream = tmpIn;
        outStream = tmpOut;
    }
    public void write(byte[] bytes){
        try{
            outStream.write(bytes);
        }catch(IOException e){
        }
    }
    public void run(){
        boolean runThread = true;
        boolean msgStarted = false;
        int msgLength = 0;
        byte commandByte;
        byte[] dataBytes = new byte[256];
        byte[] rxLog = new byte[256];
        while(runThread){
            try{
                if(!msgStarted){
                    if((byte)inStream.read() == (byte)0x8F){
                        msgStarted = true; //message starting from HC1000
                        rxLog[0] = (byte)0x8F;
                    }
                }else if(msgStarted && (msgLength == 0)){ //we're talking to HC1000, but haven't recorded anything yet
                    inStream.read(); //first byte on length is always zero, so read it and forget it
                    rxLog[1] = (byte)0x00;
                    msgLength = inStream.read(); //get the message length
                    rxLog[2] = (byte)msgLength;
                }else if(msgStarted && (msgLength > 0)){ //we're reading a message and we know it's length
                    commandByte = (byte)inStream.read(); //get the command byte
                    rxLog[3] = commandByte;
                    for(int i = 4; i < msgLength; i++){ //read and record from the first data byte until the length
                        dataBytes[i-4] = (byte)inStream.read();
                        rxLog[i] = dataBytes[i-4];
                    }
                    sb = new StringBuilder();
                    for(int i = 0; i < msgLength; i++){
                        byte b = rxLog[i];
                        sb.append(String.format("0x%x ", b));
                    }
                    Log.d("BTCOMM RX", sb.toString().trim());
                    //check sensor block
                    if(commandByte == (byte)0xA0){ //ack check sensor
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x61, (byte)0xF4};
                        write(txArray); //check progress
                        txLog();
                    }else if(commandByte == (byte)0xA1){ //ack progress
                        if(dataBytes[0] == (byte)0x00){ //still in progress
                            txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x61, (byte)0xF4};
                            write(txArray); //try again
                            txLog();
                            act.updateProgressHC();
                        }else if(dataBytes[0] == (byte)0x01){ //ack success
                            txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x62, (byte)0xF5};
                            write(txArray); //gather sensor info
                            txLog();
                        }else if(dataBytes[0] == (byte)0x02){ //error or timeout
                            if(act.checkingFreq){
                                finishDiscoverFreq();
                            }else{
                                finishCheck();
                            }
                        }
                    }else if(commandByte == (byte)0xA2){ //sensor results received
                        if(act.checkingFreq){
                            finish(5);
                        }else{
                            resultSensorID = new byte[4];
                            resultSensorID[0] = dataBytes[0];
                            resultSensorID[1] = dataBytes[1];
                            resultSensorID[2] = dataBytes[2];
                            resultSensorID[3] = dataBytes[3];
                            sensorPressure = dataBytes[4];
                            sensorTemp = dataBytes[5];
                            sensorBat = dataBytes[6];
                            finish(1);
                        }
                    }
                    //program sensor block
                    if(commandByte == (byte)0xC0){ //ack we're programming a sensor
                        if(copySet){ //pull data from previously copied set
                            resultSensorID = new byte[4];
                            resultSensorID[0] = (byte)copySetIDs[act.copySensor][0];
                            resultSensorID[1] = (byte)copySetIDs[act.copySensor][1];
                            resultSensorID[2] = (byte)copySetIDs[act.copySensor][2];
                            resultSensorID[3] = (byte)copySetIDs[act.copySensor][3];
                        }else if(!copy){ //create random id
                            resultSensorID = new byte[4];
                            Random rand = new Random();
                            CommandWord cmdWord = act.vehicle.commandWords.get(cmdWordsCount);
                            if(cmdWord.hasRange){
                                if(Math.abs(cmdWord.highByteZero - cmdWord.lowByteZero) == 0){
                                    resultSensorID[0] = (byte)cmdWord.lowByteZero;
                                }else{
                                    resultSensorID[0] = (byte)(cmdWord.lowByteZero + rand.nextInt(Math.abs(cmdWord.highByteZero - cmdWord.lowByteZero)));
                                }
                                if(Math.abs(cmdWord.highByteOne - cmdWord.lowByteOne) == 0){
                                    resultSensorID[1] = (byte)cmdWord.lowByteOne;
                                }else{
                                    resultSensorID[1] = (byte)(cmdWord.lowByteOne + rand.nextInt(Math.abs(cmdWord.highByteOne - cmdWord.lowByteOne)));
                                }
                                if(Math.abs(cmdWord.highByteTwo - cmdWord.lowByteTwo) == 0){
                                    resultSensorID[2] = (byte)cmdWord.lowByteTwo;
                                }else{
                                    resultSensorID[2] = (byte)(cmdWord.lowByteTwo + rand.nextInt(Math.abs(cmdWord.highByteTwo - cmdWord.lowByteTwo)));
                                }
                                if(Math.abs(cmdWord.highByteThree - cmdWord.lowByteThree) == 0){
                                    resultSensorID[3] = (byte)cmdWord.lowByteThree;
                                }else{
                                    resultSensorID[3] = (byte)(cmdWord.lowByteThree + rand.nextInt(Math.abs(cmdWord.highByteThree - cmdWord.lowByteThree)));
                                }
                            }else{
                                resultSensorID[0] = (byte)rand.nextInt();
                                resultSensorID[1] = (byte)rand.nextInt();
                                resultSensorID[2] = (byte)rand.nextInt();
                                resultSensorID[3] = (byte)rand.nextInt();
                            }
                        }
                        byte sum = (byte)((byte)0x8E + (byte)0x09 + (byte)0x81 + resultSensorID[0] + resultSensorID[1] + resultSensorID[2] + resultSensorID[3]);
                        txArray = new byte[]{(byte) 0x8E, (byte) 0x00, (byte) 0x09, (byte) 0x81, resultSensorID[0], resultSensorID[1], resultSensorID[2], resultSensorID[3], sum};
                        write(txArray); //provide ID
                        txLog();
                    }else if(commandByte == (byte)0xC1){ //ack ID accepted
                        CommandWord cmdWord = act.vehicle.commandWords.get(cmdWordsCount);
                        byte sum = (byte)((byte)0x8E + (byte)0x00 + (byte)0x0C + (byte)0x90 + (byte)0x09 + act.mhz + (byte)0x00 + (byte)cmdWord.selectWord + (byte)0x00 + (byte)cmdWord.softwareVersion + (byte)0x00);
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x0C, (byte)0x90, (byte)0x09, act.mhz, (byte)0x00, (byte)cmdWord.selectWord, (byte)0x00, (byte)cmdWord.softwareVersion, (byte)0x00, sum};
                        write(txArray); //setup sensor type
                        txLog();
                    }else if(commandByte == (byte)0xD0){ //ack setup accepted
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x91, (byte)0x24};
                        write(txArray); //start wake
                        txLog();
                    }else if(commandByte == (byte)0xD1){ //ack wake started
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x92, (byte)0x25}; //get wake progress
                        write(txArray);
                        txLog();
                    }else if(commandByte == (byte)0xD2){ //return wake progress
                        if(dataBytes[0] == (byte)0x01){ //success
                            txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x93, (byte)0x26}; //get wake results
                            write(txArray);
                            txLog();
                        }else if(dataBytes[0] == (byte)0x02){ //error or timeout
                            finishCreate();
                        }else{
                            write(new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x92, (byte)0x25}); //try again
                            txLog();
                            act.updateProgressHC();
                        }
                    }else if(commandByte == (byte)0xD3){ //wake results
                        foundSensorID = new byte[4];
                        foundSensorID[0] = dataBytes[1];
                        foundSensorID[1] = dataBytes[2];
                        foundSensorID[2] = dataBytes[3];
                        foundSensorID[3] = dataBytes[4];
                        byte sum = (byte)((byte)0x8E + (byte)0x09 + (byte)0x94 + foundSensorID[0] + foundSensorID[1] + foundSensorID[2] + foundSensorID[3]);
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x09, (byte)0x94, foundSensorID[0], foundSensorID[1], foundSensorID[2], foundSensorID[3], sum}; //choose sensor to program
                        write(txArray);
                        txLog();
                    }else if(commandByte == (byte)0xD4){ //ack sensor choice accepted
                        writing = true;
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x98, (byte)0x2B}; //check writing status
                        write(txArray);
                        txLog();
                    }else if(commandByte == (byte)0xD8){ //return writing or lock results
                        if(writing){
                            if(dataBytes[0] == (byte) 0x00){ //timeout
                                finishCreate();
                            }else if(dataBytes[0] == (byte) 0x01){ //success
                                txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x06, (byte)0x95, (byte)0x00, (byte)0x29}; //lock it in
                                write(txArray);
                                txLog();
                            }else{
                                txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x98, (byte)0x2B}; //try again
                                write(txArray);
                                txLog();
                                act.updateProgressHC();
                            }
                        }else{
                            if(dataBytes[0] == (byte)0x00){ //timeout
                                finishCreate();
                            }else if(dataBytes[0] == (byte)0x01 || dataBytes[0] == (byte)0x04){ //success
                                finish(2);
                            }else{
                                txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x98, (byte)0x2B}; //try again
                                write(txArray);
                                txLog();
                                act.updateProgressHC();
                            }
                        }
                    }else if(commandByte == (byte)0xD5){ //ack writing completed
                        writing = false;
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x98, (byte)0x2B}; //get lock results
                        write(txArray);
                        txLog();
                    }
                    //fetch or update firmware block
                    if(commandByte == (byte)0xDB){
                        fetchedData = new byte[17];
                        fetchedData = dataBytes.clone();

                        if(fetchingFirmware){
                            finish(4);
                        }else{
                            hcFWVersion = Integer.valueOf(new String(fetchedData, "UTF-8").substring(10, 13));
                            if(Integer.valueOf(fwVersion) <= hcFWVersion){
                                act.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(act);
                                        builder.setTitle(act.getString(R.string.m_updateFirmware));
                                        builder.setMessage(act.getString(R.string.m_sameFirmware));
                                        builder.setPositiveButton(act.getString(R.string.m_yes), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0xAA, (byte)0x3D};
                                                write(txArray); //prepare for writing firmware
                                                txLog();
                                            }
                                        });
                                        builder.setNegativeButton(act.getString(R.string.m_no), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                act.prgDialogHC.dismiss();
                                            }
                                        });
                                        builder.create().show();
                                    }
                                });
                            }
                            else {
                                txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0xAA, (byte)0x3D};
                                write(txArray); //prepare for writing firmware
                                txLog();
                            }
                        }
                    }else if(commandByte == (byte)0xEA){ //ack we're writing firmware
                        String fileName = fwVersion + "firmware.bin";
                        try{
                            inputStream = act.getAssets().open(fileName);
                            readBytes = new byte[inputStream.available()];
                            inputStream.read(readBytes);
                            partCounter = 0;
                            float fPartTotal = (float)(((float)readBytes.length/200.0) + .5);
                            partTotal = (int)fPartTotal;
                            totalParts = ByteBuffer.allocate(4).putInt(partTotal).array();
                            int length = 12 + fileName.length();
                            byte[] byteArray = new byte[length];
                            byteArray[0] = (byte)0x8E;
                            byteArray[1] = (byte)0x00;
                            byteArray[2] = (byte)length;
                            byteArray[3] = (byte)0x9E;
                            BigInteger bigInt = new BigInteger(String.valueOf(readBytes.length));
                            byte[] fileLength = bigInt.toByteArray();
                            switch(fileLength.length){
                                case 0:
                                    Toast.makeText(act, act.getString(R.string.m_errorTimeout), Toast.LENGTH_SHORT).show();
                                    break;
                                case 1:
                                    byteArray[4] = (byte)0x00;
                                    byteArray[5] = (byte)0x00;
                                    byteArray[6] = (byte)0x00;
                                    byteArray[7] = fileLength[0];
                                    break;
                                case 2:
                                    byteArray[4] = (byte)0x00;
                                    byteArray[5] = (byte)0x00;
                                    byteArray[6] = fileLength[0];
                                    byteArray[7] = fileLength[1];
                                    break;
                                case 3:
                                    byteArray[4] = (byte)0x00;
                                    byteArray[5] = fileLength[0];
                                    byteArray[6] = fileLength[1];
                                    byteArray[7] = fileLength[2];
                                    break;
                                case 4:
                                    byteArray[4] = fileLength[0];
                                    byteArray[5] = fileLength[1];
                                    byteArray[6] = fileLength[2];
                                    byteArray[7] = fileLength[3];
                                    break;
                            }
                            byteArray[8] = (byte)0x00;
                            byteArray[9] = (byte)0xFF;
                            byteArray[10] = (byte)fileName.length();
                            byte sum = 0;
                            int fileNamePos = 0;
                            for(int i = 0; i < length-1; i++){
                                if(i > 10){
                                    byteArray[i] = (byte)fileName.charAt(fileNamePos);
                                    fileNamePos++;
                                }
                                sum = (byte)(sum + byteArray[i]);
                            }
                            byteArray[length-1] = sum;
                            StringBuilder sb = new StringBuilder();
                            for(byte b: byteArray){
                                sb.append(String.format("0x%x ", b));
                            }
                            Log.d("BTCOMM TX", sb.toString());
                            write(byteArray);
                        }catch(IOException e){ //error
                            finish(0);
                        }
                    }else if(commandByte == (byte)0xDE){ //ack we're ready to send file parts
                        writeFilePart();
                    }else if(commandByte == (byte)0xE2){ //ack file part received
                        if(dataBytes[0] == (byte)0x00){
                            if(partCounter < partTotal){
                                writeFilePart();
                            }else{
                                int sumFile = 0;
                                for(byte b : readBytes){
                                    sumFile = (sumFile + (b & 0xFF));
                                }
                                byte[] sumBytes = ByteBuffer.allocate(4).putInt(sumFile).array();
                                byte sum = (byte)((byte)0x8E + (byte)0x07 + (byte)0xA3 + sumBytes[2] + sumBytes[3]);
                                txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x07, (byte)0xA3, sumBytes[2], sumBytes[3], sum};
                                write(txArray); //done writing file
                                txLog();
                            }
                        }else{
                            finish(0);
                        }
                    }else if(commandByte == (byte)0xE3){
                        runThread = false;
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x52, (byte)0xE5}; //do a reset
                        write(txArray);
                        txLog();
                        finish(3);
                    }
                    //obd block
                    if(commandByte == (byte)0x90){ //obd has been pinged
                        act.runOnUiThread(new Runnable(){
                            public void run(){
                                act.fetchFirmware();
                            }
                        });
                    }else if(commandByte == (byte)0x5B){ //obd protocol result
                        if(dataBytes[0] == (byte)0x00){ //ecu
                            activateWithTool = false;
                            if(obdIdsGathered){
                                finish(7);
                            }else{
                                finish(8);
                            }
                        }else if(dataBytes[0] == (byte)0x01){ //tool
                            activateWithTool = true;
                            finish(7);
                        }else{
                            //vehicle not compatible?
                        }
                    }else if(commandByte == (byte)0x50){ //obd start was successful
                        txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x14, (byte)0xA7}; //check communication status
                        write(txArray);
                        txLog();
                    }else if(commandByte == (byte)0x54){ //ack check communication
                        if(dataBytes[0] == (byte) 0x00){ //ecu is active
                            //do some writing
                            //eventually determine if ids need to be sent or if the tires need to be activated afterward!!!
                        }else if(dataBytes[0] == (byte) 0x03){ //ecu is not communicating
                            //broken
                        }else{ //ecu communication is tentative
                            txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x14, (byte)0xA7}; //try again?
                            write(txArray);
                            txLog();
                        }
                    }
                    msgStarted = false;
                    msgLength = 0;
                }
            }catch(IOException e){
                runThread = false;
                cancel();
            }
        }
    }
    private void writeFilePart(){
        //length of command = 213
        //number of bytes per part = 200
        //part number
        byte[] partNumBytes = new byte[2];
        partNumBytes[0] = (byte)0x00;
        partNumBytes[1] = (byte)0x00;
        if(partCounter > 0){
            BigInteger bInt = new BigInteger(String.valueOf(partCounter));
            byte[] partNum = bInt.toByteArray();
            switch(partNum.length){
                case 0:
                    Toast.makeText(act, act.getString(R.string.m_errorTimeout), Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    partNumBytes[0] = (byte)0x00;
                    partNumBytes[1] = partNum[0];
                    break;
                case 2:
                    partNumBytes[0] = partNum[0];
                    partNumBytes[1] = partNum[1];
                    break;
            }
        }
        //get data from file
        int start = partCounter * 200;
        int end = start + 200;
        if(end >= readBytes.length){
            end = (readBytes.length - 1);
        }
        byte[] dataArray = Arrays.copyOfRange(readBytes, start, end);
        int arrayLength = (end - start) + 13;
        txArray = new byte[arrayLength];
        byte sum = 0;
        for(int i = 0; i < (arrayLength - 1); i++){
            if(i < 12){
                switch(i){
                    case 0:
                        txArray[i] = (byte)0x8E;
                        break;
                    case 1:
                        txArray[i] = (byte)0x00;
                        break;
                    case 2:
                        txArray[i] = (byte)arrayLength;
                        break;
                    case 3:
                        txArray[i] = (byte)0xA2;
                        break;
                    case 4:
                        txArray[i] = (byte)0x00;
                        break;
                    case 5:
                        txArray[i] = (byte)0x00;
                        break;
                    case 6:
                        txArray[i] = (byte)0x00;
                        break;
                    case 7:
                        txArray[i] = (byte)(arrayLength - 13);
                        break;
                    case 8:
                        txArray[i] = partNumBytes[0];
                        break;
                    case 9:
                        txArray[i] = partNumBytes[1];
                        break;
                    case 10:
                        txArray[i] = totalParts[2];
                        break;
                    case 11:
                        txArray[i] = totalParts[3];
                        break;
                }
            }else{
                txArray[i] = dataArray[i - 12];
            }
            sum = (byte)(sum + (byte)txArray[i]);
        }
        txArray[(arrayLength - 1)] = sum;
        write(txArray); //send file part
        txLog();
        partCounter++;
        act.updateProgressFirmware(partCounter, partTotal);
    }
    private void txLog(){
        sb = new StringBuilder();
        for(byte b: txArray){
            sb.append(String.format("0x%x ", b));
        }
        Log.d("BTCOMM TX", sb.toString());
    }
    private void finishCheck(){
        if(cmdWordsCount < act.vehicle.commandWords.size()-1){
            cmdWordsCount++;
            for(int i = cmdWordsCount; i < act.vehicle.commandWords.size(); i++){
                CommandWord cmdWord = act.vehicle.commandWords.get(cmdWordsCount);
                if(act.checkWord != (byte)cmdWord.checkWord){
                    act.checkWord = (byte)cmdWord.checkWord;
                    byte sum = (byte)((byte)0x8E + (byte)0x00 + (byte)0x07 + (byte)0x60 + act.checkWord + act.mhz);
                    write(new byte[]{(byte)0x8E, (byte)0x00, (byte)0x07, (byte)0x60, act.checkWord, act.mhz, sum});
                    return;
                }
            }
            finish(0);
        }else{
            finish(0);
        }
    }
    private void finishCreate(){
        if(cmdWordsCount < act.vehicle.commandWords.size()-1){
            cmdWordsCount++;
            write(new byte[]{(byte) 0x8E, (byte) 0x00, (byte) 0x06, (byte) 0x80, (byte) 0x03, (byte) 0x17});
        }else{
            finish(0);
        }
    }
    private void finishDiscoverFreq(){
        if(act.mhz == (byte)0x01){
            act.mhz = (byte)0x02;
            byte sum = (byte)((byte)0x8E + (byte)0x00 + (byte)0x07 + (byte)0x60 + act.checkWord + act.mhz);
            write(new byte[]{(byte) 0x8E, (byte) 0x00, (byte) 0x07, (byte) 0x60, act.checkWord, act.mhz, sum});
        }else{
            finish(6);
        }
    }
    private void finish(final int result){
        act.prgDialogHC.dismiss();
        act.runOnUiThread(new Runnable(){
            public void run(){
                switch(result){
                    case 0: //error
                        Toast.makeText(act, act.getString(R.string.m_errorTimeout), Toast.LENGTH_SHORT).show();
                        break;
                    case 1: //came from check, copy, or copy set
                        if(copy){
                            act.copyResult(resultSensorID, sensorPressure, sensorTemp, sensorBat);
                        }else if(copySet){
                            copySetIDs[act.copySensor][0] = resultSensorID[0];
                            copySetIDs[act.copySensor][1] = resultSensorID[1];
                            copySetIDs[act.copySensor][2] = resultSensorID[2];
                            copySetIDs[act.copySensor][3] = resultSensorID[3];
                            act.copySetResult(resultSensorID, sensorPressure, sensorTemp, sensorBat);
                        }else{
                            act.checkResult(resultSensorID, sensorPressure, sensorTemp, sensorBat);
                        }
                        break;
                    case 2: //came from create
                        act.createResult(resultSensorID, sensorPressure, sensorTemp, sensorBat);
                        break;
                    case 3: //update firmware
                        act.firmwareUpdated();
                        break;
                    case 4: //fetch firmware
                        act.returnFirmwareDetails(fwVersion, fetchedData);
                        break;
                    case 5: //discovered frequency
                        act.frequencyDiscovered();
                        break;
                    case 6: //unable to discover frequency
                        act.frequencyUndiscovered();
                        break;
                    case 7: //did ecuOrTool discovery and found ecu, plus ids are already collected
                        act.doRelearn();
                        break;
                    case 8: //did ecuOrTool discovery and found ecu, but no ids
                        Toast.makeText(act, act.getString(R.string.m_sensorBeforeOBD), Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }
    private void cancel(){
        try{
            btSocket.close();
            connected = false;
            act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    act.imvBluetooth.setSelected(false);
                }
            });
        }catch(IOException e){
        }
    }
    public void cancelSensor(){
        byte[] txArray = new byte[]{(byte)0x8E, (byte)0x00, (byte)0x05, (byte)0x51, (byte)0xE4};
        write(txArray);
        StringBuilder sb = new StringBuilder();
        for(byte b: txArray){
            sb.append(String.format("0x%x ", b));
        }
        Log.d("BTCOMM TX", sb.toString());
    }
}
